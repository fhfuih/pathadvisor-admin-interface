git submodule init
git submodule update
find "./pathadvisor-admin-interface-settings" -maxdepth 1 -print | while read file; do
  if [[ $file != *".git"* ]] && [ $file != "./pathadvisor-admin-interface-settings" ]; then
    ln -f "$file" .;
  fi
done