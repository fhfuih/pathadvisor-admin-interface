# HKUST Path advisor admin interface

## Running from source

### Dependencies

Run `npm install` to install the dependencies.

Create a `.env` file with the following field(s)

```env
REACT_APP_API_ENDPOINT_URI=http://localhost:9000 # http://pathadvisor.ust.hk/api or any backend instance
```

Since this fork adds street view features and the production database is not updated, you must run backend from local machine with an updated underlying MongoDB instance. Detailed docs are in backend repo.

Also if using the "vanilla" DB dump and doing migration manually, you need to run the `scripts/createUser.js` script to create a user you like and assign "admin" permissions. If using "streetv" DB dump, an user "admin" and passwd "admin" is already added.

### Running

Run `npm start` to start.

