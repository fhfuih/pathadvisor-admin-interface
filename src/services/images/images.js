import postBinary from '../postBinary';

async function uploadImage(image) {
  const { parsedBody: { data } = { data: null }, error } = await postBinary(`/images`, image);
  return { data, error };
}

export { uploadImage };
