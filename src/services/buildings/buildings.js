import serviceBase from '../serviceBase';

const url = '/buildings';
const { get, upsert, remove } = serviceBase(url);

export { get, upsert, remove };
