import serviceBase from '../serviceBase';

const url = '/floors';
const { get, upsert, remove } = serviceBase(url);

const getOne = id => get({ overrideUrl: `${url}/${id}` });

export { getOne, get, upsert, remove };
