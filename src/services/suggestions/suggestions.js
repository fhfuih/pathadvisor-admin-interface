import serviceBase from '../serviceBase';

const url = '/suggestions';
const { get, upsert } = serviceBase(url);

const resolve = (id, requestBody) =>
  upsert(id, requestBody, { overrideUrl: `/suggestions/${id}/resolved` });

export { get, resolve };
