import serviceBase from '../serviceBase';

const url = '/nodes';
const { get, insert, upsert, remove } = serviceBase(url);
const getWithUrlOverridden = floorId => get({ overrideUrl: `/floors/${floorId}/nodes` });

export { getWithUrlOverridden as get, insert, upsert, remove };
