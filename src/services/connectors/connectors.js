import serviceBase from '../serviceBase';

const url = '/connectors';
const { get, upsert, remove } = serviceBase(url);

export { get, upsert, remove };
