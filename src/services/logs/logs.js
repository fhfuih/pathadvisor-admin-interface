import { APIEndpoint } from '../../config/config';
import { getSession } from '../auth/auth';

function getDownloadUrl(startDate, endDate) {
  if (!startDate || !endDate) {
    return '';
  }

  return `${APIEndpoint()}/logs?startDate=${new Date(startDate).valueOf()}&endDate=${new Date(
    endDate,
  ).valueOf()}&session=${getSession()}`;
}

export { getDownloadUrl };
