import postJSON from '../postJSON';
import getJSON from '../getJSON';

const SESSION_STORAGE_KEY = 'pathadvisor:session';

async function login(username, password) {
  const { error, parsedBody: { data: { session } = {} } = {} } = await postJSON(
    '/login',
    { username, password },
    { noSession: true },
  );

  if (error || !session) {
    return {
      success: false,
      errorMessage: error.message,
    };
  }

  localStorage.setItem(SESSION_STORAGE_KEY, session);
  return { success: true };
}

function getSession() {
  return localStorage.getItem(SESSION_STORAGE_KEY);
}

async function logout() {
  if (!getSession()) {
    return true;
  }

  await postJSON('/logout');
  localStorage.removeItem(SESSION_STORAGE_KEY);

  return true;
}

async function isSessionValid() {
  const session = localStorage.getItem(SESSION_STORAGE_KEY);
  if (!session) {
    return false;
  }

  const { error } = await getJSON('/is-logged-in');

  if (error) {
    return false;
  }

  return true;
}

export { login, getSession, isSessionValid, logout };
