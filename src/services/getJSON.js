import { APIEndpoint } from '../config/config';
import { getSession } from './auth/auth';

async function getJSON(url) {
  try {
    const response = await fetch(APIEndpoint() + url, {
      method: 'GET',
      headers: { 'pathadvisor-session-key': getSession() },
    });

    const parsedBody = await response.json();
    const error = response.ok ? null : parsedBody.error;

    return { response, parsedBody, error };
  } catch (err) {
    return { error: { message: err.message } };
  }
}

export default getJSON;
