import getJSON from './getJSON';
import postJSON from './postJSON';
import deleteJSON from './deleteJSON';

function base(url) {
  async function get({ overrideUrl = null } = {}) {
    const { parsedBody: { data } = {}, error } = await getJSON(overrideUrl || url);
    return { data, error };
  }

  async function upsert(id, requestBody, { overrideUrl = null } = {}) {
    const { parsedBody: { data } = {}, error } = await postJSON(
      overrideUrl || `${url}/${id}`,
      requestBody,
    );
    return { data, error };
  }

  async function remove(id, { overrideUrl = null } = {}) {
    const { parsedBody: { data } = {}, error } = await deleteJSON(overrideUrl || `${url}/${id}`);
    return { data, error };
  }

  async function insert(requestBody, { overrideUrl = null } = {}) {
    const { parsedBody: { data } = {}, error } = await postJSON(overrideUrl || url, requestBody);
    return { data, error };
  }

  return { insert, get, upsert, remove };
}

export default base;
