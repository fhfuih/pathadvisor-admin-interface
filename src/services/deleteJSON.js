import { APIEndpoint } from '../config/config';
import { getSession } from './auth/auth';

async function deleteJSON(url) {
  try {
    const response = await fetch(APIEndpoint() + url, {
      method: 'DELETE',
      headers: { 'pathadvisor-session-key': getSession() },
    });

    const parsedBody = await response.json();
    const error = response.ok ? null : parsedBody.error;

    return { response, parsedBody, error };
  } catch (err) {
    return { error: { message: err.message } };
  }
}

export default deleteJSON;
