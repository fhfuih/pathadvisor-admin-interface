import serviceBase from '../serviceBase';

const url = '/edges';
const { get, insert, remove } = serviceBase(url);
const getWithUrlOverridden = floorId => get({ overrideUrl: `/floors/${floorId}/edges` });

export { getWithUrlOverridden as get, insert, remove };
