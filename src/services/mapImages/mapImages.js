import { APIEndpoint } from '../../config/config';
import postBinary from '../postBinary';

function getImageSrc(floorId) {
  return `${APIEndpoint()}/floors/${floorId}/map-image?${new Date().getTime()}`;
}

async function uploadImage(floorId, image) {
  const { parsedBody: { data } = { data: null }, error } = await postBinary(
    `/floors/${floorId}/map-image`,
    image,
  );
  return { data, error };
}

export { getImageSrc, uploadImage };
