function normalizeData(data, transform) {
  return {
    ids: data.map(({ _id }) => _id),
    data: data.reduce((carrier, item) => {
      const { _id, ...rest } = item;
      carrier[_id] = transform ? transform(rest) : rest;
      return carrier;
    }, {}),
  };
}

export default normalizeData;
