import { APIEndpoint } from '../config/config';
import { getSession } from './auth/auth';

async function postJSON(url, data = undefined, { noSession = false } = {}) {
  try {
    const response = await fetch(APIEndpoint() + url, {
      method: 'POST',
      headers: {
        ...(!noSession ? { 'pathadvisor-session-key': getSession() } : {}),
        ...(data !== undefined ? { 'Content-Type': 'application/json' } : {}),
      },
      ...(data !== undefined ? { body: JSON.stringify(data) } : {}),
    });

    const parsedBody = await response.json();
    const error = response.ok ? null : parsedBody.error;

    return { response, parsedBody, error };
  } catch (err) {
    return { error: { message: err.message } };
  }
}

export default postJSON;
