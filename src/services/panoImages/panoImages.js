import postFormData from '../postFormData';

async function uploadPanoImage(form) {
  const { parsedBody: { data } = { data: null }, error } = await postFormData(`/pano/images`, form);
  return { data, error };
}

export { uploadPanoImage };
