import serviceBase from '../serviceBase';

const url = '/apiKeys';
const { get, insert, upsert, remove } = serviceBase(url);

export { get, insert, upsert, remove };
