import serviceBase from '../serviceBase';
import postBinary from '../postBinary';

const url = '/tags';
const { get, upsert, remove } = serviceBase(url);

async function uploadImage(id, image) {
  const { parsedBody: { data } = { data: null }, error } = await postBinary(
    `${url}/${id}/data`,
    image,
  );
  return { data, error };
}

export { get, upsert, remove, uploadImage };
