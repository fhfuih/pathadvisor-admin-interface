import postJSON from '../postJSON';

async function rebuildGraph() {
  const { parsedBody: { data } = {}, error } = await postJSON('/graph/rebuild');
  return { data, error };
}

async function clearInitDataCache() {
  const { parsedBody: { data } = {}, error } = await postJSON('/init-data/rebuild');
  return { data, error };
}

async function updateMeta() {
  const { parsedBody: { data } = {}, error } = await postJSON('/meta');
  return { data, error };
}

export { rebuildGraph, clearInitDataCache, updateMeta };
