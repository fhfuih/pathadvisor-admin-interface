import serviceBase from '../serviceBase';

const url = '/settings';
const { get, insert } = serviceBase(url);

export { get, insert };
