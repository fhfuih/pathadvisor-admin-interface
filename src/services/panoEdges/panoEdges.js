import serviceBase from '../serviceBase';

const url = '/pano/edges';
const { get, insert, remove } = serviceBase(url);
const getWithUrlOverridden = floorId => get({ overrideUrl: `/pano/floors/${floorId}/edges` });

export { getWithUrlOverridden as get, insert, remove };
