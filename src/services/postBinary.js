import { APIEndpoint } from '../config/config';
import { getSession } from './auth/auth';

async function postBinary(url, data) {
  try {
    const response = await fetch(APIEndpoint() + url, {
      method: 'POST',
      headers: {
        'pathadvisor-session-key': getSession(),
        'Content-Type': 'application/octet-stream',
      },

      body: data,
    });

    const parsedBody = await response.json();
    const error = response.ok ? null : parsedBody.error;

    return { response, parsedBody, error };
  } catch (err) {
    return { error: { message: err.message } };
  }
}

export default postBinary;
