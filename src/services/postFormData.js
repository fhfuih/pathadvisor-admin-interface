import { APIEndpoint } from '../config/config';
import { getSession } from './auth/auth';

async function postFormData(url, form = undefined, { noSession = false } = {}) {
  try {
    const response = await fetch(APIEndpoint() + url, {
      method: 'POST',
      headers: {
        ...(!noSession ? { 'pathadvisor-session-key': getSession() } : {}),
      },
      ...(form !== undefined ? { body: form } : {}),
    });

    const parsedBody = await response.json();
    const error = response.ok ? null : parsedBody.error;

    return { response, parsedBody, error };
  } catch (err) {
    return { error: { message: err.message } };
  }
}

export default postFormData;
