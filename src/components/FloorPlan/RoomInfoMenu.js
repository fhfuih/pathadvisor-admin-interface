import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DropDownIcon from '@material-ui/icons/ArrowDropDown';
import DropUpIcon from '@material-ui/icons/ArrowDropUp';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';
import nodePropType from './nodePropType';

const styles = {
  roomInfoMenu: {
    backgroundColor: 'whitesmoke',
    width: '100%',
  },
  roomInfoMenuHead: {
    display: 'flex',
    alignItems: 'center',
  },
  arrowButton: {
    padding: '3px',
    minWidth: 0,
  },
  arrowIcon: {
    fontSize: '14px',
  },
  menuTitle: {
    fontSize: '12px',
    padding: '3px 10px',
    color: 'white',
    backgroundColor: '#333',
    fontWeight: 'bold',
    flex: 1,
  },
  button: {
    padding: '5px',
    fontSize: '10px',
    margin: '3px',
    minWidth: 0,
  },
  row: {
    fontSize: '10px',
    padding: '3px',
  },
  flexRow: {
    fontSize: '10px',
    padding: '3px',
    display: 'flex',
    alignItems: 'center',
  },
  input: {
    marginTop: '3px',
  },
  hide: { display: 'none' },
  select: { maxWidth: '200px' },
};

class RoomInfoMenu extends Component {
  renderBody() {
    const {
      classes,
      room,
      tagIds,
      tagData,
      connectorIds,
      uploadingImage,
      uploadImageSuccess,
      uploadImageError,
      onNodeInputChange,
      onSetDoor,
      onRemoveDoor,
      onAddSubPolygon,
      onRemoveSubPolygon,
      onImageChange,
      onDeleteImage,
    } = this.props;

    if (!room) {
      return <div>Please select a room</div>;
    }

    return (
      <>
        <div className={classes.row}>
          <div className={classes.label}>Name</div>
          <input
            type="text"
            className={classes.input}
            value={room.name || ''}
            onChange={onNodeInputChange('name')}
          />
        </div>
        <div className={classes.row}>
          <div className={classes.label}>Keywords</div>
          <textarea
            className={classes.input}
            value={room.keywords || ''}
            onChange={onNodeInputChange('keywords')}
          />
        </div>
        <div className={classes.row}>
          <div className={classes.label}>Tag</div>
          <select
            className={classes.select}
            value={room.tagIds ? room.tagIds[0] : ''}
            onChange={onNodeInputChange('tagIds')}
          >
            <option value="" />
            {tagIds.map(id => (
              <option key={id} value={id}>
                {tagData[id].name}
              </option>
            ))}
          </select>
        </div>
        <div className={classes.row}>
          <div className={classes.label}>Url</div>
          <input
            type="text"
            className={classes.input}
            value={room.url || ''}
            onChange={onNodeInputChange('url')}
          />
        </div>
        <div className={classes.flexRow}>
          <div className={classes.label}>Unsearchable</div>
          <input
            type="checkbox"
            className={classes.input}
            checked={room.unsearchable || false}
            onChange={onNodeInputChange('unsearchable')}
          />
        </div>
        <div className={classes.row}>
          <div className={classes.label}>Connector Id</div>
          <select
            className={classes.select}
            value={room.connectorId || ''}
            onChange={onNodeInputChange('connectorId')}
          >
            <option value="" />
            {connectorIds.map(id => (
              <option key={id} value={id}>
                {id}
              </option>
            ))}
          </select>
        </div>
        {!room.coordinates ? (
          <Button variant="outlined" className={classes.button} onClick={onSetDoor}>
            Set Door
          </Button>
        ) : (
          <Button variant="outlined" className={classes.button} onClick={onRemoveDoor}>
            Remove Door
          </Button>
        )}

        <Button variant="outlined" className={classes.button} onClick={onAddSubPolygon}>
          Add sub polygon
        </Button>
        {room.geoLocs.coordinates.length > 1 ? (
          <Button variant="outlined" className={classes.button} onClick={onRemoveSubPolygon}>
            Remove sub polygons
          </Button>
        ) : null}

        {room.image ? (
          <Button variant="outlined" className={classes.button} onClick={onDeleteImage}>
            Delete Image
          </Button>
        ) : (
          <>
            <input
              accept="image/*"
              className={classes.hide}
              id="mapItemImageFileInput"
              type="file"
              onChange={onImageChange}
              disabled={uploadingImage}
            />
            <label htmlFor="mapItemImageFileInput">
              <Button
                component="span"
                variant="outlined"
                disabled={uploadingImage}
                className={classes.button}
              >
                {uploadingImage ? 'Uploading...' : 'Upload image'}
              </Button>
            </label>
          </>
        )}

        {room.image ? (
          <Button
            variant="outlined"
            className={classes.button}
            href={room.imageUrl}
            target="_blank"
          >
            View Image
          </Button>
        ) : null}

        <AutoStatusBar
          open={Boolean(uploadImageSuccess)}
          variant="success"
          message="Image uploaded successfully"
        />
        <AutoStatusBar
          open={Boolean(uploadImageError)}
          variant="error"
          message={(uploadImageError || {}).message}
        />
      </>
    );
  }

  render() {
    const { classes, showRoomInfoMenu, toggleRoomInfoMenu } = this.props;
    return (
      <div className={classes.roomInfoMenu}>
        <div className={classes.roomInfoMenuHead}>
          <Button className={classes.arrowButton} onClick={toggleRoomInfoMenu}>
            {showRoomInfoMenu ? (
              <DropUpIcon className={classes.arrowIcon} />
            ) : (
              <DropDownIcon className={classes.arrowIcon} />
            )}
          </Button>
          <div className={classes.menuTitle}> Room information </div>
        </div>

        {showRoomInfoMenu && this.renderBody()}
      </div>
    );
  }
}

RoomInfoMenu.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  showRoomInfoMenu: PropTypes.bool.isRequired,
  toggleRoomInfoMenu: PropTypes.func.isRequired,
  room: nodePropType,
  tagIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  tagData: PropTypes.objectOf(PropTypes.shape({ name: PropTypes.string.isRequired })).isRequired,
  connectorIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  uploadingImage: PropTypes.bool,
  uploadImageSuccess: PropTypes.bool,
  uploadImageError: PropTypes.shape({ message: PropTypes.string.isRequired }),
  onNodeInputChange: PropTypes.func.isRequired,
  onSetDoor: PropTypes.func.isRequired,
  onRemoveDoor: PropTypes.func.isRequired,
  onAddSubPolygon: PropTypes.func.isRequired,
  onRemoveSubPolygon: PropTypes.func.isRequired,
  onImageChange: PropTypes.func.isRequired,
  onDeleteImage: PropTypes.func.isRequired,
};

export default withStyles(styles)(RoomInfoMenu);
