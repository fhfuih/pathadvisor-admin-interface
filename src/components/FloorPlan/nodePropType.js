import PropTypes from 'prop-types';

export default PropTypes.shape({
  floorId: PropTypes.string.isRequired,
  name: PropTypes.string,
  coordinates: PropTypes.arrayOf(PropTypes.number),
  centerCoordinates: PropTypes.arrayOf(PropTypes.number),
  geoLocs: PropTypes.shape({
    type: PropTypes.oneOf(['MultiPolygon']).isRequired,
    coordinates: PropTypes.arrayOf(
      PropTypes.arrayOf(
        PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number).isRequired).isRequired,
      ).isRequired,
    ).isRequired,
  }),
  unsearchable: PropTypes.bool,
  connectorId: PropTypes.string,
  url: PropTypes.string,
  image: PropTypes.string,
  imageUrl: PropTypes.string,
  tagIds: PropTypes.arrayOf(PropTypes.string),
  keywords: PropTypes.arrayOf(PropTypes.string),
});
