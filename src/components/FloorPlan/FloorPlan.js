import React, { Component, createRef } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import isEmpty from 'lodash.isempty';
import difference from 'lodash.difference';
import xor from 'lodash.xor';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import CanvasHandler from '../../pathadvisor-map-canvas/CanvasHandler';
import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';
import getNodeShape, { getNodeShapeIds } from './getNodeShape';
import getEdgeShape, { getEdgeShapeIds } from './getEdgeShape';
import getPointShape from './getPointShape';
import getGeoLoc, { GEO_LOC_POLYGON, GEO_LOC_RECTANGLE } from './getGeoLoc';
import RoomInfoMenu from './RoomInfoMenu';
import PanoInfoMenu from './PanoInfoMenu';
import nodePropType from './nodePropType';
import { ACTION } from '../FloorPlanPage/constants';

const WEIGHT_TYPE = {
  NODE_DISTANCE: 'nodeDistance',
  MAX: 'max',
  NUMBER: 'number',
};

const PANO_COLOR = 'MediumOrchid';
const SELECTED_COLOR = 'rgb(26, 115, 232)';
const ERROR_COLOR = 'red';

const ARROW_KEY = {
  LEFT: 'ArrowLeft',
  RIGHT: 'ArrowRight',
  UP: 'ArrowUp',
  DOWN: 'ArrowDown',
};

const DRAW_MODE = {
  DOOR: 'DRAW_MODE_DOOR',
  RECTANGLE: 'DRAW_MODE_RECTANGLE',
  POLYGON: 'DRAW_MODE_POLYGON',
  SUB_POLYGON: 'DRAW_MODE_SUB_POLYGON',
  EDGE: 'DRAW_MODE_EDGE',
  POINT: 'DRAW_MODE_POINT',
};

const DRAW_MODE_GEO_LOC_SHAPE = {
  [DRAW_MODE.RECTANGLE]: GEO_LOC_RECTANGLE,
  [DRAW_MODE.POLYGON]: GEO_LOC_POLYGON,
  [DRAW_MODE.SUB_POLYGON]: GEO_LOC_POLYGON,
};

const NODE_TYPE = {
  POINT: 'POINT',
  POLYGON: 'POLYGON',
};

const styles = {
  canvasRoot: { height: '100%', overflow: 'hidden' },
  hide: { display: 'none' },
  menu: {
    position: 'absolute',
    backgroundColor: 'whitesmoke',
    width: '180px',
    height: '100%',
  },
  floatMenu: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 210,
  },
  button: {
    padding: '5px',
    fontSize: '10px',
    margin: '3px',
    minWidth: 0,
  },
  menuTitle: {
    fontSize: '12px',
    padding: '3px 10px',
    color: 'white',
    backgroundColor: '#333',
    fontWeight: 'bold',
  },
  menuContent: {
    fontSize: '10px',
  },
  optionBox: {
    display: 'flex',
    alignItems: 'center',
    padding: '2px 5px',
  },
  optionLabel: {
    fontSize: '10px',
  },
  precisionInput: {
    width: '50px',
    marginLeft: '5px',
  },
  rect: {
    width: 5,
    height: 5,
    margin: 5,
    flex: '5px 0 0',
  },
  circle: {
    width: 5,
    height: 5,
    margin: 5,
    flex: '5px 0 0',
    borderRadius: 2.5,
  },
};

const initDrawState = { drawMode: null, drawParams: {}, intermediateItems: [] };
class FloorPlan extends Component {
  canvasRootRef = createRef();

  state = {
    ...initDrawState,
    persistDraw: false,
    precision: 1,
    isPolygonMovable: true,
    showRoomInfoMenu: true,
    showPanoInfoMenu: true,
  };

  componentDidMount() {
    this.canvasHandler = new CanvasHandler();

    const { floorId } = this.props;
    this.canvasRootRef.current.appendChild(this.canvasHandler.getCanvas());

    this.canvasHandler.updateLevelToScale([1]);

    this.canvasHandler.updateDimension(
      this.canvasRootRef.current.offsetWidth,
      this.canvasRootRef.current.offsetHeight,
    );

    this.canvasHandler.updatePosition(0, 0, floorId, 0);
    this.canvasHandler.addMouseUpListener(({ clientMapX, clientMapY }) => {
      this.draw(clientMapX, clientMapY);
    });

    this.renderMapImage();
    this.renderCanvasItems();

    document.addEventListener('keydown', this.keyDownEvent);
    document.addEventListener('keyup', this.keyUpEvent);

    this.canvasHandler.addClickAwayListener(this.onClickAway);
  }

  componentDidUpdate(prevProps, prevState) {
    const { uploadImageSuccess } = this.props;

    if (uploadImageSuccess && uploadImageSuccess !== prevProps.uploadImageSuccess) {
      this.renderMapImage();
    }

    this.renderCanvasItems(prevProps, prevState);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.keyDownEvent);
    document.removeEventListener('keyup', this.keyUpEvent);
  }

  onClickAway = () => {
    if (
      this.shiftKeyPressed ||
      this.controlKeyPressed ||
      [DRAW_MODE.EDGE, DRAW_MODE.SUB_POLYGON].includes(this.state.drawMode)
    ) {
      return;
    }

    // clear all selection
    this.clearAllSelections();
  };

  onClickNode = (id, { isPolygonClickHandler = false } = {}) => ({ stopPropagation }) => {
    const { onSelect, onDeselectNodes } = this.props;
    const { drawMode } = this.state;

    if (drawMode === DRAW_MODE.EDGE && isPolygonClickHandler) {
      return;
    }

    if (
      [
        DRAW_MODE.RECTANGLE,
        DRAW_MODE.POLYGON,
        DRAW_MODE.POINT,
        DRAW_MODE.DOOR,
        DRAW_MODE.SUB_POLYGON,
      ].includes(drawMode)
    ) {
      return;
    }

    if (this.controlKeyPressed) {
      onDeselectNodes([id]);
      return;
    }

    if (!this.shiftKeyPressed) {
      stopPropagation();
    }

    onSelect({
      // clear edge selection if shift key is not pressed
      edges: { ids: [], merge: this.shiftKeyPressed },
      nodes: { ids: [id], merge: this.shiftKeyPressed || drawMode === DRAW_MODE.EDGE },
    });
  };

  getNodes(nodeIds) {
    const { nodeData, selectedNodes, errorNodes, displayText, unsafePanoNodeIdSet } = this.props;

    return nodeIds.reduce(
      (carrier, id) => [
        ...carrier,
        ...getNodeShape({
          id,
          node: nodeData[id],
          ...(nodeData[id].panoImageUrl
            ? { colors: { shape: PANO_COLOR, text: PANO_COLOR, point: PANO_COLOR } }
            : {}),
          ...(selectedNodes[id]
            ? { colors: { shape: SELECTED_COLOR, text: SELECTED_COLOR, point: SELECTED_COLOR } }
            : {}),
          ...(errorNodes[id]
            ? { colors: { shape: ERROR_COLOR, text: ERROR_COLOR, point: ERROR_COLOR } }
            : {}),
          onClick: this.onClickNode(id),
          onClickPolygon: this.onClickNode(id, { isPolygonClickHandler: true }),
          displayText,
          circle: nodeData[id].panoImageUrl && !unsafePanoNodeIdSet.has(id),
        }),
      ],
      [],
    );
  }

  getEdges(edgeIds) {
    const {
      edgeData,
      nodeData,
      selectedEdges,
      errorEdges,
      onSelect,
      onDeselectEdges,
      displayEdge,
    } = this.props;

    return displayEdge
      ? edgeIds.reduce(
          (carrier, id) => [
            ...carrier,
            ...getEdgeShape({
              id,
              edge: edgeData[id],
              startNode: nodeData[edgeData[id].fromNodeId],
              endNode: nodeData[edgeData[id].toNodeId],
              ...(selectedEdges[id]
                ? { colors: { indicator: SELECTED_COLOR, line: SELECTED_COLOR } }
                : {}),
              ...(errorEdges[id] ? { colors: { indicator: ERROR_COLOR, line: ERROR_COLOR } } : {}),
              onClick: ({ stopPropagation }) => {
                if (this.state.drawMode) {
                  return;
                }

                if (this.controlKeyPressed) {
                  onDeselectEdges([id]);
                  return;
                }

                if (!this.shiftKeyPressed) {
                  stopPropagation();
                }

                onSelect({
                  // clear node selection if shift key is not pressed
                  nodes: { ids: [], merge: this.shiftKeyPressed },
                  edges: { ids: [id], merge: this.shiftKeyPressed },
                });
              },
            }),
          ],
          [],
        )
      : [];
  }

  nothingSelected = () => isEmpty(this.props.selectedEdges) && isEmpty(this.props.selectedNodes);

  clearAllSelections = () => {
    if (this.nothingSelected()) {
      return;
    }
    this.props.onSelect({ nodes: { ids: [] }, edges: { ids: [] } });
  };

  keyDownEvent = e => {
    this.moveNode(e);
    this.keyPressedEvent(e);
  };

  keyUpEvent = e => {
    this.keyPressedEvent(e);
  };

  keyPressedEvent = e => {
    this.shiftKeyPressed = e.shiftKey;
    this.controlKeyPressed = e.ctrlKey || e.metaKey;
  };

  switchDrawMode = (drawMode, drawParams = {}) => () => {
    if (![DRAW_MODE.DOOR, DRAW_MODE.SUB_POLYGON].includes(drawMode)) {
      this.clearAllSelections();
    }
    this.setState({ drawMode, drawParams });
  };

  switchOffDrawMode = () => {
    this.setState({ ...initDrawState });
  };

  resetDrawMode = () => {
    this.setState(state => ({
      ...initDrawState,
      ...(state.persistDraw ? { drawMode: state.drawMode, drawParams: state.drawParams } : {}),
    }));
  };

  togglePersistDraw = () => {
    this.setState(state => ({ persistDraw: !state.persistDraw }));
  };

  onPrecisionChange = e => {
    this.setState({
      precision: parseInt(e.target.value, 10),
    });
  };

  toggleIsPolygonMovable = () => {
    this.setState(state => ({ isPolygonMovable: !state.isPolygonMovable }));
  };

  toggleRoomInfoMenu = () => {
    this.setState(state => ({ showRoomInfoMenu: !state.showRoomInfoMenu }));
  };

  togglePanoInfoMenu = () => {
    this.setState(state => ({ showPanoInfoMenu: !state.showPanoInfoMenu }));
  };

  moveNode = e => {
    if (!Object.values(ARROW_KEY).includes(e.key)) {
      return;
    }

    // Can only move items by arrow if current focus is on body
    if (e.target !== document.body) {
      return;
    }

    const { onUpsertNode, nodeData, selectedNodes } = this.props;
    const { precision, isPolygonMovable } = this.state;
    const selectedNodeIds = Object.keys(selectedNodes);
    if (selectedNodeIds.length !== 1) {
      return;
    }

    const id = selectedNodeIds[0];

    let translate = [0, 0];

    switch (e.key) {
      case ARROW_KEY.UP:
        translate = [0, -precision];
        break;
      case ARROW_KEY.DOWN:
        translate = [0, precision];
        break;
      case ARROW_KEY.LEFT:
        translate = [-precision, 0];
        break;
      case ARROW_KEY.RIGHT:
        translate = [precision, 0];
        break;
      default:
        return;
    }

    const { coordinates, centerCoordinates, geoLocs = {} } = nodeData[id];

    onUpsertNode(id, {
      ...nodeData[id],
      ...(coordinates ? { coordinates: coordinates.map((v, key) => v + translate[key]) } : {}),
      ...(isPolygonMovable && centerCoordinates
        ? { centerCoordinates: centerCoordinates.map((v, key) => v + translate[key]) }
        : {}),
      ...(isPolygonMovable && geoLocs.type === 'MultiPolygon' && geoLocs.coordinates
        ? {
            geoLocs: {
              ...geoLocs,
              coordinates: geoLocs.coordinates.map(polygon =>
                polygon.map(ring =>
                  ring.map(ringCoordinates => ringCoordinates.map((v, key) => v + translate[key])),
                ),
              ),
            },
          }
        : {}),
    });
  };

  /**
   * Returns a single selected polygon. Returns { id: null, node: null } if no or more than one polygons selected
   */
  getSelectedPolygon() {
    const emptyResponse = { id: null, node: null };
    const { selectedNodes, nodeData } = this.props;
    const selectedNodeIds = Object.keys(selectedNodes);
    if (selectedNodeIds.length !== 1) {
      return emptyResponse;
    }

    const id = selectedNodeIds[0];
    return nodeData[id] && nodeData[id].geoLocs ? { id, node: nodeData[id] } : emptyResponse;
  }

  /**
   * Returns a single selected node. Returns { id: null, node: null, type: null } if no or more than one nodes selected
   */
  getSelectedNode() {
    const emptyResponse = { id: null, node: null, type: null };
    const { selectedNodes, nodeData } = this.props;
    const selectedNodeIds = Object.keys(selectedNodes);
    if (selectedNodeIds.length !== 1) {
      return emptyResponse;
    }

    const id = selectedNodeIds[0];
    return nodeData[id] ? { id, node: nodeData[id], type: (nodeData[id].geoLocs ? NODE_TYPE.POLYGON : NODE_TYPE.POINT) } : emptyResponse;
  }

  draw = async (x, y) => {
    const { floorId, onUpsertNode, onUpsertEdge, selectedNodes, onSelect } = this.props;
    const { drawMode, drawParams } = this.state;

    switch (drawMode) {
      case DRAW_MODE.POINT:
        onUpsertNode(shortid.generate(), { floorId, coordinates: [x, y] });
        this.resetDrawMode();
        break;
      case DRAW_MODE.DOOR: {
        const { id, node } = this.getSelectedPolygon();

        if (!id || !node) {
          this.resetDrawMode();
          return;
        }

        onUpsertNode(id, { ...node, coordinates: [x, y] });
        this.resetDrawMode();
        break;
      }
      case DRAW_MODE.POLYGON:
      case DRAW_MODE.RECTANGLE:
      case DRAW_MODE.SUB_POLYGON: {
        const steps = drawMode === DRAW_MODE.RECTANGLE ? 2 : 4;

        const { intermediateItems } = this.state;

        // Do not need to render the last point to save a render as the last point will be cleared right away
        if (intermediateItems.length < steps - 1) {
          this.setState(state => ({
            intermediateItems: [
              ...state.intermediateItems,
              getPointShape({ id: shortid.generate(), floorId, coordinates: [x, y] }),
            ],
          }));

          return;
        }

        const { id, node } =
          drawMode === DRAW_MODE.SUB_POLYGON
            ? this.getSelectedPolygon()
            : { id: shortid.generate(), node: {} };

        if (!id || !node) {
          this.resetDrawMode();
          return;
        }

        onUpsertNode(id, {
          ...node,
          floorId,
          geoLocs: getGeoLoc(
            DRAW_MODE_GEO_LOC_SHAPE[drawMode],
            [
              ...intermediateItems,
              getPointShape({ id: shortid.generate(), floorId, coordinates: [x, y] }),
            ],
            node.geoLocs,
          ),
        });

        this.resetDrawMode();
        onSelect({ nodes: { ids: [id] } });

        break;
      }

      case DRAW_MODE.EDGE: {
        const selectedNodeIds = Object.keys(selectedNodes);

        if (selectedNodeIds.length !== 2) {
          return;
        }

        const [fromNodeId, toNodeId] = selectedNodeIds;
        const { weightType, uniDirection = false } = drawParams;

        onUpsertEdge(shortid.generate(), {
          floorId,
          fromNodeId,
          toNodeId,
          weightType,
        });

        if (!uniDirection) {
          onUpsertEdge(shortid.generate(), {
            floorId,
            fromNodeId: toNodeId,
            toNodeId: fromNodeId,
            weightType,
          });
        }

        this.clearAllSelections();
        this.resetDrawMode();
        break;
      }

      default:
    }
  };

  onNodeInputChange = key => e => {
    const { id, node } = this.getSelectedPolygon();

    if (!node) {
      return;
    }

    let value = e.target.value || '';

    switch (key) {
      case 'tagIds':
        value = [value];
        break;
      case 'keywords':
        value = value.split(',');
        break;
      case 'unsearchable':
        value = !node.unsearchable;
        break;
      default:
    }

    this.props.onUpsertNode(id, {
      ...node,
      [key]: value,
    });
  };

  renderCanvasItems(prevProps = {}, prevState = {}) {
    const {
      nodeIds: prevNodeIds = [],
      nodeData: prevNodeData = {},
      edgeIds: prevEdgeIds = [],
      selectedNodes: prevSelectedNodes = {},
      selectedEdges: prevSelectedEdges = {},
      errorNodes: prevErrorNodes = {},
      errorEdges: prevErrorEdges = {},
      displayText: prevDisplayText,
      displayEdge: prevDisplayEdge,
    } = prevProps;

    const { intermediateItems: prevIntermediateItems = [] } = prevState;
    const {
      nodeIds,
      nodeData,
      edgeIds,
      edgeData,
      selectedNodes,
      selectedEdges,
      errorNodes,
      errorEdges,
      displayText,
      displayEdge,
      dirtyNodes,
    } = this.props;
    const { intermediateItems } = this.state;

    const removedNodeIds = difference(prevNodeIds, nodeIds);
    const removedEdgeIds = difference(prevEdgeIds, edgeIds);
    const removeIntermediateItemIds = difference(
      prevIntermediateItems.map(({ id }) => id),
      intermediateItems.map(({ id }) => id),
    );

    let modifiedNodeIds = new Set([
      ...Object.keys(dirtyNodes).filter(id => prevNodeData[id] !== nodeData[id]),
      ...difference(nodeIds, prevNodeIds),
      ...xor(Object.keys(prevSelectedNodes), Object.keys(selectedNodes)),
      ...xor(Object.keys(prevErrorNodes), Object.keys(errorNodes)),
    ]);

    removedNodeIds.forEach(id => {
      modifiedNodeIds.delete(id);
    });

    let modifiedEdgeIds = new Set([
      ...difference(edgeIds, prevEdgeIds),
      ...xor(Object.keys(prevSelectedEdges), Object.keys(selectedEdges)),
      ...xor(Object.keys(prevErrorEdges), Object.keys(errorEdges)),
      ...edgeIds.filter(
        id =>
          modifiedNodeIds.has(edgeData[id].fromNodeId) ||
          modifiedNodeIds.has(edgeData[id].toNodeId),
      ),
    ]);

    removedEdgeIds.forEach(id => {
      modifiedEdgeIds.delete(id);
    });

    if (displayText !== prevDisplayText) {
      modifiedNodeIds = nodeIds;
    }

    if (displayEdge !== prevDisplayEdge) {
      modifiedEdgeIds = edgeIds;
    }

    this.canvasHandler.removeMapItems([
      ...[...removedNodeIds, ...modifiedNodeIds]
        .map(id => getNodeShapeIds(id, prevNodeData[id] || {}))
        .reduce((carrier, ids) => [...carrier, ...ids], []),
      ...[...removedEdgeIds, ...modifiedEdgeIds]
        .map(id => getEdgeShapeIds(id))
        .reduce((carrier, ids) => [...carrier, ...ids], []),
      ...removeIntermediateItemIds,
    ]);

    this.canvasHandler.setMapItems(
      [
        ...this.getNodes([...modifiedNodeIds]),
        ...this.getEdges([...modifiedEdgeIds]),
        ...intermediateItems,
      ],
      { merge: true },
    );
  }

  onRemoveDoor = () => {
    const { id } = this.getSelectedPolygon();
    if (!id) {
      return;
    }

    this.props.onDelete({ doors: [id] });
  };

  onRemoveSubPolygon = () => {
    const { id, node } = this.getSelectedPolygon();
    if (!id || !node) {
      return;
    }

    this.props.onUpsertNode(id, {
      ...node,
      geoLocs: {
        ...node.geoLocs,
        coordinates: [node.geoLocs.coordinates[0]],
      },
    });
  };

  onItemImageChange = e => {
    const { id } = this.getSelectedPolygon();

    if (!id) {
      return;
    }

    this.props.onItemImageChange(id, e);
  };

  onDeleteImage = () => {
    const { id } = this.getSelectedPolygon();
    if (!id) {
      return;
    }

    this.props.onDelete({ images: [id] });
  };

  onPanoImageSubmit = e => {
    const { id, type } = this.getSelectedNode();

    if (!id || type !== NODE_TYPE.POINT) {
      return;
    }

    this.props.onPanoImageSubmit(id, e);
  };

  onDeletePanoImage = () => {
    const { id, type } = this.getSelectedNode();

    if (!id || type !== NODE_TYPE.POINT) {
      return;
    }

    this.props.onDelete({ panoImages: [id] });
  };

  renderMapImage() {
    const { mapImageSrc, floorId, floorData } = this.props;

    const mapImage = new Image();
    mapImage.src = mapImageSrc;

    this.canvasHandler.removeAllMapTiles();
    this.canvasHandler.addMapTiles([
      {
        id: 'mapImage',
        floor: floorId,
        x: floorData.startX,
        y: floorData.startY,
        image: mapImage,
      },
    ]);
  }

  render() {
    console.log('render called');
    const {
      floorId,
      classes,
      onImageChange,
      uploadingImage,
      uploadImageSuccess,
      uploadImageError,
      uploadingItemImages,
      uploadItemImagesSuccess,
      uploadItemImagesError,
      uploadingPanoImages,
      uploadPanoImagesSuccess,
      uploadPanoImagesError,
      deleteError,
      deleteSuccess,
      displayText,
      displayEdge,
      selectedEdges,
      selectedNodes,
      isDirty,
      onToggleDisplayText,
      onToggleDisplayEdge,
      onDelete,
      onResetError,
      tagIds,
      tagData,
      connectorIds,
      saving,
      saveSuccess,
      saveError,
      onSave,
      selectionContainsUnsafePanoNodes,
    } = this.props;

    const {
      drawMode,
      persistDraw,
      precision,
      isPolygonMovable,
      showRoomInfoMenu,
      showPanoInfoMenu,
    } = this.state;
    const { node: selectedNode, id: selectedId, type: selectedType } = this.getSelectedNode();

    return (
      <>
        <div className={classes.menu}>
          <div className={classes.menuBox}>
            <div className={classes.menuTitle}>Floor {floorId}</div>
            <Button
              variant="outlined"
              className={classes.button}
              component={Link}
              to="/floor-plans"
            >
              Dashboard
            </Button>
            <input
              accept="image/*"
              className={classes.hide}
              id="mapImageFileInput"
              type="file"
              onChange={onImageChange}
              disabled={uploadingImage}
            />
            <label htmlFor="mapImageFileInput">
              <Button
                component="span"
                variant="outlined"
                disabled={uploadingImage}
                className={classes.button}
              >
                {uploadingImage ? 'Uploading...' : 'Upload image'}
              </Button>
            </label>
          </div>
          <div className={classes.menuBox}>
            <div className={classes.menuTitle}>Draw</div>
            <Button
              variant="outlined"
              className={classes.button}
              disabled={drawMode === DRAW_MODE.POINT}
              onClick={this.switchDrawMode(DRAW_MODE.POINT)}
            >
              Point
            </Button>
            <Button
              variant="outlined"
              className={classes.button}
              disabled={drawMode === DRAW_MODE.EDGE}
              onClick={this.switchDrawMode(DRAW_MODE.EDGE, {
                weightType: WEIGHT_TYPE.NODE_DISTANCE,
              })}
            >
              Edge
            </Button>
            <Button
              variant="outlined"
              className={classes.button}
              disabled={drawMode === DRAW_MODE.EDGE}
              onClick={this.switchDrawMode(DRAW_MODE.EDGE, {
                weightType: WEIGHT_TYPE.NODE_DISTANCE,
                uniDirection: true,
              })}
            >
              Edge Uni-direction
            </Button>
            <Button
              variant="outlined"
              className={classes.button}
              disabled={drawMode === DRAW_MODE.EDGE}
              onClick={this.switchDrawMode(DRAW_MODE.EDGE, { weightType: WEIGHT_TYPE.MAX })}
            >
              Weighted Edge
            </Button>
            <Button
              variant="outlined"
              className={classes.button}
              disabled={drawMode === DRAW_MODE.RECTANGLE}
              onClick={this.switchDrawMode(DRAW_MODE.RECTANGLE)}
            >
              Room - Rectangle
            </Button>
            <Button
              variant="outlined"
              className={classes.button}
              disabled={drawMode === DRAW_MODE.POLYGON}
              onClick={this.switchDrawMode(DRAW_MODE.POLYGON)}
            >
              Room - Convex Quadrangle
            </Button>
            {drawMode ? (
              <>
                <div className={classes.optionBox}>
                  <div className={classes.optionLabel}>Persist draw mode</div>
                  <Switch color="primary" checked={persistDraw} onChange={this.togglePersistDraw} />
                </div>
                <Button
                  variant="contained"
                  className={classes.button}
                  color="secondary"
                  onClick={this.switchOffDrawMode}
                >
                  Swift off draw mode
                </Button>
              </>
            ) : null}
            <Button
              variant="contained"
              className={classes.button}
              color="secondary"
              disabled={this.nothingSelected() || deleteError || selectionContainsUnsafePanoNodes}
              onClick={() => {
                onDelete({
                  edges: Object.keys(selectedEdges),
                  nodes: Object.keys(selectedNodes),
                });
              }}
            >
              Delete selected
            </Button>
            {deleteError ? (
              <Button
                variant="contained"
                className={classes.button}
                color="secondary"
                onClick={() => {
                  onResetError();
                }}
              >
                Reset error
              </Button>
            ) : null}
          </div>

          <div className={classes.menuBox}>
            <div className={classes.menuTitle}> Layers </div>
            <div className={classes.optionBox}>
              <input type="checkbox" checked={displayText} onChange={onToggleDisplayText} />
              <div className={classes.optionLabel}>Text</div>
            </div>
            <div className={classes.optionBox}>
              <input type="checkbox" checked={displayEdge} onChange={onToggleDisplayEdge} />
              <div className={classes.optionLabel}>Edges</div>
            </div>
          </div>

          <div className={classes.menuBox}>
            <div className={classes.menuTitle}>Positioning</div>
            <div className={classes.menuContent}>
              <div className={classes.optionBox}>
                You can use ← ↑ → ↓ to move selected node position
              </div>
              <div className={classes.optionBox}>
                <div className={classes.optionLabel}>Precision</div>
                <input
                  className={classes.precisionInput}
                  type="number"
                  value={precision}
                  onChange={this.onPrecisionChange}
                />
              </div>
              <div className={classes.optionBox}>
                <input
                  type="checkbox"
                  checked={isPolygonMovable}
                  onChange={this.toggleIsPolygonMovable}
                />
                <div className={classes.optionLabel}>Move door point + polygon</div>
              </div>
            </div>
          </div>

          <div className={classes.menuBox}>
            <div className={classes.menuTitle}> Legends </div>
            <div className={classes.optionBox}>
              <span className={classes.rect} style={{ backgroundColor: 'black' }} />
              <div className={classes.optionLabel}>Nodes</div>
            </div>
            <div className={classes.optionBox}>
              <span className={classes.rect} style={{ backgroundColor: PANO_COLOR }} />
              <div className={classes.optionLabel}>Nodes with panorama and has edges connected</div>
            </div>
            <div className={classes.optionBox}>
              <span className={classes.circle} style={{ backgroundColor: PANO_COLOR }} />
              <div className={classes.optionLabel}>Nodes with panorama and no edges connected</div>
            </div>
            <div className={classes.optionBox}>
              <span className={classes.rect} style={{ backgroundColor: SELECTED_COLOR }} />
              <div className={classes.optionLabel}>Selected nodes</div>
            </div>
            <div className={classes.optionBox}>
              <span className={classes.rect} style={{ backgroundColor: ERROR_COLOR }} />
              <div className={classes.optionLabel}>Error nodes</div>
            </div>
            <div className={classes.optionBox}>
              <div
                className={classes.optionLabel}
                style={{ color: selectionContainsUnsafePanoNodes ? 'red' : 'inherit' }}
              >
                Nodes with panorama and with edges connected are not allowed to be deleted or have
                its associated panorama deleted before all edges connected to it are deleted in
                FLOOR PLAN - PANORAMA page.
              </div>
            </div>
          </div>

          <div className={classes.menuBox}>
            <div className={classes.menuTitle}> Save &amp; Reset </div>
            <div className={classes.menuContent}>
              <Button
                variant="contained"
                className={classes.button}
                color="secondary"
                disabled={!isDirty || saving}
                onClick={onSave}
              >
                {saving ? 'Saving...' : 'Save changes'}
              </Button>

              {saveError ? (
                <Button
                  variant="contained"
                  className={classes.button}
                  color="secondary"
                  onClick={() => {
                    onResetError();
                  }}
                >
                  Reset error
                </Button>
              ) : null}
            </div>
          </div>
        </div>
        <div className={classes.floatMenu}>
          <RoomInfoMenu
            showRoomInfoMenu={showRoomInfoMenu}
            toggleRoomInfoMenu={this.toggleRoomInfoMenu}
            tagIds={tagIds}
            tagData={tagData}
            connectorIds={connectorIds}
            room={selectedType === NODE_TYPE.POLYGON ? selectedNode : null}
            uploadingImage={uploadingItemImages[selectedId]}
            uploadImageSuccess={uploadItemImagesSuccess[selectedId]}
            uploadImageError={uploadItemImagesError[selectedId]}
            onNodeInputChange={this.onNodeInputChange}
            onSetDoor={this.switchDrawMode(DRAW_MODE.DOOR)}
            onRemoveDoor={this.onRemoveDoor}
            onAddSubPolygon={this.switchDrawMode(DRAW_MODE.SUB_POLYGON)}
            onRemoveSubPolygon={this.onRemoveSubPolygon}
            onImageChange={this.onItemImageChange}
            onDeleteImage={this.onDeleteImage}
          />
          <PanoInfoMenu
            showPanoInfoMenu={showPanoInfoMenu}
            togglePanoInfoMenu={this.togglePanoInfoMenu}
            point={selectedType === NODE_TYPE.POINT ? selectedNode : null}
            uploadingImage={uploadingPanoImages[selectedId]}
            uploadImageSuccess={uploadPanoImagesSuccess[selectedId]}
            uploadImageError={uploadPanoImagesError[selectedId]}
            onImageChange={this.onPanoImageSubmit}
            onDeleteImage={this.onDeletePanoImage}
            disableDelete={selectionContainsUnsafePanoNodes}
          />
        </div>
        <div ref={this.canvasRootRef} className={classes.canvasRoot} />
        <AutoStatusBar
          open={uploadImageSuccess}
          variant="success"
          message="Image uploaded successfully"
        />
        <AutoStatusBar
          open={Boolean(uploadImageError)}
          variant="error"
          message={(uploadImageError || {}).message}
        />
        <AutoStatusBar
          open={deleteError}
          variant="error"
          message="Not all selected items are deleted because some of them are still connected. See items painted in red."
        />
        <AutoStatusBar open={deleteSuccess} variant="success" message="Selected items deleted" />
        <AutoStatusBar
          open={saveError}
          variant="error"
          message="Not all modified items are saved successfully. See items painted in red."
        />
        <AutoStatusBar open={saveSuccess} variant="success" message="All modified items saved" />
      </>
    );
  }
}
const ErrorPropType = PropTypes.shape({ message: PropTypes.string.isRequired });

FloorPlan.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  floorId: PropTypes.string.isRequired,
  floorData: PropTypes.shape({
    startX: PropTypes.number.isRequired,
    startY: PropTypes.number.isRequired,
  }).isRequired,
  mapImageSrc: PropTypes.string.isRequired,
  uploadingImage: PropTypes.bool.isRequired,
  uploadImageSuccess: PropTypes.bool.isRequired,
  uploadImageError: ErrorPropType,
  uploadingItemImages: PropTypes.objectOf(PropTypes.bool).isRequired,
  uploadItemImagesSuccess: PropTypes.objectOf(PropTypes.bool).isRequired,
  uploadItemImagesError: PropTypes.objectOf(ErrorPropType).isRequired,
  uploadingPanoImages: PropTypes.objectOf(PropTypes.bool).isRequired,
  uploadPanoImagesSuccess: PropTypes.objectOf(PropTypes.bool).isRequired,
  uploadPanoImagesError: PropTypes.objectOf(ErrorPropType).isRequired,
  displayText: PropTypes.bool.isRequired,
  displayEdge: PropTypes.bool.isRequired,
  deleteError: PropTypes.bool.isRequired,
  deleteSuccess: PropTypes.bool.isRequired,
  onImageChange: PropTypes.func.isRequired,
  nodeIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  nodeData: PropTypes.objectOf(nodePropType.isRequired).isRequired,
  edgeIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  edgeData: PropTypes.objectOf(
    PropTypes.shape({
      floorId: PropTypes.string.isRequired,
      fromNodeId: PropTypes.string.isRequired,
      toNodeId: PropTypes.string.isRequired,
      weightType: PropTypes.oneOf(Object.values(WEIGHT_TYPE)).isRequired,
    }),
  ).isRequired,
  tagIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  tagData: PropTypes.objectOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      imageUrl: PropTypes.string,
    }),
  ).isRequired,
  connectorIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedEdges: PropTypes.objectOf(PropTypes.bool).isRequired,
  selectedNodes: PropTypes.objectOf(PropTypes.bool).isRequired,
  errorEdges: PropTypes.objectOf(PropTypes.bool).isRequired,
  errorNodes: PropTypes.objectOf(PropTypes.bool).isRequired,
  dirtyNodes: PropTypes.objectOf(PropTypes.oneOf(Object.values(ACTION)).isRequired).isRequired,
  isDirty: PropTypes.bool.isRequired,
  saving: PropTypes.bool.isRequired,
  saveSuccess: PropTypes.bool.isRequired,
  saveError: PropTypes.bool.isRequired,
  onSelect: PropTypes.func.isRequired,
  onDeselectEdges: PropTypes.func.isRequired,
  onDeselectNodes: PropTypes.func.isRequired,
  onToggleDisplayText: PropTypes.func.isRequired,
  onToggleDisplayEdge: PropTypes.func.isRequired,
  onUpsertNode: PropTypes.func.isRequired,
  onUpsertEdge: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onResetError: PropTypes.func.isRequired,
  onItemImageChange: PropTypes.func.isRequired,
  onPanoImageSubmit: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  unsafePanoNodeIdSet: PropTypes.instanceOf(Set).isRequired,
  selectionContainsUnsafePanoNodes: PropTypes.bool.isRequired,
};

export default withStyles(styles)(FloorPlan);
