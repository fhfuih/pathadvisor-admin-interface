import getPointShape from './getPointShape';

const getId = {
  shape: (id, number) => `${id}_shape_${number}`,
  point: id => `${id}_point`,
  name: id => `${id}_name`,
};

export function getNodeShapeIds(id, node) {
  const { geoLocs } = node;

  const shapes =
    geoLocs && geoLocs.type === 'MultiPolygon'
      ? (geoLocs.coordinates || []).map((unused, index) => getId.shape(id, index))
      : [];

  return [...shapes, getId.point(id), getId.name(id)];
}

function getNodeShape({
  id,
  node,
  colors = {},
  onClick = null,
  onClickPolygon = null,
  displayText = false,
  circle = false,
}) {
  const { name, geoLocs, coordinates, centerCoordinates = coordinates, floorId } = node;

  const polygons =
    geoLocs && geoLocs.type === 'MultiPolygon'
      ? (geoLocs.coordinates || []).map(([innerPolygon], index) => {
          const [refX, refY] = innerPolygon[0];

          return {
            id: getId.shape(id, index),
            floor: floorId,
            x: refX,
            y: refY,
            shape: {
              coordinates: [...innerPolygon.map(([x, y]) => [x - refX, y - refY]), [0, 0]],
              strokeStyle: colors.shape || 'black',
              width: 1,
              cap: 'square',
            },
            ...(onClickPolygon ? { onClick: onClickPolygon } : {}),
          };
        })
      : [];

  const points = coordinates
    ? [
        getPointShape({
          id: getId.point(id),
          floorId,
          coordinates,
          rect: {
            width: 5,
            height: 5,
            color: colors.point,
            customHitWidth: 8,
            customHitHeight: 8,
            ...(circle ? { radius: 2.5 } : {}),
          },
          zIndex: 1,
          onClick,
        }),
      ]
    : [];

  const names =
    name && displayText
      ? [
          {
            id: getId.name(id),
            floor: floorId,
            x: centerCoordinates[0],
            y: centerCoordinates[1],
            center: true,
            textElement: {
              style: '10px',
              color: colors.text || 'black',
              text: name,
            },
          },
        ]
      : [];

  return [...points, ...polygons, ...names];
}

export default getNodeShape;
