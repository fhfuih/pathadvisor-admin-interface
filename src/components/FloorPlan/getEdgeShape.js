function normalize(v) {
  const length = Math.sqrt(v[0] ** 2 + v[1] ** 2);
  return [v[0] / length, v[1] / length];
}

function add(a, b) {
  return [a[0] + b[0], a[1] + b[1]];
}

function sub(a, b) {
  return [a[0] - b[0], a[1] - b[1]];
}

function inverse(v) {
  return [v[1], -v[0]];
}

function dot(k, v) {
  return [k * v[0], k * v[1]];
}

const getId = {
  indicator: id => `${id}_edge_direction_indicator`,
  edge: id => `${id}_edge`,
};

export function getEdgeShapeIds(id) {
  return Object.values(getId).map(fn => fn(id));
}

function getEdgeShape({ id, edge, startNode, endNode, colors = {}, onClick = null }) {
  const distance = 5;
  const directionVector = normalize(inverse(sub(endNode.coordinates, startNode.coordinates)));

  return [
    {
      id: getId.indicator(id),
      floor: edge.floorId,
      line: {
        coordinates: [
          add(endNode.coordinates, dot(distance, directionVector)),
          add(endNode.coordinates, dot(-distance, directionVector)),
        ],
        strokeStyle: colors.indicator || '#047223',
        cap: 'butt',
        width: 1,
      },
    },
    {
      id: getId.edge(id),
      floor: edge.floorId,
      line: {
        coordinates: [startNode.coordinates, endNode.coordinates],
        strokeStyle: colors.line || 'black',
        cap: 'butt',
        width: 1,
      },
      ...(onClick ? { onClick } : {}),
    },
  ];
}

export default getEdgeShape;
