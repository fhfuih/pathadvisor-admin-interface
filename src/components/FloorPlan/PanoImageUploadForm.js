import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  hide: {
    display: 'none',
  },
  fileNameIndicator: {
    display: 'inline',
    marginLeft: '1rem',
  },
};

class PanoImageUploadForm extends Component {
  state = {
    name: '',
    fileNameIndicator: '',
  };

  onClose = () => {
    const { onClose: parentOnClose } = this.props;
    parentOnClose();
    this.setState({ name: '', fileNameIndicator: '' });
  }

  onFileChange = e => {
    const file = e.target.value;
    const name = file
      .split('\\')
      .pop()
      .split('/')
      .pop()
      .split('.')
      .shift();
    this.setState({ fileNameIndicator: file, name });
  };

  onNameChange = e => {
    this.setState({ name: e.target.value });
  };

  render() {
    const { classes, open, onSubmit } = this.props;
    const { name, fileNameIndicator } = this.state;

    return (
      <Dialog
        open={open}
        onClose={this.onClose}
        aria-labelledby="pano-upload-form-title"
        disableBackdropClick
        disableEscapeKeyDown
      >
        <DialogTitle id="pano-upload-form-title">Upload Panorama</DialogTitle>
        <form action="#" onSubmit={onSubmit}>
          <DialogContent>
            <DialogContentText paragraph>
              Please chooose a 360-degree panorama file, and input the x-coordinate of the image
              indicating the geographical west direction, and the name of the image (default to the
              original filename if not presented).
            </DialogContentText>
            <input
              id="pano-upload-form-file-input"
              name="data"
              type="file"
              accept="image/*"
              onChange={this.onFileChange}
              hidden
              required
            />
            <label htmlFor="pano-upload-form-file-input">
              <Button margin="dense" variant="contained" component="span">
                Browse...
              </Button>
              <Typography
                className={classes.fileNameIndicator}
                variant="caption"
                component="span"
                color="textSecondary"
                noWrap
              >
                {fileNameIndicator}
              </Typography>
            </label>
            <TextField
              margin="dense"
              id="pano-upload-form-field-westx"
              name="westX"
              label="West X Coord"
              type="number"
              required
              fullWidth
            />
            <TextField
              margin="dense"
              id="pano-upload-form-field-name"
              name="name"
              label="Name"
              value={name}
              onChange={this.onNameChange}
              required
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.onClose}>Cancel</Button>
            <Button color="primary" type="submit" disabled={!fileNameIndicator || !open}>
              Submit
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    );
  }
}

PanoImageUploadForm.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default withStyles(styles)(PanoImageUploadForm);
