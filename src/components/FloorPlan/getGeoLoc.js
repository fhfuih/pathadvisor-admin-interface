export const GEO_LOC_RECTANGLE = 'GEO_LOC_RECTANGLE';
export const GEO_LOC_POLYGON = 'GEO_LOC_POLYGON';

function getGeoLoc(shape, points, baseGeoLoc = { type: 'MultiPolygon', coordinates: [] }) {
  switch (shape) {
    case GEO_LOC_RECTANGLE: {
      const [{ x: x1, y: y1 }, { x: x2, y: y2 }] = points;
      return {
        ...baseGeoLoc,
        coordinates: [...baseGeoLoc.coordinates, [[[x1, y1], [x2, y1], [x2, y2], [x1, y2]]]],
      };
    }
    case GEO_LOC_POLYGON: {
      return {
        ...baseGeoLoc,
        coordinates: [...baseGeoLoc.coordinates, [points.map(({ x, y }) => [x, y])]],
      };
    }
    default:
      throw new TypeError(
        `shape must be one of [${GEO_LOC_RECTANGLE},${GEO_LOC_POLYGON}], ${shape} received`,
      );
  }
}

export default getGeoLoc;
