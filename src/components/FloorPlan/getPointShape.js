const DEFAULT_WIDTH = 2;
const DEFAULT_HEIGHT = 2;
const DEFAULT_COLOR = 'black';

function getPointShape({ id, floorId, coordinates, rect = {}, zIndex = null, onClick = null }) {
  return {
    id,
    floor: floorId,
    x: coordinates[0],
    y: coordinates[1],
    center: true,
    ...(rect.radius
      ? { circle: { radius: rect.radius, color: rect.color || DEFAULT_COLOR } }
      : {
          rect: {
            width: rect.width || DEFAULT_WIDTH,
            height: rect.height || DEFAULT_HEIGHT,
            color: rect.color || DEFAULT_COLOR,
          },
        }),
    ...(zIndex ? { zIndex } : {}),
    ...(rect.customHitWidth ? { customHitWidth: rect.customHitWidth } : {}),
    ...(rect.customHitHeight ? { customHitHeight: rect.customHitHeight } : {}),
    ...(onClick ? { onClick } : {}),
  };
}

export default getPointShape;
