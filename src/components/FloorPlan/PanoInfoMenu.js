import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DropDownIcon from '@material-ui/icons/ArrowDropDown';
import DropUpIcon from '@material-ui/icons/ArrowDropUp';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';
import PanoImageUploadForm from './PanoImageUploadForm';
import nodePropType from './nodePropType';

const styles = {
  panoInfoMenu: {
    backgroundColor: 'whitesmoke',
    width: '100%',
  },
  panoInfoMenuHead: {
    display: 'flex',
    alignItems: 'center',
  },
  arrowButton: {
    padding: '3px',
    minWidth: 0,
  },
  arrowIcon: {
    fontSize: '14px',
  },
  menuTitle: {
    fontSize: '12px',
    padding: '3px 10px',
    color: 'white',
    backgroundColor: '#333',
    fontWeight: 'bold',
    flex: 1,
  },
  button: {
    padding: '5px',
    fontSize: '10px',
    margin: '3px',
    minWidth: 0,
  },
  hide: { display: 'none' },
};

class PanoInfoMenu extends Component {
  state = {
    formOpen: false,
  };

  onFormClose = () => {
    this.setState({ formOpen: false });
  };

  onFormOpen = () => {
    this.setState({ formOpen: true });
  };

  onImageChange = e => {
    e.preventDefault();
    this.onFormClose();
    this.props.onImageChange(e);
  };

  renderBody() {
    const {
      classes,
      point,
      uploadingImage,
      uploadImageSuccess,
      uploadImageError,
      onDeleteImage,
      disableDelete,
    } = this.props;
    const { formOpen } = this.state;

    if (!point) {
      return <div>Please select a point</div>;
    }

    return (
      <div>
        {point.panoImage ? (
          <Button
            variant="outlined"
            className={classes.button}
            onClick={onDeleteImage}
            disabled={disableDelete}
          >
            Delete Panorama
          </Button>
        ) : (
          <Button
            variant="outlined"
            className={classes.button}
            onClick={this.onFormOpen}
            disabled={uploadingImage}
          >
            Upload Panorama
          </Button>
        )}

        {point.panoImage && (
          <Button
            variant="outlined"
            className={classes.button}
            href={point.panoImageUrl}
            target="_blank"
          >
            View Panorama
          </Button>
        )}

        <PanoImageUploadForm
          open={formOpen && !uploadingImage}
          onClose={this.onFormClose}
          onSubmit={this.onImageChange}
        />

        <AutoStatusBar
          open={Boolean(uploadImageSuccess)}
          variant="success"
          message="Panorama uploaded successfully"
        />
        <AutoStatusBar
          open={Boolean(uploadImageError)}
          variant="error"
          message={(uploadImageError || {}).message}
        />
      </div>
    );
  }

  render() {
    const { classes, showPanoInfoMenu, togglePanoInfoMenu } = this.props;

    return (
      <div className={classes.panoInfoMenu}>
        <div className={classes.panoInfoMenuHead}>
          <Button className={classes.arrowButton} onClick={togglePanoInfoMenu}>
            {showPanoInfoMenu ? (
              <DropUpIcon className={classes.arrowIcon} />
            ) : (
              <DropDownIcon className={classes.arrowIcon} />
            )}
          </Button>
          <div className={classes.menuTitle}> Panorama information </div>
        </div>

        {showPanoInfoMenu && this.renderBody()}
      </div>
    );
  }
}

PanoInfoMenu.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  showPanoInfoMenu: PropTypes.bool.isRequired,
  togglePanoInfoMenu: PropTypes.func.isRequired,
  point: nodePropType,
  uploadingImage: PropTypes.bool,
  uploadImageSuccess: PropTypes.bool,
  uploadImageError: PropTypes.shape({ message: PropTypes.string.isRequired }),
  onImageChange: PropTypes.func.isRequired,
  onDeleteImage: PropTypes.func.isRequired,
  disableDelete: PropTypes.bool.isRequired,
};

export default withStyles(styles)(PanoInfoMenu);
