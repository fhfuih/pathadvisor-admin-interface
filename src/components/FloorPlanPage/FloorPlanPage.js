import React, { Component } from 'react';
import PropTypes from 'prop-types';
import omit from 'lodash.omit';
import omitBy from 'lodash.omitby';
import without from 'lodash.without';
import isEmpty from 'lodash.isempty';
import FloorPlan from '../FloorPlan/FloorPlan';
import * as nodesService from '../../services/nodes/nodes';
import * as edgesService from '../../services/edges/edges';
import * as connectorsService from '../../services/connectors/connectors';
import * as tagsService from '../../services/tags/tags';
import * as mapImagesService from '../../services/mapImages/mapImages';
import * as imagesService from '../../services/images/images';
import * as panoImagesService from '../../services/panoImages/panoImages';
import * as panoEdgesService from '../../services/panoEdges/panoEdges';
import * as floorsService from '../../services/floors/floors';
import Loading from '../Loading/Loading';
import Error from '../Error/Error';
import normalizeData from '../../services/normalizeData';
import getCenterCoordinates from '../FloorPlan/getCenterCoordinates';
import { ACTION } from './constants';

const ITEM_TYPE = {
  NODE: 'NODE',
  EDGE: 'EDGE',
};

function sanitizeNode(node) {
  const { imageUrl, panoImageUrl, centerCoordinates, ...preserved } = node;
  const {
    connectorId = '',
    coordinates = [],
    name = '',
    geoLocs,
    url = '',
    keywords = [],
    tagIds = [],
  } = preserved;
  const sanitizedKeywords =
    keywords && keywords.length ? keywords.map(v => v.trim()).filter(v => v) : [];
  const sanitizedTagIds = tagIds && tagIds.length ? tagIds.map(v => v.trim()).filter(v => v) : [];
  return {
    ...preserved,
    name: (name && name.trim()) || null,
    coordinates: coordinates && coordinates.length ? coordinates.map(v => parseInt(v, 10)) : null,
    geoLocs: geoLocs
      ? {
          ...geoLocs,
          coordinates: geoLocs.coordinates.map(([innerPolygon]) => [
            innerPolygon.map(point => point.map(v => parseInt(v, 10))),
          ]),
        }
      : null,
    url: (url && url.trim()) || null,
    keywords: sanitizedKeywords.length ? sanitizedKeywords : null,
    tagIds: sanitizedTagIds.length ? sanitizedTagIds : null,
    connectorId: connectorId || null,
  };
}

function sanitizeEdge(edge, nodeIdToNewNodeId) {
  const { fromNodeId, toNodeId } = edge;
  return {
    ...edge,
    fromNodeId: nodeIdToNewNodeId[fromNodeId] || fromNodeId,
    toNodeId: nodeIdToNewNodeId[toNodeId] || toNodeId,
  };
}

function getTaskOrder(task) {
  if (task.action === ACTION.DELETE) {
    if (task.type === ITEM_TYPE.EDGE) {
      return 1;
    }
    return 2;
  }
  if (task.type === ITEM_TYPE.NODE) {
    return 3;
  }
  return 4;
}

class FloorPlanPage extends Component {
  state = {
    loading: true,
    loadError: null,
    nodeIds: [],
    nodeData: {},
    edgeIds: [],
    dirtyNodes: {},
    dirtyEdges: {},
    edgeData: {},
    connectorIds: [],
    connectorData: {},
    tagIds: [],
    tagData: {},
    uploadingImage: false,
    uploadImageSuccess: false,
    uploadImageError: null,
    uploadingItemImages: {},
    uploadItemImagesSuccess: {},
    uploadItemImagesError: {},
    uploadingPanoImages: {},
    uploadPanoImagesSuccess: {},
    uploadPanoImagesError: {},
    selectedEdges: {},
    selectedNodes: {},
    errorEdges: {},
    errorNodes: {},
    deleteSuccess: false,
    deleteError: false,
    displayText: true,
    displayEdge: true,
    saving: false,
    saveSuccess: false,
    saveError: false,
    unsafePanoNodeIdSet: null,
    selectionContainsUnsafePanoNodes: false,
  };

  async componentDidMount() {
    const floorId = this.props.match.params.floorId;
    this.setState({ loading: true });

    const [
      { data: nodes, error: getNodesError },
      { data: edges, error: getEdgesError },
      { data: connectors, error: getConnectorsError },
      { data: tags, error: getTagsError },
      { data: floorData, error: getFloorError },
      { data: panoEdges, error: getPanoEdgesError },
    ] = await Promise.all([
      nodesService.get(floorId),
      edgesService.get(floorId),
      connectorsService.get(),
      tagsService.get(),
      floorsService.getOne(floorId),
      panoEdgesService.get(floorId),
    ]);

    if (
      getNodesError ||
      getEdgesError ||
      getConnectorsError ||
      getTagsError ||
      getFloorError ||
      getPanoEdgesError
    ) {
      this.setState({ loading: false, loadError: true });
      return;
    }

    const { ids: nodeIds, data: nodeData } = normalizeData(nodes);
    const { ids: edgeIds, data: edgeData } = normalizeData(edges);
    const { ids: connectorIds, data: connectorData } = normalizeData(connectors);
    const { ids: tagIds, data: tagData } = normalizeData(tags);
    const unsafePanoNodeIds = panoEdges.reduce(
      (carrier, panoEdge) => [...carrier, panoEdge.fromNodeId, panoEdge.toNodeId],
      [],
    );
    const unsafePanoNodeIdSet = new Set(unsafePanoNodeIds);
    console.log(unsafePanoNodeIdSet);

    this.setState({
      loading: false,
      nodeIds,
      nodeData,
      edgeIds,
      edgeData,
      connectorIds: connectorIds.sort(),
      connectorData,
      tagIds,
      tagData,
      floorData,
      unsafePanoNodeIdSet,
    });
  }

  onImageChange = async e => {
    const floorId = this.props.match.params.floorId;

    if (!e.target.files.length) {
      return;
    }

    const file = e.target.files[0];

    this.setState({
      uploadingImage: true,
      uploadImageSuccess: false,
      uploadImageError: null,
    });

    const { error } = await mapImagesService.uploadImage(floorId, file);

    this.setState({
      uploadingImage: false,
      uploadImageSuccess: !error,
      uploadImageError: error,
    });
  };

  onItemImageChange = async (id, e) => {
    if (!e.target.files.length) {
      return;
    }

    this.setState(state => ({
      uploadingItemImages: {
        ...state.uploadingItemImages,
        [id]: true,
      },
      uploadItemImagesSuccess: {
        ...state.uploadItemImagesSuccess,
        [id]: null,
      },
      uploadItemImagesError: {
        ...state.uploadItemImagesError,
        [id]: null,
      },
    }));

    const file = e.target.files[0];
    const { data, error } = await imagesService.uploadImage(file);

    if (!error && (!data || !data._id)) {
      throw new TypeError('Image id is missing from response');
    }

    this.setState(state => ({
      uploadingItemImages: {
        ...state.uploadingItemImages,
        [id]: false,
      },
      uploadItemImagesSuccess: {
        ...state.uploadItemImagesSuccess,
        [id]: !error,
      },
      uploadItemImagesError: {
        ...state.uploadItemImagesError,
        [id]: error,
      },
    }));

    this.onUpsertNode(id, {
      ...this.state.nodeData[id],
      image: data._id,
      imageUrl: data.imageUrl,
    });
  };

  onPanoImageSubmit = async (id, e) => {
    const form = e.target && new FormData(e.target);

    if (!form) {
      return;
    }

    this.setState(state => ({
      uploadingPanoImages: {
        ...state.uploadingPanoImages,
        [id]: true,
      },
      uploadPanoImagesSuccess: {
        ...state.uploadPanoImagesSuccess,
        [id]: null,
      },
      uploadPanoImagesError: {
        ...state.uploadPanoImagesError,
        [id]: null,
      },
    }));

    const { data, error } = await panoImagesService.uploadPanoImage(form);

    if (!error && (!data || !data._id)) {
      throw new TypeError('Image id is missing from response');
    }

    this.setState(state => ({
      uploadingPanoImages: {
        ...state.uploadingPanoImages,
        [id]: false,
      },
      uploadPanoImagesSuccess: {
        ...state.uploadPanoImagesSuccess,
        [id]: !error,
      },
      uploadPanoImagesError: {
        ...state.uploadPanoImagesError,
        [id]: error,
      },
    }));

    if (error) {
      return;
    }

    this.onUpsertNode(id, {
      ...this.state.nodeData[id],
      panoImage: data._id,
      panoImageUrl: data.panoImageUrl,
    });
  };

  findReverseEdgeIds = ids => {
    const { edgeIds, edgeData } = this.state;
    const reverseIds = [];

    ids.forEach(id => {
      const edge = edgeData[id];
      edgeIds.forEach(edgeId => {
        const nextEdge = edgeData[edgeId];
        if (
          edgeId === id ||
          edge.fromNodeId !== nextEdge.toNodeId ||
          edge.toNodeId !== nextEdge.fromNodeId
        ) {
          return;
        }

        reverseIds.push(edgeId);
      });
    });

    return reverseIds;
  };

  onSelect = ({ edges, nodes }) => {
    if (!edges && !nodes) {
      return;
    }

    if ((edges && !Array.isArray(edges.ids)) || (nodes && !Array.isArray(nodes.ids))) {
      throw new TypeError('Argument must be an array');
    }

    const reverseIds = edges && edges.ids ? this.findReverseEdgeIds(edges.ids) : [];

    this.setState(state => ({
      ...(edges
        ? {
            selectedEdges: {
              ...(edges.merge ? state.selectedEdges : {}),
              ...edges.ids.reduce((carrier, id) => ({ ...carrier, [id]: true }), {}),
              ...reverseIds.reduce((carrier, id) => ({ ...carrier, [id]: true }), {}),
            },
          }
        : {}),
      ...(nodes
        ? {
            selectedNodes: {
              ...(nodes.merge ? state.selectedNodes : {}),
              ...nodes.ids.reduce((carrier, id) => ({ ...carrier, [id]: true }), {}),
            },
          }
        : {}),
      selectionContainsUnsafePanoNodes:
        (nodes.merge && state.selectionContainsUnsafePanoNodes) ||
        nodes.ids.some(id => state.unsafePanoNodeIdSet.has(id)),
    }));
  };

  onDeselectEdges = ids => {
    if (!Array.isArray(ids)) {
      throw new TypeError('Argument must be an array');
    }

    const reverseIds = this.findReverseEdgeIds(ids);

    this.setState(state => ({
      selectedEdges: omit(state.selectedEdges, ids, reverseIds),
    }));
  };

  onDeselectNodes = ids => {
    if (!Array.isArray(ids)) {
      throw new TypeError('Argument must be an array');
    }

    this.setState(state => ({
      selectedNodes: omit(state.selectedNodes, ids),
    }));
  };

  onToggleDisplayText = () => {
    this.setState(state => ({
      displayText: !state.displayText,
    }));
  };

  onToggleDisplayEdge = () => {
    this.setState(state => ({
      displayEdge: !state.displayEdge,
    }));
  };

  onDelete = async ({ nodes = [], edges = [], doors = [], images = [], panoImages = [] }) => {
    const edgeSet = new Set(edges);
    const { edgeData, edgeIds, nodeData } = this.state;

    // force no batch setState
    await this.setState({ deleteError: false, deleteSuccess: false });

    const connectedNodes = edgeIds.reduce((carrier, id) => {
      if (edgeSet.has(id)) {
        return carrier;
      }

      const { fromNodeId, toNodeId } = edgeData[id];
      if (fromNodeId) carrier[fromNodeId] = true;
      if (toNodeId) carrier[toNodeId] = true;
      return carrier;
    }, {});

    const safeNodes = nodes.filter(id => !connectedNodes[id]);
    const safeNodeSet = new Set(safeNodes);
    const safeDoors = doors.filter(id => !connectedNodes[id] && nodeData[id].geoLocs);
    const errorNodes = [
      ...nodes.filter(id => connectedNodes[id]),
      ...doors.filter(id => connectedNodes[id] && nodeData[id].geoLocs),
    ];

    if (
      !safeNodes.length &&
      !edges.length &&
      !safeDoors.length &&
      !images.length &&
      !panoImages.length &&
      !errorNodes.length
    ) {
      return;
    }

    this.setState(state => ({
      ...(edges.length
        ? {
            dirtyEdges: {
              ...omitBy(
                state.dirtyEdges,
                (action, id) => edgeSet.has(id) && action === ACTION.INSERT,
              ),
              ...edges.reduce(
                (carrier, id) => ({
                  ...carrier,
                  ...(state.dirtyEdges[id] !== ACTION.INSERT ? { [id]: ACTION.DELETE } : {}),
                }),
                {},
              ),
            },
            edgeIds: without(state.edgeIds, ...edges),
            // edges will always be deleted successfully
            selectedEdges: {},
          }
        : {}),
      ...(safeNodes.length
        ? {
            dirtyNodes: {
              ...omitBy(
                state.dirtyNodes,
                (action, id) => safeNodeSet.has(id) && action === ACTION.INSERT,
              ),
              ...safeNodes.reduce(
                (carrier, id) => ({
                  ...carrier,
                  ...(state.dirtyNodes[id] !== ACTION.INSERT ? { [id]: ACTION.DELETE } : {}),
                }),
                {},
              ),
            },
            nodeIds: without(state.nodeIds, ...safeNodes),
            selectedNodes: omitBy(state.selectedNodes, (unused, id) => safeNodeSet.has(id)),
          }
        : {}),
      ...(safeDoors.length
        ? {
            dirtyNodes: {
              ...state.dirtyNodes,
              ...safeDoors.reduce(
                (carrier, id) => ({
                  ...carrier,
                  ...(state.dirtyNodes[id] === ACTION.INSERT
                    ? { [id]: ACTION.INSERT }
                    : { [id]: ACTION.UPDATE }),
                }),
                {},
              ),
            },
            nodeData: {
              ...state.nodeData,
              ...safeDoors.reduce(
                (carrier, id) => ({
                  ...carrier,
                  [id]: { ...state.nodeData[id], coordinates: null },
                }),
                {},
              ),
            },
          }
        : {}),
      ...(images.length
        ? {
            dirtyNodes: {
              ...state.dirtyNodes,
              ...images.reduce(
                (carrier, id) => ({
                  ...carrier,
                  ...(state.dirtyNodes[id] === ACTION.INSERT
                    ? { [id]: ACTION.INSERT }
                    : { [id]: ACTION.UPDATE }),
                }),
                {},
              ),
            },
            nodeData: {
              ...state.nodeData,
              ...images.reduce(
                (carrier, id) => ({
                  ...carrier,
                  [id]: { ...state.nodeData[id], image: null, imageUrl: null },
                }),
                {},
              ),
            },
          }
        : {}),
      ...(panoImages.length
        ? {
            dirtyNodes: {
              ...state.dirtyNodes,
              ...panoImages.reduce(
                (carrier, id) => ({
                  ...carrier,
                  ...(state.dirtyNodes[id] === ACTION.INSERT
                    ? { [id]: ACTION.INSERT }
                    : { [id]: ACTION.UPDATE }),
                }),
                {},
              ),
            },
            nodeData: {
              ...state.nodeData,
              ...panoImages.reduce(
                (carrier, id) => ({
                  ...carrier,
                  [id]: { ...state.nodeData[id], panoImage: null, panoImageUrl: null },
                }),
                {},
              ),
            },
          }
        : {}),
      errorNodes: errorNodes.reduce((carrier, id) => ({ ...carrier, [id]: true }), {}),
      deleteError: !isEmpty(errorNodes),
      deleteSuccess: isEmpty(errorNodes),
    }));
  };

  onUpsertNode = (id, node) => {
    this.setState(state => ({
      dirtyNodes: {
        ...state.dirtyNodes,
        [id]:
          !state.nodeData[id] || state.dirtyNodes[id] === ACTION.INSERT
            ? ACTION.INSERT
            : ACTION.UPDATE,
      },
      nodeIds: [...state.nodeIds, ...(!state.nodeData[id] ? [id] : [])],
      nodeData: {
        ...state.nodeData,
        [id]: {
          ...node,
          ...(node.geoLocs && !state.nodeData[id] ? getCenterCoordinates(node.geoLocs) : {}),
        },
      },
    }));
  };

  onUpsertEdge = (id, edge) => {
    this.setState(state => ({
      dirtyEdges: {
        ...state.dirtyEdges,
        [id]:
          !state.edgeData[id] || state.dirtyEdges[id] === ACTION.INSERT
            ? ACTION.INSERT
            : ACTION.UPDATE,
      },
      edgeIds: [...state.edgeIds, ...(!state.edgeData[id] ? [id] : [])],
      edgeData: {
        ...state.edgeData,
        [id]: {
          ...edge,
        },
      },
    }));
  };

  onResetError = () => {
    this.setState({
      saveError: false,
      deleteError: false,
      errorNodes: {},
      errorEdges: {},
    });
  };

  onSave = async () => {
    this.setState({
      saving: true,
      saveSuccess: false,
      saveError: false,
    });

    const tasks = [];
    const { dirtyNodes, nodeData, dirtyEdges, edgeData } = this.state;
    const cleanIds = { [ITEM_TYPE.EDGE]: [], [ITEM_TYPE.NODE]: [] };
    const errors = { [ITEM_TYPE.EDGE]: {}, [ITEM_TYPE.NODE]: {} };
    const oldIdsToNewIds = { [ITEM_TYPE.EDGE]: {}, [ITEM_TYPE.NODE]: {} };
    const deletedIds = { [ITEM_TYPE.EDGE]: [], [ITEM_TYPE.NODE]: [] };

    for (const id of Object.keys(dirtyNodes)) {
      const task = { id, action: dirtyNodes[id], type: ITEM_TYPE.NODE };
      switch (dirtyNodes[id]) {
        case ACTION.INSERT:
          task.run = () => nodesService.insert(sanitizeNode(nodeData[id]));
          break;
        case ACTION.UPDATE:
          task.run = () => nodesService.upsert(id, sanitizeNode(nodeData[id]));
          break;
        case ACTION.DELETE:
          task.run = () => nodesService.remove(id);
          break;
        default:
          throw new TypeError(
            `Action type must for node must be be one of [${ACTION.INSERT}, ${ACTION.UPDATE}, ${
              ACTION.DELETE
            }], ${dirtyNodes[id]} received`,
          );
      }

      tasks.push(task);
    }

    for (const id of Object.keys(dirtyEdges)) {
      const task = { id, action: dirtyEdges[id], type: ITEM_TYPE.EDGE };
      switch (dirtyEdges[id]) {
        case ACTION.INSERT:
          task.run = () =>
            edgesService.insert(sanitizeEdge(edgeData[id], oldIdsToNewIds[ITEM_TYPE.NODE]));
          break;
        case ACTION.DELETE:
          task.run = () => edgesService.remove(id);
          break;
        default:
          throw new TypeError(
            `Action type for edge must be one of [${ACTION.INSERT}, ${ACTION.DELETE}], ${
              dirtyEdges[id]
            } received`,
          );
      }

      tasks.push(task);
    }

    // tasks must follow the order edge:delete, node:delete, node:save, edge:save to avoid ref/no-ref errors
    tasks.sort((a, b) => getTaskOrder(a) - getTaskOrder(b));

    for (const { id, run, type, action } of tasks) {
      const response = await run();

      const { data: { _id: newId = null } = {}, error } = response;

      // newly inserted item gets a new ID
      if (newId && newId !== id) {
        oldIdsToNewIds[type][id] = newId;
      }

      if (error) {
        console.log(error);
        errors[type][id] = action;
      } else {
        cleanIds[type].push(id);

        if (action === ACTION.DELETE) {
          deletedIds[type].push(id);
        }
      }
    }

    const newNodeIds = Object.entries(oldIdsToNewIds[ITEM_TYPE.NODE]);
    const newEdgeIds = Object.entries(oldIdsToNewIds[ITEM_TYPE.EDGE]);

    this.setState(state => ({
      saving: false,
      saveSuccess: isEmpty(errors[ITEM_TYPE.NODE]) && isEmpty(errors[ITEM_TYPE.EDGE]),
      saveError: !isEmpty(errors[ITEM_TYPE.NODE]) || !isEmpty(errors[ITEM_TYPE.EDGE]),

      dirtyNodes: omit(
        state.dirtyNodes,
        ...cleanIds[ITEM_TYPE.NODE],
        // if delete fails items will be re-added and therefore remove from dirty
        ...Object.entries(errors[ITEM_TYPE.NODE])
          .filter(([, action]) => action === ACTION.DELETE)
          .map(([id]) => id),
      ),
      ...(newNodeIds.length ||
      deletedIds[ITEM_TYPE.NODE].length ||
      Object.keys(errors[ITEM_TYPE.NODE]).length
        ? {
            nodeIds: [
              ...new Set([
                ...without(state.nodeIds, ...newNodeIds.map(([oldId]) => oldId)),
                ...newNodeIds.map(([, newId]) => newId),
                ...Object.keys(errors[ITEM_TYPE.NODE]),
              ]),
            ],
            nodeData: {
              ...omit(
                state.nodeData,
                newNodeIds.map(([oldId]) => oldId),
                deletedIds[ITEM_TYPE.NODE],
              ),
              ...newNodeIds.reduce(
                (carrier, [id, newId]) => ({ ...carrier, [newId]: state.nodeData[id] }),
                {},
              ),
            },
            selectedNodes: {
              ...omit(state.selectedNodes, newNodeIds.map(([oldId]) => oldId)),
              ...newNodeIds.reduce(
                (carrier, [id, newId]) =>
                  state.selectedNodes[id]
                    ? { ...carrier, [newId]: state.selectedNodes[id] }
                    : carrier,
                {},
              ),
            },
          }
        : {}),
      errorNodes: Object.keys(errors[ITEM_TYPE.NODE]).reduce(
        (carrier, id) => ({ ...carrier, [id]: true }),
        {},
      ),

      dirtyEdges: omit(
        state.dirtyEdges,
        ...cleanIds[ITEM_TYPE.EDGE],
        // if delete fails items will be re-added and therefore remove from dirty
        ...Object.entries(errors[ITEM_TYPE.EDGE])
          .filter(([, action]) => action === ACTION.DELETE)
          .map(([id]) => id),
      ),
      ...(newEdgeIds.length ||
      newNodeIds.length ||
      deletedIds[ITEM_TYPE.EDGE].length ||
      Object.keys(errors[ITEM_TYPE.EDGE]).length
        ? {
            edgeIds: [
              ...new Set([
                ...without(state.edgeIds, ...newEdgeIds.map(([oldId]) => oldId)),
                ...newEdgeIds.map(([, newId]) => newId),
                ...Object.keys(errors[ITEM_TYPE.EDGE]),
              ]),
            ],
            edgeData: {
              ...omit(
                state.edgeData,
                newEdgeIds.map(([oldId]) => oldId),
                deletedIds[ITEM_TYPE.EDGE],
              ),
              ...newEdgeIds.reduce(
                (carrier, [id, newId]) => ({
                  ...carrier,
                  [newId]: {
                    ...state.edgeData[id],
                    fromNodeId:
                      oldIdsToNewIds[ITEM_TYPE.NODE][state.edgeData[id].fromNodeId] ||
                      state.edgeData[id].fromNodeId,
                    toNodeId:
                      oldIdsToNewIds[ITEM_TYPE.NODE][state.edgeData[id].toNodeId] ||
                      state.edgeData[id].toNodeId,
                  },
                }),
                {},
              ),
            },
            selectedEdges: {
              ...omit(state.selectedEdges, newEdgeIds.map(([oldId]) => oldId)),
              ...newEdgeIds.reduce(
                (carrier, [id, newId]) =>
                  state.selectedEdges[id]
                    ? { ...carrier, [newId]: state.selectedEdges[id] }
                    : carrier,
                {},
              ),
            },
          }
        : {}),
      errorEdges: Object.keys(errors[ITEM_TYPE.EDGE]).reduce(
        (carrier, id) => ({ ...carrier, [id]: true }),
        {},
      ),
    }));
  };

  render() {
    const {
      loading,
      loadError,
      nodeIds,
      nodeData,
      edgeIds,
      edgeData,
      connectorIds,
      connectorData,
      tagIds,
      tagData,
      floorData,
      uploadingImage,
      uploadImageSuccess,
      uploadImageError,
      uploadingItemImages,
      uploadItemImagesSuccess,
      uploadItemImagesError,
      uploadingPanoImages,
      uploadPanoImagesSuccess,
      uploadPanoImagesError,
      selectedEdges,
      selectedNodes,
      errorEdges,
      errorNodes,
      displayText,
      displayEdge,
      deleteError,
      deleteSuccess,
      dirtyEdges,
      dirtyNodes,
      saving,
      saveSuccess,
      saveError,
      unsafePanoNodeIdSet,
      selectionContainsUnsafePanoNodes,
    } = this.state;
    const floorId = this.props.match.params.floorId;

    switch (true) {
      case loading:
        return <Loading />;
      case loadError:
        return <Error message="Error retrieving data" />;
      default:
        return (
          <FloorPlan
            floorId={floorId}
            floorData={floorData}
            nodeIds={nodeIds}
            nodeData={nodeData}
            edgeIds={edgeIds}
            edgeData={edgeData}
            connectorIds={connectorIds}
            connectorData={connectorData}
            tagIds={tagIds}
            tagData={tagData}
            onImageChange={this.onImageChange}
            onItemImageChange={this.onItemImageChange}
            onPanoImageSubmit={this.onPanoImageSubmit}
            mapImageSrc={mapImagesService.getImageSrc(floorId)}
            uploadingImage={uploadingImage}
            uploadImageSuccess={uploadImageSuccess}
            uploadImageError={uploadImageError}
            uploadingItemImages={uploadingItemImages}
            uploadItemImagesSuccess={uploadItemImagesSuccess}
            uploadItemImagesError={uploadItemImagesError}
            uploadingPanoImages={uploadingPanoImages}
            uploadPanoImagesSuccess={uploadPanoImagesSuccess}
            uploadPanoImagesError={uploadPanoImagesError}
            deleteSuccess={deleteSuccess}
            deleteError={deleteError}
            selectedEdges={selectedEdges}
            selectedNodes={selectedNodes}
            errorEdges={errorEdges}
            errorNodes={errorNodes}
            onSelect={this.onSelect}
            onDeselectEdges={this.onDeselectEdges}
            onDeselectNodes={this.onDeselectNodes}
            displayText={displayText}
            displayEdge={displayEdge}
            isDirty={!isEmpty(dirtyEdges) || !isEmpty(dirtyNodes)}
            dirtyNodes={dirtyNodes}
            onToggleDisplayText={this.onToggleDisplayText}
            onToggleDisplayEdge={this.onToggleDisplayEdge}
            onUpsertNode={this.onUpsertNode}
            onUpsertEdge={this.onUpsertEdge}
            onDelete={this.onDelete}
            onResetError={this.onResetError}
            saving={saving}
            saveSuccess={saveSuccess}
            saveError={saveError}
            onSave={this.onSave}
            unsafePanoNodeIdSet={unsafePanoNodeIdSet}
            selectionContainsUnsafePanoNodes={selectionContainsUnsafePanoNodes}
          />
        );
    }
  }
}

FloorPlanPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      floorId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default FloorPlanPage;
