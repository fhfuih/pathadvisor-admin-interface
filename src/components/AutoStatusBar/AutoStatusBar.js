import React, { Component } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import PropTypes from 'prop-types';
import StatusBar from '../StatusBar/StatusBar';

class AutoStatusBar extends Component {
  state = {};

  componentDidUpdate(prevProps) {
    const { open = false } = this.props;
    const { open: prevOpen = false } = prevProps;
    if (open !== prevOpen) {
      this.setState({ open });
    }
  }

  onClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    const { open } = this.state;
    const {
      message,
      variant,
      autoHideDuration = 3000,
      anchorOrigin = {
        vertical: 'top',
        horizontal: 'center',
      },
    } = this.props;

    return (
      <Snackbar
        anchorOrigin={anchorOrigin}
        open={open}
        onClose={this.onClose}
        autoHideDuration={autoHideDuration}
      >
        <StatusBar message={message} onClose={this.onClose} variant={variant} />
      </Snackbar>
    );
  }
}

AutoStatusBar.propTypes = {
  open: PropTypes.bool,
  anchorOrigin: PropTypes.shape({}),
  autoHideDuration: PropTypes.number,
  message: PropTypes.node,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

export default AutoStatusBar;
