import React, { Component } from 'react';
import Tags from '../Tags/Tags';
import * as tagsService from '../../services/tags/tags';
import Loading from '../Loading/Loading';
import Error from '../Error/Error';
import BodyWrapper from '../BodyWrapper/BodyWrapper';
import normalizeData from '../../services/normalizeData';

const initNewTagData = {
  id: '',
  name: '',
};
class TagsPage extends Component {
  state = {
    loading: false,
    loadError: null,
    submitting: false,
    submitSuccess: false,
    submitError: null,
    submittingImage: {},
    submitImageSuccess: false,
    submitImageError: null,
    deleting: {},
    deleteSuccess: false,
    deleteError: null,
    ids: [],
    data: {},
    newTagData: {
      ...initNewTagData,
    },
  };

  componentDidMount() {
    this.setState({ loading: true });
    this.loadData();
  }

  onNewTagInputChange = key => e => {
    const value = e.target.value;
    this.setState(state => ({
      newTagData: {
        ...state.newTagData,
        [key]: value,
      },
    }));
  };

  onFileChange = id => e => {
    if (!e.target.files.length) {
      return;
    }

    const file = e.target.files[0];

    this.setState({
      submitImageError: null,
      submitImageSuccess: false,
      submittingImage: { id },
    });

    const fileReader = new FileReader();

    fileReader.addEventListener('loadend', async () => {
      const { error } = await tagsService.uploadImage(id, file);
      this.setState(state => ({
        submitImageSuccess: !error,
        submitImageError: error,
        submittingImage: {},
        ...(!error
          ? {
              data: {
                ...state.data,
                [id]: {
                  ...state.data[id],
                  imageUrl: fileReader.result,
                },
              },
            }
          : {}),
      }));
    });

    fileReader.readAsDataURL(file);
  };

  onInsert = async () => {
    this.setState({
      submitting: true,
      submitSuccess: false,
      submitError: null,
    });

    const { id, name } = this.state.newTagData;

    const { error } = await tagsService.upsert(id, { name });

    this.setState({
      submitError: error,
      submitSuccess: !error,
      submitting: false,
      newTagData: {
        ...initNewTagData,
      },
    });

    if (!error) {
      this.loadData();
    }
  };

  onDelete = async id => {
    this.setState({
      deleting: { id },
      deleteSuccess: false,
      deleteError: null,
    });

    const { error } = await tagsService.remove(id);

    this.setState({
      deleteError: error,
      deleteSuccess: !error,
      deleting: {},
    });

    if (!error) {
      this.loadData();
    }
  };

  async loadData() {
    const { data: rawData, error } = await tagsService.get();

    if (error) {
      this.setState({ loading: false, loadError: error });
      return;
    }

    const { ids, data } = normalizeData(rawData);
    this.setState({ loading: false, ids, data });
  }

  render() {
    const {
      loading,
      data,
      ids,
      loadError,
      submitting,
      submitSuccess,
      submitError,
      submittingImage,
      submitImageSuccess,
      submitImageError,
      newTagData,
      deleting,
      deleteError,
      deleteSuccess,
    } = this.state;

    switch (true) {
      case loading:
        return <Loading />;
      case Boolean(loadError):
        return <Error message="Error retrieving data" />;
      default:
        return (
          <BodyWrapper title="Tags">
            <Tags
              ids={ids}
              data={data}
              newTagData={newTagData}
              onInsert={this.onInsert}
              onNewTagInputChange={this.onNewTagInputChange}
              onFileChange={this.onFileChange}
              onDelete={this.onDelete}
              submitting={submitting}
              submitSuccess={submitSuccess}
              submitError={submitError}
              submittingImage={submittingImage}
              submitImageSuccess={submitImageSuccess}
              submitImageError={submitImageError}
              deleting={deleting}
              deleteError={deleteError}
              deleteSuccess={deleteSuccess}
            />
          </BodyWrapper>
        );
    }
  }
}

export default TagsPage;
