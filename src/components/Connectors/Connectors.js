import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';
import styles from './styles';
import ConnectorRow from './ConnectorRow';

const fields = [
  { key: 'id', name: 'ID' },
  { key: 'floorIds', name: 'Floors' },
  { key: 'weight', name: 'Weight' },
  { key: 'tagIds', name: 'Tag' },
  { key: 'ignoreMinLiftRestriction', name: 'Exclude from min lift restriction' },
];

class Connectors extends Component {
  state = {
    confirmDialog: null,
  };

  openDeleteDialog = id => () => {
    this.setState({
      confirmDialog: { id },
    });
  };

  onConfirmDelete = () => {
    this.props.onDelete(this.state.confirmDialog.id);
    this.setState({ confirmDialog: null });
  };

  onCloseDialog = () => {
    this.setState({ confirmDialog: null });
  };

  render() {
    const {
      classes,
      ids,
      submitting,
      submitSuccess,
      submitError,
      updating,
      updateSuccess,
      updateError,
      deleting,
      deleteSuccess,
      deleteError,
      onInsert,
      onUpdate,
      onInputChange,
      onNewConnectorInputChange,
      data,
      newConnectorData,
      tagIds,
      tagData,
      floorData,
    } = this.props;
    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              {fields.map(({ key, name }) => (
                <TableCell className={classes.tableCell} key={`header_${key}`}>
                  {name}
                </TableCell>
              ))}
              <TableCell className={classes.tableCell} />
              <TableCell className={classes.tableCell} />
            </TableRow>
            <ConnectorRow
              fields={fields}
              submitting={submitting}
              onUpdate={onUpdate}
              onInsert={onInsert}
              openDeleteDialog={this.openDeleteDialog}
              tagIds={tagIds}
              tagData={tagData}
              floorData={floorData}
              data={newConnectorData}
              onInputChange={onInputChange}
              onNewConnectorInputChange={onNewConnectorInputChange}
            />
          </TableHead>
          <TableBody>
            {ids.map(id => (
              <ConnectorRow
                key={id}
                id={id}
                fields={fields}
                updating={updating[id]}
                deleting={deleting[id]}
                onUpdate={onUpdate}
                onInsert={onInsert}
                openDeleteDialog={this.openDeleteDialog}
                tagIds={tagIds}
                tagData={tagData}
                floorData={floorData}
                data={data[id]}
                onInputChange={onInputChange}
                onNewConnectorInputChange={onNewConnectorInputChange}
              />
            ))}
          </TableBody>
        </Table>
        <AutoStatusBar open={updateSuccess} variant="success" message="Updated successfully" />
        <AutoStatusBar open={submitSuccess} variant="success" message="Inserted successfully" />
        <AutoStatusBar open={deleteSuccess} variant="success" message="Deleted successfully" />
        <AutoStatusBar
          open={Boolean(submitError)}
          variant="error"
          message={(submitError || {}).message}
        />
        <AutoStatusBar
          open={Boolean(updateError)}
          variant="error"
          message={(updateError || {}).message}
        />
        <AutoStatusBar
          open={Boolean(deleteError)}
          variant="error"
          message={(deleteError || {}).message}
        />
        {this.state.confirmDialog ? (
          <Dialog open onClose={this.onCloseDialog}>
            <DialogTitle>Are you sure to delete {this.state.confirmDialog.id} ?</DialogTitle>
            <DialogActions>
              <Button onClick={this.onConfirmDelete} color="primary" autoFocus>
                Yes
              </Button>
              <Button onClick={this.onCloseDialog} color="primary">
                No
              </Button>
            </DialogActions>
          </Dialog>
        ) : null}
      </div>
    );
  }
}

const ErrorPropType = PropTypes.shape({ message: PropTypes.string.isRequired });

Connectors.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  ids: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  data: PropTypes.objectOf(
    PropTypes.shape({
      floorIds: PropTypes.arrayOf(PropTypes.string).isRequired,
      weight: PropTypes.number,
      tagIds: PropTypes.arrayOf(PropTypes.string),
      ignoreMinLiftRestriction: PropTypes.bool,
    }),
  ).isRequired,
  floorData: PropTypes.array.isRequired,
  tagIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  tagData: PropTypes.objectOf(PropTypes.shape({ name: PropTypes.string.isRequired })).isRequired,
  newConnectorData: PropTypes.shape({
    id: PropTypes.string.isRequired,
    floorIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    weight: PropTypes.number,
    tagIds: PropTypes.arrayOf(PropTypes.string),
    ignoreMinLiftRestriction: PropTypes.bool,
  }).isRequired,
  submitting: PropTypes.bool.isRequired,
  submitSuccess: PropTypes.bool.isRequired,
  submitError: ErrorPropType,
  updating: PropTypes.shape({ id: PropTypes.string }).isRequired,
  updateSuccess: PropTypes.bool.isRequired,
  updateError: ErrorPropType,
  deleting: PropTypes.shape({ id: PropTypes.string }).isRequired,
  deleteSuccess: PropTypes.bool.isRequired,
  deleteError: ErrorPropType,
  onNewConnectorInputChange: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
  onInsert: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default withStyles(styles)(Connectors);
