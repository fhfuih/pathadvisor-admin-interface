import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import Select from '@material-ui/core/Select';
import styles from './styles';

class ConnectorRow extends PureComponent {
  renderTableCell(key, id = null) {
    const {
      classes,
      tagIds,
      tagData,
      floorData,
      data,
      onInputChange,
      onNewConnectorInputChange,
    } = this.props;

    if (key === 'id' && id !== null) {
      return id;
    }

    const value = data[key];
    const onChange = id === null ? (unused, e) => onNewConnectorInputChange(e) : onInputChange;

    switch (key) {
      case 'tagIds':
        return (
          <select value={(value && value[0]) || ''} onChange={onChange(id, key)}>
            <option value="" />
            {tagIds.map(tagId => (
              <option key={tagId} value={tagId}>
                {tagData[tagId].name}
              </option>
            ))}
          </select>
        );
      case 'floorIds': {
        const items = [];

        floorData.forEach(([buildingId, floors]) => {
          items.push(
            <MenuItem key={buildingId} disabled>
              {buildingId}
            </MenuItem>,
          );
          floors.forEach(({ _id, name }) => {
            items.push(
              <MenuItem key={_id} value={_id}>
                <Checkbox checked={value.indexOf(_id) > -1} />
                {name || _id}
              </MenuItem>,
            );
          });
        });

        return (
          <Select
            multiple
            value={value}
            onChange={onChange(id, key)}
            renderValue={selected => selected.join(', ')}
          >
            {items}
          </Select>
        );
      }
      case 'ignoreMinLiftRestriction':
        return <Checkbox checked={value} onChange={onChange(id, key)} />;
      case 'weight':
        return (
          <input
            type="number"
            className={classes.textField}
            value={value}
            onChange={onChange(id, key)}
          />
        );

      default:
        return (
          <input
            type="text"
            className={classes.textField}
            value={value}
            onChange={onChange(id, key)}
          />
        );
    }
  }

  render() {
    const {
      id,
      classes,
      fields,
      submitting,
      updating,
      deleting,
      onUpdate,
      onInsert,
      openDeleteDialog,
    } = this.props;
    return (
      <TableRow>
        {fields.map(({ key }) => (
          <TableCell className={classes.tableCell} key={`${id}_${key}`}>
            {this.renderTableCell(key, id)}
          </TableCell>
        ))}

        {id ? (
          <>
            <TableCell className={classes.tableCell}>
              <Button
                onClick={onUpdate(id)}
                variant="outlined"
                disabled={updating}
                className={classes.button}
              >
                {updating ? 'Updating...' : 'Update'}
              </Button>
            </TableCell>
            <TableCell className={classes.tableCell}>
              <Button
                className={classes.button}
                variant="outlined"
                onClick={openDeleteDialog(id)}
                disabled={deleting}
              >
                {deleting ? 'Deleting...' : 'Delete'}
              </Button>
            </TableCell>
          </>
        ) : (
          <>
            <TableCell className={classes.tableCell}>
              <Button className={classes.button} variant="outlined" onClick={onInsert}>
                {submitting ? 'Adding...' : '+ Add Connector'}
              </Button>
            </TableCell>
            <TableCell className={classes.tableCell} />
          </>
        )}
      </TableRow>
    );
  }
}

ConnectorRow.propTypes = {
  id: PropTypes.string,
  classes: PropTypes.shape({}).isRequired,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }).isRequired,
  ).isRequired,
  tagIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  tagData: PropTypes.objectOf(PropTypes.shape({ name: PropTypes.string.isRequired })).isRequired,
  floorData: PropTypes.array.isRequired,
  data: PropTypes.shape({
    floorIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    weight: PropTypes.number,
    tagIds: PropTypes.arrayOf(PropTypes.string),
    ignoreMinLiftRestriction: PropTypes.bool,
  }).isRequired,
  submitting: PropTypes.bool,
  updating: PropTypes.bool,
  deleting: PropTypes.bool,
  onUpdate: PropTypes.func.isRequired,
  onInsert: PropTypes.func.isRequired,
  openDeleteDialog: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
  onNewConnectorInputChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(ConnectorRow);
