import React, { Component } from 'react';
import Login from '../Login/Login';
import * as authService from '../../services/auth/auth';

class LoginPage extends Component {
  state = {
    loading: false,
    error: false,
    inputValues: {
      username: '',
      password: '',
    },
  };

  onInputChange = name => e => {
    const value = e.target.value;
    this.setState(state => ({
      inputValues: {
        ...state.inputValues,
        [name]: value,
      },
    }));
  };

  onSubmit = async () => {
    const {
      inputValues: { username, password },
    } = this.state;
    this.setState({ error: false, loading: true });

    const { success, errorMessage } = await authService.login(username, password);

    if (!success) {
      this.setState({ loading: false, error: true, errorMessage });
      return;
    }

    window.location.reload();
  };

  render() {
    const { inputValues, loading, errorMessage, error } = this.state;
    return (
      <Login
        onInputChange={this.onInputChange}
        inputValues={inputValues}
        loading={loading}
        errorMessage={errorMessage}
        error={error}
        onSubmit={this.onSubmit}
      />
    );
  }
}

export default LoginPage;
