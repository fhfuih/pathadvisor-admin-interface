import React, { Component } from 'react';
import Suggestions from '../Suggestions/Suggestions';
import * as suggestionsService from '../../services/suggestions/suggestions';
import Loading from '../Loading/Loading';
import Error from '../Error/Error';
import BodyWrapper from '../BodyWrapper/BodyWrapper';
import normalizeData from '../../services/normalizeData';

class SuggestionsPage extends Component {
  state = {
    loading: false,
    loadError: null,
    updating: {},
    updateSuccess: false,
    updateError: null,
    ids: [],
    data: {},
  };

  componentDidMount() {
    this.setState({ loading: true });
    this.loadData();
  }

  onResolve = id => async () => {
    this.setState({
      updating: { id },
      updateSuccess: false,
      updateError: null,
    });

    const { error } = await suggestionsService.resolve(id, !this.state.data[id].resolved);

    this.setState(state => ({
      ...(!error
        ? {
            data: {
              ...state.data,
              [id]: { ...state.data[id], resolved: !state.data[id].resolved },
            },
          }
        : {}),
      updating: {},
      updateSuccess: !error,
      updateError: error,
    }));
  };

  async loadData() {
    const { data: rawData, error } = await suggestionsService.get();

    if (error) {
      this.setState({ loading: false, loadError: error });
      return;
    }

    const { ids, data } = normalizeData(rawData);

    this.setState({ loading: false, ids, data });
  }

  render() {
    const { loading, loadError, data, ids, updating, updateSuccess, updateError } = this.state;

    switch (true) {
      case loading:
        return <Loading />;
      case Boolean(loadError):
        return <Error message="Error retrieving data" />;
      default:
        return (
          <BodyWrapper title="Suggestions">
            <Suggestions
              ids={ids}
              data={data}
              updating={updating}
              updateSuccess={updateSuccess}
              updateError={updateError}
              onResolve={this.onResolve}
            />
          </BodyWrapper>
        );
    }
  }
}

export default SuggestionsPage;
