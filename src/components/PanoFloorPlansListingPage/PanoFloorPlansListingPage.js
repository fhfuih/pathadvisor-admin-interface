import React, { Component } from 'react';
import FloorPlansListing from '../PanoFloorPlansListing/PanoFloorPlansListing';
import * as floorsService from '../../services/floors/floors';
import * as buildingService from '../../services/buildings/buildings';
import * as operationsService from '../../services/operations/operations';
import Loading from '../Loading/Loading';
import Error from '../Error/Error';
import BodyWrapper from '../BodyWrapper/BodyWrapper';
import normalizeData from '../../services/normalizeData';

class FloorPlansListingPage extends Component {
  state = {
    loading: false,
    loadError: null,
    ids: [],
    data: {},
    buildingIds: [],
    buildingData: {},
    metaUpdating: false,
    metaUpdateSuccess: false,
    metaUpdateError: false,
  };

  async componentDidMount() {
    this.setState({ loading: true });
    const [
      { data: rawData, error },
      { data: rawBuildingData, error: buildingError },
    ] = await Promise.all([floorsService.get(), buildingService.get()]);

    if (error || buildingError) {
      this.setState({ loading: false, loadError: error });
      return;
    }

    const { ids, data } = normalizeData(rawData);
    const { ids: buildingIds, data: buildingData } = normalizeData(rawBuildingData);
    this.setState({ loading: false, ids, data, buildingIds, buildingData });
  }

  onMetaUpdate = async () => {
    this.setState({
      metaUpdating: true,
      metaUpdateSuccess: false,
      metaUpdateError: false,
    });

    const { error } = await operationsService.updateMeta();

    this.setState({
      metaUpdating: false,
      metaUpdateSuccess: !error,
      metaUpdateError: error,
    });
  };

  render() {
    const {
      loading,
      loadError,
      data,
      ids,
      buildingIds,
      buildingData,
      metaUpdating,
      metaUpdateSuccess,
      metaUpdateError,
    } = this.state;

    switch (true) {
      case loading:
        return <Loading />;
      case Boolean(loadError):
        return <Error message="Error retrieving data" />;
      default:
        return (
          <BodyWrapper title="Panorama Floor Plans">
            <FloorPlansListing
              ids={ids}
              data={data}
              buildingIds={buildingIds}
              buildingData={buildingData}
              onMetaUpdate={this.onMetaUpdate}
              metaUpdating={metaUpdating}
              metaUpdateSuccess={metaUpdateSuccess}
              metaUpdateError={metaUpdateError}
            />
          </BodyWrapper>
        );
    }
  }
}

export default FloorPlansListingPage;
