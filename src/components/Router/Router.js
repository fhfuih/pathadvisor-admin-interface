import React, { Component } from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';
import LoginPage from '../LoginPage/LoginPage';
import Dashboard from '../Dashboard/Dashboard';
import SettingsPage from '../SettingsPage/SettingsPage';

import { isSessionValid } from '../../services/auth/auth';
import TagsPage from '../TagsPage/TagsPage';
import BuildingsPage from '../BuildingsPage/BuildingsPage';
import FloorsPage from '../FloorsPage/FloorsPage';
import FloorPlansListingPage from '../FloorPlansListingPage/FloorPlansListingPage';
import FloorPlanPage from '../FloorPlanPage/FloorPlanPage';
import PanoFloorPlansListingPage from '../PanoFloorPlansListingPage/PanoFloorPlansListingPage';
import PanoFloorPlanPage from '../PanoFloorPlanPage/PanoFloorPlanPage';
import ConnectorsPage from '../ConnectorsPage/ConnectorsPage';
import SuggestionsPage from '../SuggestionsPage/SuggestionsPage';
import LogsPage from '../LogsPage/LogsPage';
import ApiKeysPage from '../ApiKeysPage/ApiKeysPage';
import Loading from '../Loading/Loading';
import RestrictMessage from '../RestrictMessage/RestrictMessage';
import VideoPage from '../VideoPage/VideoPage';

const PrivateRoute = ({
  component: PageComponent,
  isLoggedIn,
  restrictOnProd = false,
  ...rest
}) => {
  if (window.location.host === process.env.REACT_APP_PRODUCTION_HOST && restrictOnProd) {
    return <RestrictMessage />;
  }
  return (
    <Route
      {...rest}
      render={props => (isLoggedIn ? <PageComponent {...props} /> : <Redirect to="/login" />)}
    />
  );
};

class Router extends Component {
  state = {
    isLoggedIn: null,
  };

  async componentDidMount() {
    const isLoggedIn = await isSessionValid();

    this.setState({
      isLoggedIn,
    });
  }

  render() {
    const { isLoggedIn } = this.state;
    if (isLoggedIn === null) {
      return <Loading />;
    }

    return (
      <HashRouter>
        <Switch>
          <Route
            path="/"
            render={() => (isLoggedIn ? <Redirect to="/dashboard" /> : <Redirect to="/login" />)}
            exact
          />
          <Route
            path="/login"
            render={props => (isLoggedIn ? <Redirect to="/dashboard" /> : <LoginPage {...props} />)}
            exact
          />
          <Route path="/videos" component={VideoPage} exact />
          <PrivateRoute isLoggedIn={isLoggedIn} path="/dashboard" component={Dashboard} exact />
          <PrivateRoute
            isLoggedIn={isLoggedIn}
            restrictOnProd
            path="/settings"
            component={SettingsPage}
            exact
          />
          <PrivateRoute
            isLoggedIn={isLoggedIn}
            restrictOnProd
            path="/tags"
            component={TagsPage}
            exact
          />
          <PrivateRoute
            isLoggedIn={isLoggedIn}
            restrictOnProd
            path="/buildings"
            component={BuildingsPage}
            exact
          />
          <PrivateRoute
            isLoggedIn={isLoggedIn}
            restrictOnProd
            path="/floors"
            component={FloorsPage}
            exact
          />
          <PrivateRoute
            isLoggedIn={isLoggedIn}
            restrictOnProd
            path="/floor-plans"
            component={FloorPlansListingPage}
            exact
          />
          <PrivateRoute
            isLoggedIn={isLoggedIn}
            restrictOnProd
            path="/floor-plans/:floorId"
            component={FloorPlanPage}
            exact
          />
          <PrivateRoute
            isLoggedIn={isLoggedIn}
            restrictOnProd
            path="/pano-floor-plans"
            component={PanoFloorPlansListingPage}
            exact
          />
          <PrivateRoute
            isLoggedIn={isLoggedIn}
            restrictOnProd
            path="/pano-floor-plans/:floorId"
            component={PanoFloorPlanPage}
            exact
          />
          <PrivateRoute
            isLoggedIn={isLoggedIn}
            restrictOnProd
            path="/connectors"
            component={ConnectorsPage}
            exact
          />
          <PrivateRoute
            isLoggedIn={isLoggedIn}
            path="/suggestions"
            component={SuggestionsPage}
            exact
          />
          <PrivateRoute isLoggedIn={isLoggedIn} path="/logs" component={LogsPage} exact />
          <PrivateRoute isLoggedIn={isLoggedIn} path="/api-keys" component={ApiKeysPage} exact />
        </Switch>
      </HashRouter>
    );
  }
}

export default Router;
