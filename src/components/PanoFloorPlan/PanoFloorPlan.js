import React, { Component, createRef } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import isEmpty from 'lodash.isempty';
import difference from 'lodash.difference';
import xor from 'lodash.xor';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import CanvasHandler from '../../pathadvisor-map-canvas/CanvasHandler';
import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';
import getNodeShape, { getNodeShapeIds } from '../FloorPlan/getNodeShape';
import getEdgeShape, { getEdgeShapeIds } from '../FloorPlan/getEdgeShape';
import nodePropType from '../FloorPlan/nodePropType';

const PANO_COLOR = 'MediumOrchid';
const SELECTED_COLOR = 'rgb(26, 115, 232)';
const ERROR_COLOR = 'red';

const styles = {
  canvasRoot: { height: '100%', overflow: 'hidden' },
  hide: { display: 'none' },
  menu: {
    position: 'absolute',
    backgroundColor: 'whitesmoke',
    width: '180px',
    height: '100%',
  },
  floatMenu: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 210,
  },
  button: {
    padding: '5px',
    fontSize: '10px',
    margin: '3px',
    minWidth: 0,
  },
  menuTitle: {
    fontSize: '12px',
    padding: '3px 10px',
    color: 'white',
    backgroundColor: '#333',
    fontWeight: 'bold',
  },
  menuContent: {
    fontSize: '10px',
  },
  optionBox: {
    display: 'flex',
    alignItems: 'center',
    padding: '2px 5px',
  },
  optionLabel: {
    fontSize: '10px',
  },
  precisionInput: {
    width: '50px',
    marginLeft: '5px',
  },
};

const initDrawState = { drawMode: false, drawParams: {}, intermediateItems: [] };
class FloorPlan extends Component {
  canvasRootRef = createRef();

  state = {
    ...initDrawState,
    persistDraw: false,
  };

  componentDidMount() {
    this.canvasHandler = new CanvasHandler();

    const { floorId } = this.props;
    this.canvasRootRef.current.appendChild(this.canvasHandler.getCanvas());

    this.canvasHandler.updateLevelToScale([1]);

    this.canvasHandler.updateDimension(
      this.canvasRootRef.current.offsetWidth,
      this.canvasRootRef.current.offsetHeight,
    );

    this.canvasHandler.updatePosition(0, 0, floorId, 0);
    this.canvasHandler.addMouseUpListener(() => {
      this.draw();
    });

    this.renderMapImage();
    this.renderCanvasItems();

    document.addEventListener('keydown', this.keyDownEvent);
    document.addEventListener('keyup', this.keyUpEvent);

    this.canvasHandler.addClickAwayListener(this.onClickAway);
  }

  componentDidUpdate(prevProps, prevState) {
    this.renderCanvasItems(prevProps, prevState);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.keyDownEvent);
    document.removeEventListener('keyup', this.keyUpEvent);
  }

  onClickAway = () => {
    if (this.shiftKeyPressed || this.controlKeyPressed || this.state.drawMode) {
      return;
    }

    // clear all selection
    this.clearAllSelections();
  };

  onClickNode = (id, { isPolygonClickHandler = false } = {}) => ({ stopPropagation }) => {
    const { onSelect, onDeselectNodes } = this.props;
    const { drawMode } = this.state;

    // if not in drawEdge mode, prevent the user from selecting nodes.
    // if in drawMode and isPolygonClickHandler, also prevent (behavior inherited from FloorPlan)
    if (!drawMode || isPolygonClickHandler) {
      return;
    }

    if (this.controlKeyPressed) {
      onDeselectNodes([id]);
      return;
    }

    if (!this.shiftKeyPressed) {
      stopPropagation();
    }

    onSelect({
      // clear edge selection if shift key is not pressed
      edges: { ids: [], merge: this.shiftKeyPressed },
      nodes: { ids: [id], merge: this.shiftKeyPressed || drawMode },
    });
  };

  getNodes(nodeIds) {
    const { nodeData, selectedNodes, displayText, displayNonPano } = this.props;

    return nodeIds.reduce(
      (carrier, id) => [
        ...carrier,
        ...(nodeData[id].panoImageUrl || displayNonPano
          ? getNodeShape({
              id,
              node: nodeData[id],
              ...(nodeData[id].panoImageUrl
                ? { colors: { shape: PANO_COLOR, text: PANO_COLOR, point: PANO_COLOR } }
                : {}),
              ...(selectedNodes[id]
                ? { colors: { shape: SELECTED_COLOR, text: SELECTED_COLOR, point: SELECTED_COLOR } }
                : {}),
              onClick: this.onClickNode(id),
              onClickPolygon: this.onClickNode(id, { isPolygonClickHandler: true }),
              displayText,
            })
          : []),
      ],
      [],
    );
  }

  getEdges(edgeIds) {
    const {
      edgeData,
      nodeData,
      selectedEdges,
      errorEdges,
      onSelect,
      onDeselectEdges,
      displayEdge,
    } = this.props;

    return displayEdge
      ? edgeIds.reduce(
          (carrier, id) => [
            ...carrier,
            ...getEdgeShape({
              id,
              edge: edgeData[id],
              startNode: nodeData[edgeData[id].fromNodeId],
              endNode: nodeData[edgeData[id].toNodeId],
              ...(selectedEdges[id]
                ? { colors: { indicator: SELECTED_COLOR, line: SELECTED_COLOR } }
                : {}),
              ...(errorEdges[id] ? { colors: { indicator: ERROR_COLOR, line: ERROR_COLOR } } : {}),
              onClick: ({ stopPropagation }) => {
                if (this.state.drawMode) {
                  return;
                }

                if (this.controlKeyPressed) {
                  onDeselectEdges([id]);
                  return;
                }

                if (!this.shiftKeyPressed) {
                  stopPropagation();
                }

                onSelect({
                  // clear node selection if shift key is not pressed
                  nodes: { ids: [], merge: this.shiftKeyPressed },
                  edges: { ids: [id], merge: this.shiftKeyPressed },
                });
              },
            }),
          ],
          [],
        )
      : [];
  }

  nothingSelected = () => isEmpty(this.props.selectedEdges) && isEmpty(this.props.selectedNodes);

  clearAllSelections = () => {
    if (this.nothingSelected()) {
      return;
    }
    this.props.onSelect({ nodes: { ids: [] }, edges: { ids: [] } });
  };

  keyDownEvent = e => {
    this.keyPressedEvent(e);
  };

  keyUpEvent = e => {
    this.keyPressedEvent(e);
  };

  keyPressedEvent = e => {
    this.shiftKeyPressed = e.shiftKey;
    this.controlKeyPressed = e.ctrlKey || e.metaKey;
  };

  switchDrawMode = (drawParams = {}) => () => {
    this.clearAllSelections();
    this.setState({ drawMode: true, drawParams });
  };

  switchOffDrawMode = () => {
    this.setState({ ...initDrawState });
  };

  resetDrawMode = () => {
    this.setState(state => ({
      ...initDrawState,
      ...(state.persistDraw ? { drawMode: state.drawMode, drawParams: state.drawParams } : {}),
    }));
  };

  togglePersistDraw = () => {
    this.setState(state => ({ persistDraw: !state.persistDraw }));
  };

  draw = async () => {
    const { floorId, onUpsertEdge, selectedNodes, nodeData } = this.props;
    const { drawMode, drawParams } = this.state;

    if (drawMode) {
      const selectedNodeIds = Object.keys(selectedNodes);

      if (selectedNodeIds.length !== 2) {
        return;
      }

      const [fromNodeId, toNodeId] = selectedNodeIds;

      // only allow the connections of two pano nodes.
      if (!nodeData[fromNodeId].panoImageUrl || !nodeData[toNodeId].panoImageUrl) {
        this.clearAllSelections();
        return;
      }

      const { uniDirection = false } = drawParams;

      onUpsertEdge(shortid.generate(), {
        floorId,
        fromNodeId,
        toNodeId,
      });

      if (!uniDirection) {
        onUpsertEdge(shortid.generate(), {
          floorId,
          fromNodeId: toNodeId,
          toNodeId: fromNodeId,
        });
      }

      this.clearAllSelections();
      this.resetDrawMode();
    }
  };

  renderCanvasItems(prevProps = {}, prevState = {}) {
    const {
      nodeData: prevNodeData = {},
      edgeIds: prevEdgeIds = [],
      selectedNodes: prevSelectedNodes = {},
      selectedEdges: prevSelectedEdges = {},
      errorEdges: prevErrorEdges = {},
      displayText: prevDisplayText,
      displayEdge: prevDisplayEdge,
      displayNonPano: prevDisplayNonPano,
    } = prevProps;

    const { intermediateItems: prevIntermediateItems = [] } = prevState;
    const {
      nodeIds,
      edgeIds,
      edgeData,
      selectedNodes,
      selectedEdges,
      errorEdges,
      displayText,
      displayEdge,
      displayNonPano,
    } = this.props;
    const { intermediateItems } = this.state;

    const removedEdgeIds = difference(prevEdgeIds, edgeIds);
    const removeIntermediateItemIds = difference(
      prevIntermediateItems.map(({ id }) => id),
      intermediateItems.map(({ id }) => id),
    );

    let modifiedNodeIds = new Set([
      ...xor(Object.keys(prevSelectedNodes), Object.keys(selectedNodes)),
    ]);

    let modifiedEdgeIds = new Set([
      ...difference(edgeIds, prevEdgeIds),
      ...xor(Object.keys(prevSelectedEdges), Object.keys(selectedEdges)),
      ...xor(Object.keys(prevErrorEdges), Object.keys(errorEdges)),
      ...edgeIds.filter(
        id =>
          modifiedNodeIds.has(edgeData[id].fromNodeId) ||
          modifiedNodeIds.has(edgeData[id].toNodeId),
      ),
    ]);

    removedEdgeIds.forEach(id => {
      modifiedEdgeIds.delete(id);
    });

    if (displayText !== prevDisplayText || displayNonPano !== prevDisplayNonPano) {
      modifiedNodeIds = nodeIds;
    } else {
      modifiedNodeIds = Array.from(modifiedNodeIds);
    }

    if (displayEdge !== prevDisplayEdge) {
      modifiedEdgeIds = edgeIds;
    }

    this.canvasHandler.removeMapItems([
      ...modifiedNodeIds
        .map(id => getNodeShapeIds(id, prevNodeData[id] || {}))
        .reduce((carrier, ids) => [...carrier, ...ids], []),
      ...[...removedEdgeIds, ...modifiedEdgeIds]
        .map(id => getEdgeShapeIds(id))
        .reduce((carrier, ids) => [...carrier, ...ids], []),
      ...removeIntermediateItemIds,
    ]);

    this.canvasHandler.setMapItems(
      [
        ...this.getNodes([...modifiedNodeIds]),
        ...this.getEdges([...modifiedEdgeIds]),
        ...intermediateItems,
      ],
      { merge: true },
    );
  }

  renderMapImage() {
    const { mapImageSrc, floorId, floorData } = this.props;

    const mapImage = new Image();
    mapImage.src = mapImageSrc;

    this.canvasHandler.removeAllMapTiles();
    this.canvasHandler.addMapTiles([
      {
        id: 'mapImage',
        floor: floorId,
        x: floorData.startX,
        y: floorData.startY,
        image: mapImage,
      },
    ]);
  }

  render() {
    const {
      floorId,
      classes,
      deleteError,
      deleteSuccess,
      displayText,
      displayEdge,
      displayNonPano,
      selectedEdges,
      isDirty,
      onToggleDisplayText,
      onToggleDisplayEdge,
      onToggleDisplayNonPano,
      onDelete,
      onResetError,
      saving,
      saveSuccess,
      saveError,
      onSave,
    } = this.props;

    const { drawMode, persistDraw } = this.state;

    return (
      <>
        <div className={classes.menu}>
          <div className={classes.menuBox}>
            <div className={classes.menuTitle}>Floor {floorId}</div>
            <Button
              variant="outlined"
              className={classes.button}
              component={Link}
              to="/pano-floor-plans"
            >
              Dashboard
            </Button>
          </div>
          <div className={classes.menuBox}>
            <div className={classes.menuTitle}>Draw</div>
            <Button
              variant="outlined"
              className={classes.button}
              disabled={drawMode || !displayEdge}
              onClick={this.switchDrawMode()}
            >
              Edge
            </Button>
            <Button
              variant="outlined"
              className={classes.button}
              disabled={drawMode || !displayEdge}
              onClick={this.switchDrawMode({
                uniDirection: true,
              })}
            >
              Edge Uni-direction
            </Button>
            {drawMode ? (
              <>
                <div className={classes.optionBox}>
                  <div className={classes.optionLabel}>Persist draw mode</div>
                  <Switch color="primary" checked={persistDraw} onChange={this.togglePersistDraw} />
                </div>
                <Button
                  variant="contained"
                  className={classes.button}
                  color="secondary"
                  onClick={this.switchOffDrawMode}
                >
                  Swift off draw mode
                </Button>
              </>
            ) : null}
            <Button
              variant="contained"
              className={classes.button}
              color="secondary"
              disabled={this.nothingSelected() || deleteError}
              onClick={() => {
                onDelete(Object.keys(selectedEdges));
              }}
            >
              Delete selected
            </Button>
            {deleteError ? (
              <Button
                variant="contained"
                className={classes.button}
                color="secondary"
                onClick={() => {
                  onResetError();
                }}
              >
                Reset error
              </Button>
            ) : null}
          </div>

          <div className={classes.menuBox}>
            <div className={classes.menuTitle}> Layers </div>
            <div className={classes.optionBox}>
              <input type="checkbox" checked={displayText} onChange={onToggleDisplayText} />
              <div className={classes.optionLabel}>Text</div>
            </div>
            <div className={classes.optionBox}>
              <input type="checkbox" checked={displayEdge} onChange={onToggleDisplayEdge} />
              <div className={classes.optionLabel}>Edges</div>
            </div>
            <div className={classes.optionBox}>
              <input type="checkbox" checked={displayNonPano} onChange={onToggleDisplayNonPano} />
              <div className={classes.optionLabel}>Non-Pano Nodes</div>
            </div>
          </div>

          <div className={classes.menuBox}>
            <div className={classes.menuTitle}> Notes </div>
            <div className={classes.menuContent}>
              <div className={classes.optionBox}>
                You can upload panorama and associate them to nodes in Floor Plan page. This page is
                intended for adding edges to the implicit streetview map.
                <br />
                (These edges only describe how the yellow man icon traverses the map. They are not
                used in finding shortest paths, etc.)
              </div>
            </div>
          </div>

          <div className={classes.menuBox}>
            <div className={classes.menuTitle} />
            <div className={classes.menuContent}>
              <Button
                variant="contained"
                className={classes.button}
                color="secondary"
                disabled={!isDirty || saving}
                onClick={onSave}
              >
                {saving ? 'Saving...' : 'Save changes'}
              </Button>

              {saveError ? (
                <Button
                  variant="contained"
                  className={classes.button}
                  color="secondary"
                  onClick={() => {
                    onResetError();
                  }}
                >
                  Reset error
                </Button>
              ) : null}
            </div>
          </div>
        </div>
        <div ref={this.canvasRootRef} className={classes.canvasRoot} />
        <AutoStatusBar
          open={deleteError}
          variant="error"
          message="Not all selected items are deleted because some of them are still connected. See items painted in red."
        />
        <AutoStatusBar open={deleteSuccess} variant="success" message="Selected items deleted" />
        <AutoStatusBar
          open={saveError}
          variant="error"
          message="Not all modified items are saved successfully. See items painted in red."
        />
        <AutoStatusBar open={saveSuccess} variant="success" message="All modified items saved" />
      </>
    );
  }
}

FloorPlan.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  floorId: PropTypes.string.isRequired,
  floorData: PropTypes.shape({
    startX: PropTypes.number.isRequired,
    startY: PropTypes.number.isRequired,
  }).isRequired,
  mapImageSrc: PropTypes.string.isRequired,
  displayText: PropTypes.bool.isRequired,
  displayEdge: PropTypes.bool.isRequired,
  displayNonPano: PropTypes.bool.isRequired,
  deleteError: PropTypes.bool.isRequired,
  deleteSuccess: PropTypes.bool.isRequired,
  nodeIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  nodeData: PropTypes.objectOf(nodePropType.isRequired).isRequired,
  edgeIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  edgeData: PropTypes.objectOf(
    PropTypes.shape({
      floorId: PropTypes.string.isRequired,
      fromNodeId: PropTypes.string.isRequired,
      toNodeId: PropTypes.string.isRequired,
    }),
  ).isRequired,
  selectedEdges: PropTypes.objectOf(PropTypes.bool).isRequired,
  selectedNodes: PropTypes.objectOf(PropTypes.bool).isRequired,
  errorEdges: PropTypes.objectOf(PropTypes.bool).isRequired,
  isDirty: PropTypes.bool.isRequired,
  saving: PropTypes.bool.isRequired,
  saveSuccess: PropTypes.bool.isRequired,
  saveError: PropTypes.bool.isRequired,
  onSelect: PropTypes.func.isRequired,
  onDeselectEdges: PropTypes.func.isRequired,
  onDeselectNodes: PropTypes.func.isRequired,
  onToggleDisplayText: PropTypes.func.isRequired,
  onToggleDisplayEdge: PropTypes.func.isRequired,
  onToggleDisplayNonPano: PropTypes.func.isRequired,
  onUpsertEdge: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onResetError: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
};

export default withStyles(styles)(FloorPlan);
