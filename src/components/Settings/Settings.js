import React from 'react';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  input: {
    padding: '10px',
  },
  row: {
    display: 'block',
    marginTop: '20px',
    marginBottom: '20px',
  },
  levelRow: {
    display: 'flex',
    alignItems: 'center',
  },
});

function Settings(props) {
  const {
    classes,
    onInputChange,
    onScaleInputChange,
    onPositionChange,
    onMobilePositionChange,
    onAddLevel,
    onRemoveLevel,
    onSubmit,
    data: {
      levelToScale,
      highestLevel,
      lowestLevel,
      minutesPerMeter,
      defaultPosition,
      mobileDefaultPosition,
    },
    submitting,
    submitSuccess,
    submitError,
  } = props;

  return (
    <div>
      <div className={classes.row}>
        <div className={classes.largeLabel}>Level Scale </div>
        <div>
          {levelToScale.map((scale, level) => (
            <div key={level} className={classes.levelRow}>
              <TextField
                inputProps={{ className: classes.input }}
                type="number"
                label={`Level ${level}`}
                className={classes.textField}
                margin="normal"
                variant="outlined"
                value={scale}
                onChange={onScaleInputChange(level)}
              />
              <Button variant="outlined" onClick={onRemoveLevel(level)}>
                - Remove level
              </Button>
            </div>
          ))}

          <Button variant="outlined" onClick={onAddLevel}>
            + Add level
          </Button>
        </div>
      </div>
      <Divider />
      <TextField
        inputProps={{ className: classes.input }}
        type="number"
        label="Highest level allowed"
        className={classnames(classes.row, classes.textField)}
        margin="normal"
        variant="outlined"
        value={highestLevel}
        helperText="Highest allowed level for users to zoom out, should not higher than the highest level defined in Scale section"
        onChange={onInputChange('highestLevel')}
      />
      <Divider />
      <TextField
        inputProps={{ className: classes.input }}
        type="number"
        label="Lowest level allowed"
        className={classnames(classes.row, classes.textField)}
        margin="normal"
        variant="outlined"
        value={lowestLevel}
        helperText="Lowest allowed level for users to zoom out, should not be smaller than 0"
        onChange={onInputChange('lowestLevel')}
      />
      <Divider />
      <TextField
        inputProps={{ className: classes.input }}
        type="number"
        label="Minutes per meter"
        className={classnames(classes.row, classes.textField)}
        margin="normal"
        variant="outlined"
        value={minutesPerMeter}
        helperText="The minute calculated to walk a path will be based on this"
        onChange={onInputChange('minutesPerMeter')}
      />
      <Divider />
      <div className={classes.row}>
        <div className={classes.largeLabel}>Default map position for desktop </div>
        <div>
          <TextField
            inputProps={{ className: classes.input }}
            label="Floor"
            className={classnames(classes.textField)}
            margin="normal"
            variant="outlined"
            value={defaultPosition.floor}
            onChange={onPositionChange('floor')}
          />
          <TextField
            inputProps={{ className: classes.input }}
            label="x"
            type="number"
            className={classnames(classes.textField)}
            margin="normal"
            variant="outlined"
            value={defaultPosition.x}
            onChange={onPositionChange('x')}
          />
          <TextField
            inputProps={{ className: classes.input }}
            label="y"
            type="number"
            className={classnames(classes.textField)}
            margin="normal"
            variant="outlined"
            value={defaultPosition.y}
            onChange={onPositionChange('y')}
          />
          <TextField
            inputProps={{ className: classes.input }}
            label="level"
            type="number"
            className={classnames(classes.textField)}
            margin="normal"
            variant="outlined"
            value={defaultPosition.level}
            onChange={onPositionChange('level')}
          />
        </div>
      </div>
      <Divider />
      <div className={classes.row}>
        <div className={classes.largeLabel}>Default map position for mobile </div>
        <div>
          <TextField
            inputProps={{ className: classes.input }}
            label="Floor"
            className={classnames(classes.textField)}
            margin="normal"
            variant="outlined"
            value={mobileDefaultPosition.floor}
            onChange={onMobilePositionChange('floor')}
          />
          <TextField
            inputProps={{ className: classes.input }}
            label="x"
            type="number"
            className={classnames(classes.textField)}
            margin="normal"
            variant="outlined"
            value={mobileDefaultPosition.x}
            onChange={onMobilePositionChange('x')}
          />
          <TextField
            inputProps={{ className: classes.input }}
            label="y"
            type="number"
            className={classnames(classes.textField)}
            margin="normal"
            variant="outlined"
            value={mobileDefaultPosition.y}
            onChange={onMobilePositionChange('y')}
          />
          <TextField
            inputProps={{ className: classes.input }}
            label="level"
            type="number"
            className={classnames(classes.textField)}
            margin="normal"
            variant="outlined"
            value={mobileDefaultPosition.level}
            onChange={onMobilePositionChange('level')}
          />
        </div>
      </div>
      <Divider />
      <Button
        onClick={onSubmit}
        variant="contained"
        color="primary"
        className={classes.row}
        disabled={submitting}
      >
        {submitting ? 'Saving...' : 'Save'}
      </Button>
      <AutoStatusBar open={submitSuccess} variant="success" message="Saved successfully" />
      <AutoStatusBar
        open={Boolean(submitError)}
        variant="error"
        message={(submitError || {}).message}
      />
    </div>
  );
}

Settings.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  onInputChange: PropTypes.func.isRequired,
  onScaleInputChange: PropTypes.func.isRequired,
  onPositionChange: PropTypes.func.isRequired,
  onMobilePositionChange: PropTypes.func.isRequired,
  onAddLevel: PropTypes.func.isRequired,
  onRemoveLevel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  data: PropTypes.shape({}).isRequired,
  submitting: PropTypes.bool.isRequired,
  submitSuccess: PropTypes.bool,
  submitError: PropTypes.shape({ message: PropTypes.string }),
};

export default withStyles(styles)(Settings);
