import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import * as authService from '../../services/auth/auth';

async function logout() {
  await authService.logout();
  window.location.href = '/';
}

const Header = ({ title }) => (
  <AppBar position="static">
    <Toolbar>
      <Typography variant="h6" color="inherit" style={{ flexGrow: 1 }}>
        {title}
      </Typography>
      <Button color="inherit" component={Link} to="/dashboard">
        Dashboard
      </Button>
      <Button color="inherit" onClick={logout}>
        Logout
      </Button>
    </Toolbar>
  </AppBar>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Header;
