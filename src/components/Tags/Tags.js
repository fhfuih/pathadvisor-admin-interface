import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  input: {
    padding: '10px',
  },
  hide: {
    display: 'none',
  },
  image: {
    width: '32px',
  },
  upload: {
    width: '200px',
  },
  delete: {
    width: '200px',
  },
});

class Tags extends Component {
  state = {
    confirmDialog: null,
  };

  onDeleteTag = id => () => {
    this.setState({
      confirmDialog: { id },
    });
  };

  onConfirmDeleteTag = () => {
    this.props.onDelete(this.state.confirmDialog.id);
    this.setState({ confirmDialog: null });
  };

  onCloseDialog = () => {
    this.setState({ confirmDialog: null });
  };

  render() {
    const {
      classes,
      ids,
      data,
      submitting,
      submitError,
      onFileChange,
      submittingImage,
      submitImageSuccess,
      submitImageError,
      onNewTagInputChange,
      submitSuccess,
      newTagData,
      onInsert,
      deleting,
      deleteError,
      deleteSuccess,
    } = this.props;
    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <TextField
                  inputProps={{ className: classes.input }}
                  label="ID"
                  className={classes.textField}
                  margin="normal"
                  variant="outlined"
                  value={newTagData.id}
                  onChange={onNewTagInputChange('id')}
                />
              </TableCell>
              <TableCell>
                <TextField
                  inputProps={{ className: classes.input }}
                  label="name"
                  className={classes.textField}
                  margin="normal"
                  variant="outlined"
                  value={newTagData.name}
                  onChange={onNewTagInputChange('name')}
                />
              </TableCell>
              <TableCell>
                <Button variant="outlined" onClick={onInsert}>
                  {submitting ? 'Adding...' : '+ Add New Tag'}
                </Button>
              </TableCell>
              <TableCell />
              <TableCell />
            </TableRow>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Image</TableCell>
              <TableCell />
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {ids.map(id => (
              <TableRow key={id}>
                <TableCell>{id}</TableCell>
                <TableCell>{data[id].name}</TableCell>
                <TableCell>
                  {data[id].imageUrl ? (
                    <img className={classes.image} src={data[id].imageUrl} alt={data[id].name} />
                  ) : null}
                </TableCell>
                <TableCell>
                  <input
                    accept="image/*"
                    className={classes.hide}
                    id={`file-${id}`}
                    type="file"
                    onChange={onFileChange(id)}
                    disabled={submittingImage.id === id}
                  />
                  <label htmlFor={`file-${id}`}>
                    <Button
                      component="span"
                      variant="outlined"
                      disabled={submittingImage.id === id}
                      className={classes.upload}
                    >
                      {submittingImage.id === id ? 'Uploading...' : 'Upload image'}
                    </Button>
                  </label>
                </TableCell>
                <TableCell>
                  <Button
                    className={classes.delete}
                    variant="outlined"
                    onClick={this.onDeleteTag(id)}
                    disabled={deleting.id === id}
                  >
                    {deleting.id === id ? 'Deleting...' : 'Delete Tag'}
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <AutoStatusBar
          open={submitImageSuccess}
          variant="success"
          message="Uploaded successfully"
        />
        <AutoStatusBar open={submitSuccess} variant="success" message="Inserted successfully" />
        <AutoStatusBar open={deleteSuccess} variant="success" message="Deleted successfully" />
        <AutoStatusBar
          open={Boolean(submitError)}
          variant="error"
          message={(submitError || {}).message}
        />
        <AutoStatusBar
          open={Boolean(submitImageError)}
          variant="error"
          message={(submitImageError || {}).message}
        />
        <AutoStatusBar
          open={Boolean(deleteError)}
          variant="error"
          message={(deleteError || {}).message}
        />
        {this.state.confirmDialog ? (
          <Dialog open onClose={this.onCloseDialog}>
            <DialogTitle>Are you sure to delete {this.state.confirmDialog.id} ?</DialogTitle>
            <DialogActions>
              <Button onClick={this.onConfirmDeleteTag} color="primary" autoFocus>
                Yes
              </Button>
              <Button onClick={this.onCloseDialog} color="primary">
                No
              </Button>
            </DialogActions>
          </Dialog>
        ) : null}
      </div>
    );
  }
}

Tags.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  ids: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  data: PropTypes.objectOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      imageUrl: PropTypes.string,
    }),
  ).isRequired,
  submitting: PropTypes.bool.isRequired,
  submitError: PropTypes.shape({
    message: PropTypes.string.isRequired,
  }),
  onFileChange: PropTypes.func.isRequired,
  submittingImage: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  submitImageSuccess: PropTypes.bool.isRequired,
  submitImageError: PropTypes.shape({
    message: PropTypes.string.isRequired,
  }),
  onNewTagInputChange: PropTypes.func.isRequired,
  submitSuccess: PropTypes.bool.isRequired,
  newTagData: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  onInsert: PropTypes.func.isRequired,
  deleting: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  deleteError: PropTypes.shape({
    message: PropTypes.string.isRequired,
  }),
  deleteSuccess: PropTypes.bool.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default withStyles(styles)(Tags);
