import React from 'react';

function VideoPage() {
  return (
    <>
      <h1>Useful video links</h1>
      <ul>
        <li>
          <a href="https://s3-ap-southeast-1.amazonaws.com/pathadvisor/videos/place2.mp4">
            Example 2: How to add / modify / remove rooms (using ROOM 5474-5485 as example)
          </a>
        </li>
        <li>
          <a href="https://s3-ap-southeast-1.amazonaws.com/pathadvisor/videos/place.mp4">
            Example 1: How to add / modify / remove a specific room / venue / spot
          </a>
        </li>
        <li>
          <a href="https://s3-ap-southeast-1.amazonaws.com/pathadvisor/videos/editAndUploadFloorPlan.mp4">
            How to update layout / drawings
          </a>
          ( Software required:{' '}
          <a href="https://www.autodesk.co.uk/products/dwg/viewers">Autodesk DWG TrueView</a>,{' '}
          <a href="https://www.gimp.org/">GIMP</a>)
        </li>
        <li>
          <a href="https://s3-ap-southeast-1.amazonaws.com/pathadvisor/videos/addAnIcon.mp4">
            How to add an icon to a specific room / venue / spot
          </a>
        </li>
      </ul>
    </>
  );
}

export default VideoPage;
