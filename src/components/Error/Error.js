import React from 'react';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import ErrorIcon from '@material-ui/icons/WarningTwoTone';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  body: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  icon: {
    fontSize: '200px',
  },
});

const Error = ({ classes, message }) => (
  <div className={classes.body}>
    <ErrorIcon className={classes.icon} color="error" />
    <Typography variant="h5" gutterBottom>
      {message}
    </Typography>
  </div>
);

Error.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  message: PropTypes.string.isRequired,
};

export default withStyles(styles)(Error);
