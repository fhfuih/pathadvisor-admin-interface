import React, { Component } from 'react';
import Buildings from '../Buildings/Buildings';
import * as buildingsService from '../../services/buildings/buildings';
import Loading from '../Loading/Loading';
import Error from '../Error/Error';
import BodyWrapper from '../BodyWrapper/BodyWrapper';
import normalizeData from '../../services/normalizeData';

const initNewBuildingData = { id: '', name: '' };
class BuildingsPage extends Component {
  state = {
    loading: false,
    loadError: null,
    submitting: false,
    submitSuccess: false,
    submitError: null,
    updating: {},
    updateSuccess: false,
    updateError: null,
    deleting: {},
    deleteSuccess: false,
    deleteError: null,
    ids: [],
    data: {},
    newBuildingData: {
      ...initNewBuildingData,
    },
  };

  componentDidMount() {
    this.setState({ loading: true });
    this.loadData();
  }

  onNewBuildingInputChange = key => e => {
    const value = e.target.value;
    this.setState(state => ({
      newBuildingData: {
        ...state.newBuildingData,
        [key]: value,
      },
    }));
  };

  onInputChange = (id, key) => e => {
    const value = e.target.value;
    this.setState(state => ({
      data: {
        ...state.data,
        [id]: {
          ...state.data[id],
          [key]: value,
        },
      },
    }));
  };

  onInsert = async () => {
    this.setState({
      submitting: true,
      submitSuccess: false,
      submitError: null,
    });

    const { id, name } = this.state.newBuildingData;
    const { error } = await buildingsService.upsert(id, { name });

    this.setState({
      submitError: error,
      submitSuccess: !error,
      submitting: false,
      newBuildingData: {
        ...initNewBuildingData,
      },
    });

    if (!error) {
      this.loadData();
    }
  };

  onUpdate = id => async () => {
    this.setState({
      updating: { id },
      updateSuccess: false,
      updateError: null,
    });

    const { name } = this.state.data[id];
    const { error } = await buildingsService.upsert(id, { name });

    this.setState({
      updateError: error,
      updateSuccess: !error,
      updating: {},
    });
  };

  onDelete = async id => {
    this.setState({
      deleting: { id },
      deleteSuccess: false,
      deleteError: null,
    });

    const { error } = await buildingsService.remove(id);

    this.setState({
      deleteError: error,
      deleteSuccess: !error,
      deleting: {},
    });

    if (!error) {
      this.loadData();
    }
  };

  async loadData() {
    const { data: rawData, error } = await buildingsService.get();

    if (error) {
      this.setState({ loading: false, loadError: error });
      return;
    }

    const { ids, data } = normalizeData(rawData);
    this.setState({ loading: false, ids, data });
  }

  render() {
    const {
      loading,
      loadError,
      data,
      ids,
      newBuildingData,
      submitting,
      submitSuccess,
      submitError,
      updating,
      updateSuccess,
      updateError,
      deleting,
      deleteSuccess,
      deleteError,
    } = this.state;

    switch (true) {
      case loading:
        return <Loading />;
      case Boolean(loadError):
        return <Error message="Error retrieving data" />;
      default:
        return (
          <BodyWrapper title="Buildings">
            <Buildings
              ids={ids}
              data={data}
              newBuildingData={newBuildingData}
              submitting={submitting}
              submitSuccess={submitSuccess}
              submitError={submitError}
              updating={updating}
              updateSuccess={updateSuccess}
              updateError={updateError}
              deleting={deleting}
              deleteSuccess={deleteSuccess}
              deleteError={deleteError}
              onNewBuildingInputChange={this.onNewBuildingInputChange}
              onInputChange={this.onInputChange}
              onInsert={this.onInsert}
              onUpdate={this.onUpdate}
              onDelete={this.onDelete}
            />
          </BodyWrapper>
        );
    }
  }
}

export default BuildingsPage;
