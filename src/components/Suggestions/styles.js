const styles = {
  textField: { width: '80px' },
  button: {
    fontSize: '10px',
  },
  tableCell: {
    padding: '5px',
  },
};

export default styles;
