import PropTypes from 'prop-types';

export default PropTypes.shape({
  type: PropTypes.oneOf(['new', 'wrong', 'bug', 'general']).isRequired,
  email: PropTypes.string,
  name: PropTypes.string,
  description: PropTypes.string.isRequired,
  coordinates: PropTypes.string,
  floorId: PropTypes.string,
  createdAt: PropTypes.number.isRequired,
  updatedAt: PropTypes.number.isRequired,
  resolved: PropTypes.bool.isRequired,
});
