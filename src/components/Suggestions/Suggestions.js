import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';
import SuggestionRow from './SuggestionRow';
import suggestionPropType from './suggestionPropType';
import styles from './styles';

const fields = [
  { key: 'type', name: 'Type' },
  { key: 'name', name: 'Name' },
  { key: 'email', name: 'Email' },
  { key: 'description', name: 'Description' },
  { key: 'floorId', name: 'Floor' },
  { key: 'coordinates', name: 'Coordinates' },
  { key: 'createdAt', name: 'Created at' },
  { key: 'updatedAt', name: 'Updated at' },
];

class Suggestions extends Component {
  render() {
    const { classes, ids, data, updating, updateSuccess, updateError, onResolve } = this.props;
    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              {fields.map(({ key, name }) => (
                <TableCell className={classes.tableCell} key={`header_${key}`}>
                  {name}
                </TableCell>
              ))}
              <TableCell className={classes.tableCell} />
              <TableCell className={classes.tableCell} />
            </TableRow>
          </TableHead>
          <TableBody>
            {ids.map(id => (
              <SuggestionRow
                key={id}
                id={id}
                fields={fields}
                updating={updating[id] || false}
                onResolve={onResolve(id)}
                data={data[id]}
              />
            ))}
          </TableBody>
        </Table>
        <AutoStatusBar open={updateSuccess} variant="success" message="Updated successfully" />
        <AutoStatusBar
          open={Boolean(updateError)}
          variant="error"
          message={(updateError || {}).message}
        />
      </div>
    );
  }
}

const ErrorPropType = PropTypes.shape({ message: PropTypes.string.isRequired });

Suggestions.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  ids: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  data: PropTypes.objectOf(suggestionPropType.isRequired).isRequired,
  updating: PropTypes.shape({ id: PropTypes.string }).isRequired,
  updateSuccess: PropTypes.bool.isRequired,
  updateError: ErrorPropType,
  onResolve: PropTypes.func.isRequired,
};

export default withStyles(styles)(Suggestions);
