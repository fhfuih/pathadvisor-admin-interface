import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import suggestionPropType from './suggestionPropType';
import styles from './styles';

class SuggestionRow extends PureComponent {
  renderTableCell(key) {
    const { data } = this.props;

    const value = data[key];

    switch (key) {
      case 'createdAt':
      case 'updatedAt':
        return new Date(value).toISOString();
      default:
        return value;
    }
  }

  renderButtonText() {
    const { updating, data } = this.props;
    if (updating) {
      return 'Updating...';
    }

    if (data.resolved) {
      return 'Mark unresolve';
    }

    return 'Mark resolve';
  }

  render() {
    const { fields, classes, onResolve, data } = this.props;
    return (
      <TableRow>
        {fields.map(({ key }) => (
          <TableCell className={classes.tableCell} key={`$${key}`}>
            {this.renderTableCell(key)}
          </TableCell>
        ))}

        <TableCell className={classes.tableCell}>
          <Button
            className={classes.button}
            variant="outlined"
            color={data.resolved ? 'primary' : 'secondary'}
            onClick={onResolve}
          >
            {this.renderButtonText()}
          </Button>
        </TableCell>
      </TableRow>
    );
  }
}

SuggestionRow.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }).isRequired,
  ).isRequired,
  data: suggestionPropType.isRequired,
  updating: PropTypes.bool.isRequired,
  onResolve: PropTypes.func.isRequired,
};

export default withStyles(styles)(SuggestionRow);
