import React from 'react';

function RestrictMessage() {
  return (
    <h1 style={{ fontSize: '18px', padding: '50px' }}>
      Direct changes on production environment is not allowed. <br />
      You can only make changes on staging environment and copy changes from staging to production
    </h1>
  );
}

export default RestrictMessage;
