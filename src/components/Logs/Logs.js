import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  inputRow: { display: 'flex', alignItems: 'center' },
  inputCell: { marginRight: '10px' },
  button: { marginTop: '10px' },
  inputLabel: { fontSize: '12px', marginBottom: '5px' },
};

function Logs({ classes, startDate, endDate, downloadUrl, onInputChange }) {
  return (
    <>
      <div className={classes.inputRow}>
        <div className={classes.inputCell}>
          <div className={classes.inputLabel}>Start date</div>
          <input type="date" value={startDate} onChange={onInputChange('startDate')} />
        </div>
        <div className={classes.inputCell}>
          <div className={classes.inputLabel}>End date</div>
          <input type="date" value={endDate} onChange={onInputChange('endDate')} />
        </div>
      </div>
      <Button
        className={classes.button}
        variant="outlined"
        target="_blank"
        href={downloadUrl}
        disabled={!downloadUrl}
      >
        Download CSV
      </Button>
    </>
  );
}

Logs.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  startDate: PropTypes.string.isRequired,
  endDate: PropTypes.string.isRequired,
  downloadUrl: PropTypes.string.isRequired,
  onInputChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(Logs);
