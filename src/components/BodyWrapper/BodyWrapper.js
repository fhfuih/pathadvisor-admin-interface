import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Header from '../Header/Header';

const styles = {
  body: {
    margin: '10px',
    flex: 1,
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
};

const BodyWrapper = ({ classes, title, children, bodyClassName = null }) => (
  <div className={classes.container}>
    <Header title={title} />
    <div className={classnames(classes.body, bodyClassName)}>{children}</div>
  </div>
);

BodyWrapper.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  bodyClassName: PropTypes.string,
};

export default withStyles(styles)(BodyWrapper);
