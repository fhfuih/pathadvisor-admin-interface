import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  input: {
    padding: '10px',
  },
  update: {
    width: '200px',
  },
  delete: {
    width: '200px',
  },
});

class Buildings extends Component {
  state = {
    confirmDialog: null,
  };

  openDeleteDialog = id => () => {
    this.setState({
      confirmDialog: { id },
    });
  };

  onConfirmDelete = () => {
    this.props.onDelete(this.state.confirmDialog.id);
    this.setState({ confirmDialog: null });
  };

  onCloseDialog = () => {
    this.setState({ confirmDialog: null });
  };

  render() {
    const {
      classes,
      ids,
      data,
      newBuildingData,
      submitting,
      submitSuccess,
      submitError,
      updating,
      updateSuccess,
      updateError,
      deleting,
      deleteSuccess,
      deleteError,
      onNewBuildingInputChange,
      onInputChange,
      onInsert,
      onUpdate,
    } = this.props;
    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <TextField
                  inputProps={{ className: classes.input }}
                  label="ID"
                  className={classes.textField}
                  margin="normal"
                  variant="outlined"
                  value={newBuildingData.id}
                  onChange={onNewBuildingInputChange('id')}
                />
              </TableCell>
              <TableCell>
                <TextField
                  inputProps={{ className: classes.input }}
                  label="name"
                  className={classes.textField}
                  margin="normal"
                  variant="outlined"
                  value={newBuildingData.name}
                  onChange={onNewBuildingInputChange('name')}
                />
              </TableCell>
              <TableCell>
                <Button variant="outlined" onClick={onInsert}>
                  {submitting ? 'Adding...' : '+ Add Building'}
                </Button>
              </TableCell>
              <TableCell />
            </TableRow>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>name</TableCell>
              <TableCell />
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {ids.map(id => (
              <TableRow key={id}>
                <TableCell>{id}</TableCell>
                <TableCell>
                  <TextField
                    inputProps={{ className: classes.input }}
                    label="Name"
                    className={classes.textField}
                    margin="normal"
                    variant="outlined"
                    value={data[id].name}
                    onChange={onInputChange(id, 'name')}
                  />
                </TableCell>
                <TableCell>
                  <Button
                    onClick={onUpdate(id)}
                    variant="outlined"
                    disabled={updating.id === id}
                    className={classes.update}
                  >
                    {updating.id === id ? 'Updating...' : 'Update'}
                  </Button>
                </TableCell>
                <TableCell>
                  <Button
                    className={classes.delete}
                    variant="outlined"
                    onClick={this.openDeleteDialog(id)}
                    disabled={deleting.id === id}
                  >
                    {deleting.id === id ? 'Deleting...' : 'Delete'}
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <AutoStatusBar open={updateSuccess} variant="success" message="Updated successfully" />
        <AutoStatusBar open={submitSuccess} variant="success" message="Inserted successfully" />
        <AutoStatusBar open={deleteSuccess} variant="success" message="Deleted successfully" />
        <AutoStatusBar
          open={Boolean(submitError)}
          variant="error"
          message={(submitError || {}).message}
        />
        <AutoStatusBar
          open={Boolean(updateError)}
          variant="error"
          message={(updateError || {}).message}
        />
        <AutoStatusBar
          open={Boolean(deleteError)}
          variant="error"
          message={(deleteError || {}).message}
        />{' '}
        {this.state.confirmDialog ? (
          <Dialog open onClose={this.onCloseDialog}>
            <DialogTitle>Are you sure to delete {this.state.confirmDialog.id} ?</DialogTitle>
            <DialogActions>
              <Button onClick={this.onConfirmDelete} color="primary" autoFocus>
                Yes
              </Button>
              <Button onClick={this.onCloseDialog} color="primary">
                No
              </Button>
            </DialogActions>
          </Dialog>
        ) : null}
      </div>
    );
  }
}

const ErrorPropType = PropTypes.shape({ message: PropTypes.string.isRequired });

Buildings.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  ids: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  data: PropTypes.objectOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
    }),
  ).isRequired,
  newBuildingData: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  submitting: PropTypes.bool.isRequired,
  submitSuccess: PropTypes.bool.isRequired,
  submitError: ErrorPropType,
  updating: PropTypes.shape({ id: PropTypes.string }).isRequired,
  updateSuccess: PropTypes.bool.isRequired,
  updateError: ErrorPropType,
  deleting: PropTypes.shape({ id: PropTypes.string }).isRequired,
  deleteSuccess: PropTypes.bool.isRequired,
  deleteError: ErrorPropType,
  onNewBuildingInputChange: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
  onInsert: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default withStyles(styles)(Buildings);
