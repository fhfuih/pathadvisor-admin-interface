import React, { Component } from 'react';
import PropTypes from 'prop-types';
import omit from 'lodash.omit';
import omitBy from 'lodash.omitby';
import without from 'lodash.without';
import isEmpty from 'lodash.isempty';
import PanoFloorPlan from '../PanoFloorPlan/PanoFloorPlan';
import * as nodesService from '../../services/nodes/nodes';
import * as panoEdgesService from '../../services/panoEdges/panoEdges';
import * as mapImagesService from '../../services/mapImages/mapImages';
import * as floorsService from '../../services/floors/floors';
import Loading from '../Loading/Loading';
import Error from '../Error/Error';
import normalizeData from '../../services/normalizeData';
import { ACTION } from '../FloorPlanPage/constants';

function getTaskOrder(task) {
  if (task.action === ACTION.DELETE) {
    return 1;
  }
  return 2;
}

class FloorPlanPage extends Component {
  state = {
    loading: true,
    loadError: null,
    nodeIds: [],
    nodeData: {},
    edgeIds: [],
    dirtyEdges: {},
    edgeData: {},
    selectedEdges: {},
    selectedNodes: {},
    errorEdges: {},
    deleteSuccess: false,
    deleteError: false,
    displayText: true,
    displayEdge: true,
    displayNonPano: false,
    saving: false,
    saveSuccess: false,
    saveError: false,
  };

  async componentDidMount() {
    const floorId = this.props.match.params.floorId;
    this.setState({ loading: true });

    const [
      { data: nodes, error: getNodesError },
      { data: edges, error: getEdgesError },
      { data: floorData, error: getFloorError },
    ] = await Promise.all([
      nodesService.get(floorId),
      panoEdgesService.get(floorId),
      floorsService.getOne(floorId),
    ]);

    if (getNodesError || getEdgesError || getFloorError) {
      this.setState({ loading: false, loadError: true });
      return;
    }

    const { ids: nodeIds, data: nodeData } = normalizeData(nodes);
    const { ids: edgeIds, data: edgeData } = normalizeData(edges);

    this.setState({
      loading: false,
      nodeIds,
      nodeData,
      edgeIds,
      edgeData,
      floorData,
    });
  }

  findReverseEdgeIds = ids => {
    const { edgeIds, edgeData } = this.state;
    const reverseIds = [];

    ids.forEach(id => {
      const edge = edgeData[id];
      edgeIds.forEach(edgeId => {
        const nextEdge = edgeData[edgeId];
        if (
          edgeId === id ||
          edge.fromNodeId !== nextEdge.toNodeId ||
          edge.toNodeId !== nextEdge.fromNodeId
        ) {
          return;
        }

        reverseIds.push(edgeId);
      });
    });

    return reverseIds;
  };

  onSelect = ({ edges, nodes }) => {
    if (!edges && !nodes) {
      return;
    }

    if ((edges && !Array.isArray(edges.ids)) || (nodes && !Array.isArray(nodes.ids))) {
      throw new TypeError('Argument must be an array');
    }

    const reverseIds = edges && edges.ids ? this.findReverseEdgeIds(edges.ids) : [];

    this.setState(state => ({
      ...(edges
        ? {
            selectedEdges: {
              ...(edges.merge ? state.selectedEdges : {}),
              ...edges.ids.reduce((carrier, id) => ({ ...carrier, [id]: true }), {}),
              ...reverseIds.reduce((carrier, id) => ({ ...carrier, [id]: true }), {}),
            },
          }
        : {}),
      ...(nodes
        ? {
            selectedNodes: {
              ...(nodes.merge ? state.selectedNodes : {}),
              ...nodes.ids.reduce((carrier, id) => ({ ...carrier, [id]: true }), {}),
            },
          }
        : {}),
    }));
  };

  onDeselectEdges = ids => {
    if (!Array.isArray(ids)) {
      throw new TypeError('Argument must be an array');
    }

    const reverseIds = this.findReverseEdgeIds(ids);

    this.setState(state => ({
      selectedEdges: omit(state.selectedEdges, ids, reverseIds),
    }));
  };

  onDeselectNodes = ids => {
    if (!Array.isArray(ids)) {
      throw new TypeError('Argument must be an array');
    }

    this.setState(state => ({
      selectedNodes: omit(state.selectedNodes, ids),
    }));
  };

  onToggleDisplayText = () => {
    this.setState(state => ({
      displayText: !state.displayText,
    }));
  };

  onToggleDisplayEdge = () => {
    this.setState(state => ({
      displayEdge: !state.displayEdge,
    }));
  };

  onToggleDisplayNonPano = () => {
    this.setState(state => ({
      displayNonPano: !state.displayNonPano,
    }));
  };

  onDelete = async (edges = []) => {
    const edgeSet = new Set(edges);

    // force no batch setState
    await this.setState({ deleteError: false, deleteSuccess: false });

    if (!edges.length) {
      return;
    }

    this.setState(state => ({
      dirtyEdges: {
        ...omitBy(state.dirtyEdges, (action, id) => edgeSet.has(id) && action === ACTION.INSERT),
        ...edges.reduce(
          (carrier, id) => ({
            ...carrier,
            ...(state.dirtyEdges[id] !== ACTION.INSERT ? { [id]: ACTION.DELETE } : {}),
          }),
          {},
        ),
      },
      edgeIds: without(state.edgeIds, ...edges),
      // edges will always be deleted successfully
      selectedEdges: {},
    }));
  };

  onUpsertEdge = (id, edge) => {
    this.setState(state => ({
      dirtyEdges: {
        ...state.dirtyEdges,
        [id]:
          !state.edgeData[id] || state.dirtyEdges[id] === ACTION.INSERT
            ? ACTION.INSERT
            : ACTION.UPDATE,
      },
      edgeIds: [...state.edgeIds, ...(!state.edgeData[id] ? [id] : [])],
      edgeData: {
        ...state.edgeData,
        [id]: {
          ...edge,
        },
      },
    }));
  };

  onResetError = () => {
    this.setState({
      saveError: false,
      deleteError: false,
      errorEdges: {},
    });
  };

  onSave = async () => {
    this.setState({
      saving: true,
      saveSuccess: false,
      saveError: false,
    });

    const tasks = [];
    const { dirtyEdges, edgeData } = this.state;
    const cleanIds = [];
    const errorEdges = {};
    const oldIdsToNewIds = {};
    const deletedIds = [];

    for (const id of Object.keys(dirtyEdges)) {
      const task = { id, action: dirtyEdges[id] };
      switch (dirtyEdges[id]) {
        case ACTION.INSERT:
          task.run = () => panoEdgesService.insert(edgeData[id]);
          break;
        case ACTION.DELETE:
          task.run = () => panoEdgesService.remove(id);
          break;
        default:
          throw new TypeError(
            `Action type for edge must be one of [${ACTION.INSERT}, ${ACTION.DELETE}], ${
              dirtyEdges[id]
            } received`,
          );
      }

      tasks.push(task);
    }

    // tasks must follow the order edge:delete, edge:save to avoid ref/no-ref errors
    tasks.sort((a, b) => getTaskOrder(a) - getTaskOrder(b));

    for (const { id, run, action } of tasks) {
      const response = await run();

      const { data: { _id: newId = null } = {}, error } = response;

      // newly inserted item gets a new ID
      if (newId && newId !== id) {
        oldIdsToNewIds[id] = newId;
      }

      if (error) {
        console.log(error);
        errorEdges[id] = action;
      } else {
        cleanIds.push(id);

        if (action === ACTION.DELETE) {
          deletedIds.push(id);
        }
      }
    }

    const newEdgeIds = Object.entries(oldIdsToNewIds);

    this.setState(state => ({
      saving: false,
      saveSuccess: isEmpty(errorEdges),
      saveError: !isEmpty(errorEdges),

      dirtyEdges: omit(
        state.dirtyEdges,
        ...cleanIds,
        // if delete fails items will be re-added and therefore remove from dirty
        ...Object.entries(errorEdges)
          .filter(([, action]) => action === ACTION.DELETE)
          .map(([id]) => id),
      ),
      ...(newEdgeIds.length || deletedIds.length || Object.keys(errorEdges).length
        ? {
            edgeIds: [
              ...new Set([
                ...without(state.edgeIds, ...newEdgeIds.map(([oldId]) => oldId)),
                ...newEdgeIds.map(([, newId]) => newId),
                ...Object.keys(errorEdges),
              ]),
            ],
            edgeData: {
              ...omit(state.edgeData, newEdgeIds.map(([oldId]) => oldId), deletedIds),
              ...newEdgeIds.reduce(
                (carrier, [id, newId]) => ({
                  ...carrier,
                  [newId]: state.edgeData[id],
                }),
                {},
              ),
            },
            selectedEdges: {
              ...omit(state.selectedEdges, newEdgeIds.map(([oldId]) => oldId)),
              ...newEdgeIds.reduce(
                (carrier, [id, newId]) =>
                  state.selectedEdges[id]
                    ? { ...carrier, [newId]: state.selectedEdges[id] }
                    : carrier,
                {},
              ),
            },
          }
        : {}),
      errorEdges: Object.keys(errorEdges).reduce((carrier, id) => ({ ...carrier, [id]: true }), {}),
    }));
  };

  render() {
    const {
      loading,
      loadError,
      nodeIds,
      nodeData,
      edgeIds,
      edgeData,
      floorData,
      selectedEdges,
      selectedNodes,
      errorEdges,
      displayText,
      displayEdge,
      displayNonPano,
      deleteError,
      deleteSuccess,
      dirtyEdges,
      saving,
      saveSuccess,
      saveError,
    } = this.state;
    const floorId = this.props.match.params.floorId;

    switch (true) {
      case loading:
        return <Loading />;
      case loadError:
        return <Error message="Error retrieving data" />;
      default:
        return (
          <PanoFloorPlan
            floorId={floorId}
            floorData={floorData}
            nodeIds={nodeIds}
            nodeData={nodeData}
            edgeIds={edgeIds}
            edgeData={edgeData}
            mapImageSrc={mapImagesService.getImageSrc(floorId)}
            deleteSuccess={deleteSuccess}
            deleteError={deleteError}
            selectedEdges={selectedEdges}
            selectedNodes={selectedNodes}
            errorEdges={errorEdges}
            onSelect={this.onSelect}
            onDeselectEdges={this.onDeselectEdges}
            onDeselectNodes={this.onDeselectNodes}
            displayText={displayText}
            displayEdge={displayEdge}
            displayNonPano={displayNonPano}
            isDirty={!isEmpty(dirtyEdges)}
            onToggleDisplayText={this.onToggleDisplayText}
            onToggleDisplayEdge={this.onToggleDisplayEdge}
            onToggleDisplayNonPano={this.onToggleDisplayNonPano}
            onUpsertEdge={this.onUpsertEdge}
            onDelete={this.onDelete}
            onResetError={this.onResetError}
            saving={saving}
            saveSuccess={saveSuccess}
            saveError={saveError}
            onSave={this.onSave}
          />
        );
    }
  }
}

FloorPlanPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      floorId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default FloorPlanPage;
