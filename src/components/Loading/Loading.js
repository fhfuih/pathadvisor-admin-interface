import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  body: {
    marginTop: '50px',
    display: 'flex',
    justifyContent: 'center',
  },
};

const Loading = ({ classes }) => (
  <div className={classes.body}>
    <CircularProgress />
  </div>
);

Loading.propTypes = {
  classes: PropTypes.shape({}).isRequired,
};

export default withStyles(styles)(Loading);
