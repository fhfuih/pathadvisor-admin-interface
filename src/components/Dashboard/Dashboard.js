import React from 'react';
import Button from '@material-ui/core/Button';
import withStyles from '@material-ui/core/styles/withStyles';
import { Link } from 'react-router-dom';
import BodyWrapper from '../BodyWrapper/BodyWrapper';

const Dashboard = ({ classes }) => (
  <BodyWrapper title="HKUST Path Advsior Admin Interface Dashboard">
    <div className={classes.container}>
      <Button variant="outlined" className={classes.button} component={Link} to="/settings">
        Edit Application Settings
      </Button>
      <Button variant="outlined" className={classes.button} component={Link} to="/tags">
        Tags
      </Button>
      <Button variant="outlined" className={classes.button} component={Link} to="/buildings">
        Buildings
      </Button>
      <Button variant="outlined" className={classes.button} component={Link} to="/floors">
        Floors
      </Button>
      <Button variant="outlined" className={classes.button} component={Link} to="/connectors">
        Connectors
      </Button>
      <Button variant="outlined" className={classes.button} component={Link} to="/floor-plans">
        Floor Plans
      </Button>
      <Button variant="outlined" className={classes.button} component={Link} to="/pano-floor-plans">
        Floor Plans - Panorama
      </Button>
      <Button variant="outlined" className={classes.button} component={Link} to="/suggestions">
        View suggestions
      </Button>
      <Button variant="outlined" className={classes.button} component={Link} to="/logs">
        Logs
      </Button>
      <Button variant="outlined" className={classes.button} component={Link} to="/api-keys">
        API Keys
      </Button>
      <Button variant="outlined" className={classes.button} component={Link} to="/videos">
        Tutorial videos
      </Button>
    </div>
  </BodyWrapper>
);

const styles = () => ({
  container: {},
  button: {
    display: 'block',
    margin: '10px 10px',
    padding: '10px 50px',
  },
});

export default withStyles(styles)(Dashboard);
