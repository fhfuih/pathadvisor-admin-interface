import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import Select from '@material-ui/core/Select';
import styles from './styles';

class ApiKeyRow extends PureComponent {
  renderTableCell(key, id = null) {
    const { classes, permissions, data, onInputChange } = this.props;

    const value = data[key];

    switch (key) {
      case 'id':
        return id;
      case 'inactive':
        return <Checkbox checked={value} onChange={onInputChange(id, key)} />;
      case 'expiryAt':
        return (
          <input
            type="text"
            className={classes.timeTextField}
            value={value || ''}
            onChange={onInputChange(id, key)}
          />
        );
      case 'namespace':
        return (
          <input
            type="text"
            className={classes.textField}
            value={value || ''}
            onChange={onInputChange(id, key)}
          />
        );
      case 'permissions':
        return (
          <Select
            multiple
            value={value || []}
            onChange={onInputChange(id, key)}
            renderValue={selected => selected.join(', ')}
          >
            {permissions.map(permission => (
              <MenuItem key={permission} value={permission}>
                <Checkbox checked={(value || []).indexOf(permission) > -1} />
                {permission}
              </MenuItem>
            ))}
          </Select>
        );
      case 'createdAt':
      case 'updatedAt':
      case 'lastUsedAt':
        return value ? new Date(value).toUTCString() : value;
      default:
        return value;
    }
  }

  render() {
    const {
      id,
      classes,
      fields,
      submitting,
      updating,
      deleting,
      onUpdate,
      onInsert,
      openDeleteDialog,
    } = this.props;
    return (
      <TableRow>
        {fields.map(({ key }) => (
          <TableCell className={classes.tableCell} key={`${id}_${key}`}>
            {this.renderTableCell(key, id)}
          </TableCell>
        ))}

        {id ? (
          <>
            <TableCell className={classes.tableCell}>
              <Button
                onClick={onUpdate(id)}
                variant="outlined"
                disabled={updating}
                className={classes.button}
              >
                {updating ? 'Updating...' : 'Update'}
              </Button>
            </TableCell>
            <TableCell className={classes.tableCell}>
              <Button
                className={classes.button}
                variant="outlined"
                onClick={openDeleteDialog(id)}
                disabled={deleting}
              >
                {deleting ? 'Deleting...' : 'Delete'}
              </Button>
            </TableCell>
          </>
        ) : (
          <>
            <TableCell className={classes.tableCell}>
              <Button className={classes.button} variant="outlined" onClick={onInsert}>
                {submitting ? 'Adding...' : '+ Add New API Key'}
              </Button>
            </TableCell>
            <TableCell className={classes.tableCell} />
          </>
        )}
      </TableRow>
    );
  }
}

ApiKeyRow.propTypes = {
  id: PropTypes.string,
  classes: PropTypes.shape({}).isRequired,
  permissions: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }).isRequired,
  ).isRequired,
  data: PropTypes.shape({
    namespace: PropTypes.string.isRequired,
    expiryAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    inactive: PropTypes.bool,
    permissions: PropTypes.arrayOf(PropTypes.string),
    createdAt: PropTypes.number,
    updatedAt: PropTypes.number,
    lastUsedAt: PropTypes.number,
  }).isRequired,
  submitting: PropTypes.bool,
  updating: PropTypes.bool,
  deleting: PropTypes.bool,
  onUpdate: PropTypes.func.isRequired,
  onInsert: PropTypes.func.isRequired,
  openDeleteDialog: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(ApiKeyRow);
