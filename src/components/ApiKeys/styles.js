const styles = {
  textField: { width: '80px' },
  timeTextField: { width: '180px' },
  button: {
    fontSize: '10px',
  },
  tableCell: {
    padding: '5px',
  },
  code: {
    overflow: 'scroll',
    margin: '10px 5px',
  },
  dialogContent: {
    margin: '10px',
  },
};

export default styles;
