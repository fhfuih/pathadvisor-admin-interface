import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';
import styles from './styles';
import ApiKeyRow from './ApiKeyRow';

const fields = [
  { key: 'id', name: 'ID' },
  { key: 'namespace', name: 'Namespace' },
  { key: 'permissions', name: 'Permissions' },
  { key: 'expiryAt', name: 'Expiry at (ISO 8601 format)' },
  { key: 'inactive', name: 'inactive' },
  { key: 'createdAt', name: 'Created At' },
  { key: 'updatedAt', name: 'Updated At' },
  { key: 'lastUsedAt', name: 'Last Used At' },
];

class ApiKeys extends Component {
  state = {
    confirmDialog: null,
    insertSuccessDialog: false,
  };

  componentDidUpdate(prevProps) {
    if (this.props.submitSuccess && prevProps.submitSuccess !== this.props.submitSuccess) {
      this.setState({ insertSuccessDialog: true });
    }
  }

  openDeleteDialog = id => () => {
    this.setState({
      confirmDialog: { id },
    });
  };

  onConfirmDelete = () => {
    this.props.onDelete(this.state.confirmDialog.id);
    this.setState({ confirmDialog: null });
  };

  onCloseDialog = () => {
    this.setState({ confirmDialog: null });
  };

  onCloseInsertSuccessDialog = (e, reason) => {
    if (reason === 'backdropClick') {
      return;
    }

    this.setState({ insertSuccessDialog: false });
  };

  render() {
    const {
      classes,
      ids,
      permissions,
      submitting,
      submitSuccess,
      submitError,
      updating,
      updateSuccess,
      updateError,
      deleting,
      deleteSuccess,
      deleteError,
      onInsert,
      onUpdate,
      onInputChange,
      onNewApiKeyInputChange,
      data,
      newApiKeyData,
    } = this.props;
    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              {fields.map(({ key, name }) => (
                <TableCell className={classes.tableCell} key={`header_${key}`}>
                  {name}
                </TableCell>
              ))}
              <TableCell className={classes.tableCell} />
              <TableCell className={classes.tableCell} />
            </TableRow>
            <ApiKeyRow
              permissions={permissions}
              fields={fields}
              submitting={submitting}
              onUpdate={onUpdate}
              onInsert={onInsert}
              openDeleteDialog={this.openDeleteDialog}
              data={newApiKeyData}
              onInputChange={(unused, key) => onNewApiKeyInputChange(key)}
            />
          </TableHead>
          <TableBody>
            {ids.map(id => (
              <ApiKeyRow
                key={id}
                id={id}
                permissions={permissions}
                fields={fields}
                updating={updating[id]}
                deleting={deleting[id]}
                onUpdate={onUpdate}
                onInsert={onInsert}
                openDeleteDialog={this.openDeleteDialog}
                data={data[id]}
                onInputChange={onInputChange}
              />
            ))}
          </TableBody>
        </Table>
        <AutoStatusBar open={updateSuccess} variant="success" message="Updated successfully" />
        <AutoStatusBar
          open={Boolean(submitSuccess)}
          variant="success"
          message="Inserted successfully"
        />
        <AutoStatusBar open={deleteSuccess} variant="success" message="Deleted successfully" />
        <AutoStatusBar
          open={Boolean(submitError)}
          variant="error"
          message={(submitError || {}).message}
        />
        <AutoStatusBar
          open={Boolean(updateError)}
          variant="error"
          message={(updateError || {}).message}
        />
        <AutoStatusBar
          open={Boolean(deleteError)}
          variant="error"
          message={(deleteError || {}).message}
        />
        {this.state.confirmDialog ? (
          <Dialog open onClose={this.onCloseDialog}>
            <DialogTitle>Are you sure to delete {this.state.confirmDialog.id} ?</DialogTitle>
            <DialogActions>
              <Button onClick={this.onConfirmDelete} color="primary" autoFocus>
                Yes
              </Button>
              <Button onClick={this.onCloseDialog} color="primary">
                No
              </Button>
            </DialogActions>
          </Dialog>
        ) : null}
        {this.state.insertSuccessDialog ? (
          <Dialog open onClose={this.onCloseInsertSuccessDialog}>
            <DialogTitle>API key created successfully. Here is the API Key</DialogTitle>
            <code className={classes.code}>{submitSuccess.key}</code>
            <div className={classes.dialogContent}>
              Once you close this dialog, you will never be able to see the API Key again for
              security reason. Please keep your API key in a safe place.
            </div>
            <DialogActions>
              <Button onClick={this.onCloseInsertSuccessDialog} color="primary">
                Close
              </Button>
            </DialogActions>
          </Dialog>
        ) : null}
      </div>
    );
  }
}

const ErrorPropType = PropTypes.shape({ message: PropTypes.string.isRequired });

ApiKeys.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  ids: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  permissions: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  data: PropTypes.objectOf(
    PropTypes.shape({
      namespace: PropTypes.string.isRequired,
      expiryAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      inactive: PropTypes.bool,
      permissions: PropTypes.arrayOf(PropTypes.string),
      createdAt: PropTypes.number.isRequired,
      updatedAt: PropTypes.number.isRequired,
      lastUsedAt: PropTypes.number,
    }),
  ).isRequired,
  newApiKeyData: PropTypes.shape({
    namespace: PropTypes.string.isRequired,
    expiryAt: PropTypes.string.isRequired,
    inactive: PropTypes.bool.isRequired,
    permissions: PropTypes.arrayOf(PropTypes.string).isRequired,
  }).isRequired,
  submitting: PropTypes.bool.isRequired,
  submitSuccess: PropTypes.shape({
    key: PropTypes.string.isRequired,
  }),
  submitError: ErrorPropType,
  updating: PropTypes.shape({ id: PropTypes.string }).isRequired,
  updateSuccess: PropTypes.bool.isRequired,
  updateError: ErrorPropType,
  deleting: PropTypes.shape({ id: PropTypes.string }).isRequired,
  deleteSuccess: PropTypes.bool.isRequired,
  deleteError: ErrorPropType,
  onNewApiKeyInputChange: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
  onInsert: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default withStyles(styles)(ApiKeys);
