import React, { Component } from 'react';
import Floors from '../Floors/Floors';
import * as floorsService from '../../services/floors/floors';
import * as buildingService from '../../services/buildings/buildings';
import Loading from '../Loading/Loading';
import Error from '../Error/Error';
import BodyWrapper from '../BodyWrapper/BodyWrapper';
import normalizeData from '../../services/normalizeData';

function dataFormatter(data) {
  const formattedData = {};
  Object.keys(data).forEach(key => {
    if (['id', 'name', 'buildingId'].includes(key)) {
      formattedData[key] = data[key].trim();
      return;
    }

    formattedData[key] = parseFloat(data[key]);
  });
  return formattedData;
}

const initNewFloorData = {
  id: '',
  name: '',
  buildingId: '',
  meterPerPixel: 0,
  mapWidth: 0,
  mapHeight: 0,
  ratio: 0,
  defaultLevel: 0,
  defaultX: 0,
  defaultY: 0,
  mobileDefaultLevel: 0,
  mobileDefaultX: 0,
  mobileDefaultY: 0,
  startX: 0,
  startY: 0,
  rank: 0,
};

class FloorsPage extends Component {
  state = {
    loading: false,
    loadError: null,
    submitting: false,
    submitSuccess: false,
    submitError: null,
    updating: {},
    updateSuccess: false,
    updateError: null,
    deleting: {},
    deleteSuccess: false,
    deleteError: null,
    ids: [],
    data: {},
    buildingIds: [],
    buildingData: {},
    newFloorData: { ...initNewFloorData },
  };

  componentDidMount() {
    this.setState({ loading: true });
    this.loadData();
  }

  onNewFloorInputChange = (unused, key) => e => {
    const value = e.target.value;
    this.setState(state => ({
      newFloorData: {
        ...state.newFloorData,
        [key]: value,
      },
    }));
  };

  onInputChange = (id, key) => e => {
    const value = e.target.value;
    this.setState(state => ({
      data: {
        ...state.data,
        [id]: {
          ...state.data[id],
          [key]: value,
        },
      },
    }));
  };

  onInsert = async () => {
    this.setState({
      submitting: true,
      submitSuccess: false,
      submitError: null,
    });

    const { id, ...rest } = this.state.newFloorData;
    const { error } = await floorsService.upsert(id, dataFormatter(rest));

    this.setState({
      submitError: error,
      submitSuccess: !error,
      submitting: false,
      newFloorData: { ...initNewFloorData },
    });

    if (!error) {
      this.loadData();
    }
  };

  onUpdate = id => async () => {
    this.setState({
      updating: { id },
      updateSuccess: false,
      updateError: null,
    });

    const { error } = await floorsService.upsert(id, dataFormatter(this.state.data[id]));

    this.setState({
      updateError: error,
      updateSuccess: !error,
      updating: {},
    });
  };

  onDelete = async id => {
    this.setState({
      deleting: { id },
      deleteSuccess: false,
      deleteError: null,
    });

    const { error } = await floorsService.remove(id);

    this.setState({
      deleteError: error,
      deleteSuccess: !error,
      deleting: {},
    });

    if (!error) {
      this.loadData();
    }
  };

  async loadData() {
    const [
      { data: rawData, error },
      { data: rawBuildingData, error: buildingError },
    ] = await Promise.all([floorsService.get(), buildingService.get()]);

    if (error || buildingError) {
      this.setState({ loading: false, loadError: error });
      return;
    }

    const { ids, data } = normalizeData(rawData);
    const { ids: buildingIds, data: buildingData } = normalizeData(rawBuildingData);
    this.setState({ loading: false, ids, data, buildingIds, buildingData });
  }

  render() {
    const {
      loading,
      loadError,
      data,
      ids,
      buildingIds,
      buildingData,
      newFloorData,
      submitting,
      submitSuccess,
      submitError,
      updating,
      updateSuccess,
      updateError,
      deleting,
      deleteSuccess,
      deleteError,
    } = this.state;

    switch (true) {
      case loading:
        return <Loading />;
      case Boolean(loadError):
        return <Error message="Error retrieving data" />;
      default:
        return (
          <BodyWrapper title="Floors">
            <Floors
              ids={ids}
              data={data}
              buildingIds={buildingIds}
              buildingData={buildingData}
              newFloorData={newFloorData}
              submitting={submitting}
              submitSuccess={submitSuccess}
              submitError={submitError}
              updating={updating}
              updateSuccess={updateSuccess}
              updateError={updateError}
              deleting={deleting}
              deleteSuccess={deleteSuccess}
              deleteError={deleteError}
              onNewFloorInputChange={this.onNewFloorInputChange}
              onInputChange={this.onInputChange}
              onInsert={this.onInsert}
              onUpdate={this.onUpdate}
              onDelete={this.onDelete}
            />
          </BodyWrapper>
        );
    }
  }
}

export default FloorsPage;
