import React, { Component } from 'react';
import groupBy from 'lodash.groupby';
import Connectors from '../Connectors/Connectors';
import * as connectorsService from '../../services/connectors/connectors';
import * as floorsService from '../../services/floors/floors';
import * as tagsService from '../../services/tags/tags';
import Loading from '../Loading/Loading';
import Error from '../Error/Error';
import BodyWrapper from '../BodyWrapper/BodyWrapper';
import normalizeData from '../../services/normalizeData';

function formatRequestBody(data) {
  const { _id, weight, tagIds, ...rest } = data;
  return {
    ...rest,
    weight: weight ? parseFloat(weight) : null,
    tagIds: !tagIds || !tagIds.length ? null : tagIds,
  };
}

function formatInputData(key, e) {
  switch (key) {
    case 'ignoreMinLiftRestriction':
      return e.target.checked;

    case 'tagIds':
      return e.target.value ? [e.target.value] : [];

    default:
      return e.target.value;
  }
}

const initConnectorData = {
  id: '',
  floorIds: [],
  weight: 0,
  tagIds: [],
  ignoreMinLiftRestriction: false,
};

class ConnectorsPage extends Component {
  state = {
    loading: false,
    loadError: null,
    submitting: false,
    submitSuccess: false,
    submitError: null,
    updating: {},
    updateSuccess: false,
    updateError: null,
    deleting: {},
    deleteSuccess: false,
    deleteError: null,
    ids: [],
    data: {},
    floorData: [],
    tagIds: [],
    tagData: {},
    newConnectorData: { ...initConnectorData },
  };

  componentDidMount() {
    this.setState({ loading: true });
    this.loadData();
  }

  onNewConnectorInputChange = key => e => {
    const value = formatInputData(key, e);
    this.setState(state => ({
      newConnectorData: {
        ...state.newConnectorData,
        [key]: value,
      },
    }));
  };

  onInputChange = (id, key) => e => {
    const value = formatInputData(key, e);
    this.setState(state => ({
      data: {
        ...state.data,
        [id]: {
          ...state.data[id],
          [key]: value,
        },
      },
    }));
  };

  onInsert = async () => {
    this.setState({
      submitting: true,
      submitSuccess: false,
      submitError: null,
    });

    const { id, ...rest } = this.state.newConnectorData;
    const { error } = await connectorsService.upsert(id, formatRequestBody(rest));

    this.setState({
      submitError: error,
      submitSuccess: !error,
      submitting: false,
      newConnectorData: { ...initConnectorData },
    });

    if (!error) {
      this.loadData();
    }
  };

  onUpdate = id => async () => {
    this.setState({
      updating: { id },
      updateSuccess: false,
      updateError: null,
    });

    const { error } = await connectorsService.upsert(id, formatRequestBody(this.state.data[id]));

    this.setState({
      updateError: error,
      updateSuccess: !error,
      updating: {},
    });
  };

  onDelete = async id => {
    this.setState({
      deleting: { id },
      deleteSuccess: false,
      deleteError: null,
    });

    const { error } = await connectorsService.remove(id);

    this.setState({
      deleteError: error,
      deleteSuccess: !error,
      deleting: {},
    });

    if (!error) {
      this.loadData();
    }
  };

  async loadData() {
    const [
      { data: rawData, error },
      { data: rawFloorData, error: floorError },
      { data: tags, error: getTagsError },
    ] = await Promise.all([connectorsService.get(), floorsService.get(), tagsService.get()]);

    if (error || floorError || getTagsError) {
      this.setState({ loading: false, loadError: error });
      return;
    }

    const { ids, data } = normalizeData(rawData);
    const floorData = Object.entries(groupBy(rawFloorData, 'buildingId')).map(([key, floors]) => [
      key,
      floors.sort((a, b) => a.rank - b.rank).map(({ _id, name }) => ({ _id, name })),
    ]);
    const { ids: tagIds, data: tagData } = normalizeData(tags);

    this.setState({ loading: false, ids, data, floorData, tagIds, tagData });
  }

  render() {
    const {
      loading,
      loadError,
      data,
      ids,
      floorData,
      tagIds,
      tagData,
      newConnectorData,
      submitting,
      submitSuccess,
      submitError,
      updating,
      updateSuccess,
      updateError,
      deleting,
      deleteSuccess,
      deleteError,
    } = this.state;

    switch (true) {
      case loading:
        return <Loading />;
      case Boolean(loadError):
        return <Error message="Error retrieving data" />;
      default:
        return (
          <BodyWrapper title="Connectors">
            <Connectors
              ids={ids}
              data={data}
              floorData={floorData}
              tagIds={tagIds}
              tagData={tagData}
              newConnectorData={newConnectorData}
              submitting={submitting}
              submitSuccess={submitSuccess}
              submitError={submitError}
              updating={updating}
              updateSuccess={updateSuccess}
              updateError={updateError}
              deleting={deleting}
              deleteSuccess={deleteSuccess}
              deleteError={deleteError}
              onNewConnectorInputChange={this.onNewConnectorInputChange}
              onInputChange={this.onInputChange}
              onInsert={this.onInsert}
              onUpdate={this.onUpdate}
              onDelete={this.onDelete}
            />
          </BodyWrapper>
        );
    }
  }
}

export default ConnectorsPage;
