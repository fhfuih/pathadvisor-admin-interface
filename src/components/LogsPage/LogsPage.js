import React, { Component } from 'react';
import Logs from '../Logs/Logs';
import * as logsService from '../../services/logs/logs';
import BodyWrapper from '../BodyWrapper/BodyWrapper';

class LogsPage extends Component {
  state = {
    startDate: '',
    endDate: '',
    downloadUrl: '',
  };

  onInputChange = key => e => {
    const value = e.target.value;

    this.setState({
      [key]: value,
    });

    this.setState(state => ({
      downloadUrl: logsService.getDownloadUrl(state.startDate, state.endDate),
    }));
  };

  render() {
    const { startDate, endDate, downloadUrl } = this.state;

    return (
      <BodyWrapper title="Logs">
        <Logs
          startDate={startDate}
          endDate={endDate}
          onInputChange={this.onInputChange}
          downloadUrl={downloadUrl}
        />
      </BodyWrapper>
    );
  }
}

export default LogsPage;
