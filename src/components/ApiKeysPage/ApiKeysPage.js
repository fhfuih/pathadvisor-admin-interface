import React, { Component } from 'react';
import ApiKeys from '../ApiKeys/ApiKeys';
import * as apiKeysService from '../../services/apiKeys/apiKeys';
import Loading from '../Loading/Loading';
import Error from '../Error/Error';
import BodyWrapper from '../BodyWrapper/BodyWrapper';
import normalizeData from '../../services/normalizeData';

const permissions = ['NODE:UPDATE:OTHERS', 'RESOURCE:INSERT'];

const initApiKeyData = {
  namespace: '',
  expiryAt: '',
  inactive: false,
  permissions: [],
};

function formatRequestBody(data) {
  const { namespace, expiryAt, createdAt, lastUpdatedAt, updatedAt, ...rest } = data;
  return {
    ...rest,
    namespace: namespace.trim(),
    expiryAt: expiryAt ? new Date(expiryAt).valueOf() : null,
  };
}

function formatInputData(key, e) {
  switch (key) {
    case 'inactive':
      return e.target.checked;
    default:
      return e.target.value;
  }
}

function transformData({ expiryAt, ...rest }) {
  let newExpiryAt = expiryAt;
  if (expiryAt) {
    try {
      newExpiryAt = new Date(expiryAt).toISOString();
    } catch (err) {
      newExpiryAt = null;
    }
  }

  return {
    ...rest,
    expiryAt: newExpiryAt,
  };
}

class ApiKeysPage extends Component {
  state = {
    loading: false,
    loadError: null,
    submitting: false,
    submitSuccess: null,
    submitError: null,
    updating: {},
    updateSuccess: false,
    updateError: null,
    deleting: {},
    deleteSuccess: false,
    deleteError: null,
    ids: [],
    data: {},
    newApiKeyData: { ...initApiKeyData },
  };

  componentDidMount() {
    this.setState({ loading: true });
    this.loadData();
  }

  onNewApiKeyInputChange = key => e => {
    const value = formatInputData(key, e);
    this.setState(state => ({
      newApiKeyData: {
        ...state.newApiKeyData,
        [key]: value,
      },
    }));
  };

  onInputChange = (id, key) => e => {
    const value = formatInputData(key, e);
    this.setState(state => ({
      data: {
        ...state.data,
        [id]: {
          ...state.data[id],
          [key]: value,
        },
      },
    }));
  };

  onInsert = async () => {
    this.setState({
      submitting: true,
      submitSuccess: null,
      submitError: null,
    });

    const { data, error } = await apiKeysService.insert(
      formatRequestBody(this.state.newApiKeyData),
    );

    this.setState({
      submitError: error,
      submitSuccess: !error ? data : null,
      submitting: false,
      ...(!error ? { newApiKeyData: { ...initApiKeyData } } : {}),
    });

    if (!error) {
      this.loadData();
    }
  };

  onUpdate = id => async () => {
    this.setState({
      updating: { id },
      updateSuccess: false,
      updateError: null,
    });

    const { error } = await apiKeysService.upsert(id, formatRequestBody(this.state.data[id]));

    this.setState(state => ({
      updateError: error,
      updateSuccess: !error,
      updating: {},
      data: {
        ...state.data,
        [id]: transformData(state.data[id]),
      },
    }));
  };

  onDelete = async id => {
    this.setState({
      deleting: { id },
      deleteSuccess: false,
      deleteError: null,
    });

    const { error } = await apiKeysService.remove(id);

    this.setState({
      deleteError: error,
      deleteSuccess: !error,
      deleting: {},
    });

    if (!error) {
      this.loadData();
    }
  };

  async loadData() {
    const { data: rawData, error } = await apiKeysService.get();

    if (error) {
      this.setState({ loading: false, loadError: error });
      return;
    }

    const { ids, data } = normalizeData(rawData, transformData);

    this.setState({ loading: false, ids, data });
  }

  render() {
    const {
      loading,
      loadError,
      data,
      ids,
      newApiKeyData,
      submitting,
      submitSuccess,
      submitError,
      updating,
      updateSuccess,
      updateError,
      deleting,
      deleteSuccess,
      deleteError,
    } = this.state;

    switch (true) {
      case loading:
        return <Loading />;
      case Boolean(loadError):
        return <Error message="Error retrieving data" />;
      default:
        return (
          <BodyWrapper title="API Keys">
            <ApiKeys
              ids={ids}
              data={data}
              permissions={permissions}
              newApiKeyData={newApiKeyData}
              submitting={submitting}
              submitSuccess={submitSuccess}
              submitError={submitError}
              updating={updating}
              updateSuccess={updateSuccess}
              updateError={updateError}
              deleting={deleting}
              deleteSuccess={deleteSuccess}
              deleteError={deleteError}
              onNewApiKeyInputChange={this.onNewApiKeyInputChange}
              onInputChange={this.onInputChange}
              onInsert={this.onInsert}
              onUpdate={this.onUpdate}
              onDelete={this.onDelete}
            />
          </BodyWrapper>
        );
    }
  }
}

export default ApiKeysPage;
