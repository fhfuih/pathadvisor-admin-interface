import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import PureTextField from './PureTextField';
import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';

const styles = {
  textField: { width: '80px' },
  button: {
    fontSize: '10px',
  },
  tableCell: {
    padding: '5px',
  },
};

class Floors extends Component {
  state = {
    confirmDialog: null,
  };

  openDeleteDialog = id => () => {
    this.setState({
      confirmDialog: { id },
    });
  };

  onConfirmDelete = () => {
    this.props.onDelete(this.state.confirmDialog.id);
    this.setState({ confirmDialog: null });
  };

  onCloseDialog = () => {
    this.setState({ confirmDialog: null });
  };

  renderTableCell(key, id = null, disabled = false) {
    const {
      buildingIds,
      buildingData,
      classes,
      onInputChange,
      onNewFloorInputChange,
      data,
      newFloorData,
    } = this.props;

    if (key === 'id' && id !== null) {
      return id;
    }

    const value = id === null ? newFloorData[key] : data[id][key];
    const onChange = id === null ? onNewFloorInputChange : onInputChange;

    if (key === 'buildingId') {
      return (
        <select value={value} onChange={onChange(id, key)}>
          <option />
          {buildingIds.map(buildingId => (
            <option key={buildingId} value={buildingId}>
              {buildingData[buildingId].name}
            </option>
          ))}
        </select>
      );
    }

    return (
      <PureTextField
        className={classes.textField}
        margin="normal"
        value={value}
        id={id}
        dataKey={key}
        onChange={onChange}
        disabled={disabled}
      />
    );
  }

  render() {
    const fields = [
      { key: 'id', name: 'ID' },
      { key: 'name', name: 'name' },
      { key: 'buildingId', name: 'Building' },
      { key: 'meterPerPixel', name: 'Meter per pixel' },
      { key: 'ratio', name: 'Ratio' },
      { key: 'defaultLevel', name: 'Default zoom level' },
      { key: 'defaultX', name: 'Default coordinate X' },
      { key: 'defaultY', name: 'Default coordinate Y' },
      { key: 'mobileDefaultLevel', name: 'Default zoom level for mobile' },
      { key: 'mobileDefaultX', name: 'Default coordinate X for mobile' },
      { key: 'mobileDefaultY', name: 'Default coordinate Y for mobile' },
      { key: 'rank', name: 'Rank' },
      { key: 'mapWidth', name: 'Map Width', disabled: true },
      { key: 'mapHeight', name: 'Map Height', disabled: true },
      { key: 'startX', name: 'Start coordinate X', disabled: true },
      { key: 'startY', name: 'Start coordinate Y', disabled: true },
    ];

    const {
      classes,
      ids,
      submitting,
      submitSuccess,
      submitError,
      updating,
      updateSuccess,
      updateError,
      deleting,
      deleteSuccess,
      deleteError,
      onInsert,
      onUpdate,
    } = this.props;
    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              {fields.map(({ key, name }) => (
                <TableCell className={classes.tableCell} key={`header_${key}`}>
                  {name}
                </TableCell>
              ))}
              <TableCell className={classes.tableCell} />
              <TableCell className={classes.tableCell} />
            </TableRow>
            <TableRow>
              {fields.map(({ key }) => (
                <TableCell className={classes.tableCell} key={`input_${key}`}>
                  {this.renderTableCell(key)}
                </TableCell>
              ))}
              <TableCell className={classes.tableCell}>
                <Button className={classes.button} variant="outlined" onClick={onInsert}>
                  {submitting ? 'Adding...' : '+ Add Floor'}
                </Button>
              </TableCell>
              <TableCell className={classes.tableCell} />
            </TableRow>
          </TableHead>
          <TableBody>
            {ids.map(id => (
              <TableRow key={id}>
                {fields.map(({ key, disabled }) => (
                  <TableCell className={classes.tableCell} key={`${id}_${key}`}>
                    {this.renderTableCell(key, id, disabled)}
                  </TableCell>
                ))}

                <TableCell className={classes.tableCell}>
                  <Button
                    onClick={onUpdate(id)}
                    variant="outlined"
                    disabled={updating.id === id}
                    className={classes.button}
                  >
                    {updating.id === id ? 'Updating...' : 'Update'}
                  </Button>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <Button
                    className={classes.button}
                    variant="outlined"
                    onClick={this.openDeleteDialog(id)}
                    disabled={deleting.id === id}
                  >
                    {deleting.id === id ? 'Deleting...' : 'Delete'}
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <AutoStatusBar open={updateSuccess} variant="success" message="Updated successfully" />
        <AutoStatusBar open={submitSuccess} variant="success" message="Inserted successfully" />
        <AutoStatusBar open={deleteSuccess} variant="success" message="Deleted successfully" />
        <AutoStatusBar
          open={Boolean(submitError)}
          variant="error"
          message={(submitError || {}).message}
        />
        <AutoStatusBar
          open={Boolean(updateError)}
          variant="error"
          message={(updateError || {}).message}
        />
        <AutoStatusBar
          open={Boolean(deleteError)}
          variant="error"
          message={(deleteError || {}).message}
        />
        {this.state.confirmDialog ? (
          <Dialog open onClose={this.onCloseDialog}>
            <DialogTitle>Are you sure to delete {this.state.confirmDialog.id} ?</DialogTitle>
            <DialogActions>
              <Button onClick={this.onConfirmDelete} color="primary" autoFocus>
                Yes
              </Button>
              <Button onClick={this.onCloseDialog} color="primary">
                No
              </Button>
            </DialogActions>
          </Dialog>
        ) : null}
      </div>
    );
  }
}

const ErrorPropType = PropTypes.shape({ message: PropTypes.string.isRequired });

Floors.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  ids: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  data: PropTypes.objectOf(
    PropTypes.shape({
      name: PropTypes.string,
      buildingId: PropTypes.string.isRequired,
      meterPerPixel: PropTypes.number.isRequired,
      mapWidth: PropTypes.number.isRequired,
      mapHeight: PropTypes.number.isRequired,
      ratio: PropTypes.number.isRequired,
      defaultX: PropTypes.number.isRequired,
      defaultY: PropTypes.number.isRequired,
      defaultLevel: PropTypes.number.isRequired,
      mobileDefaultLevel: PropTypes.number.isRequired,
      mobileDefaultX: PropTypes.number.isRequired,
      mobileDefaultY: PropTypes.number.isRequired,
      startX: PropTypes.number.isRequired,
      startY: PropTypes.number.isRequired,
      rank: PropTypes.number.isRequired,
    }),
  ).isRequired,
  buildingIds: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  buildingData: PropTypes.objectOf(
    PropTypes.shape({
      name: PropTypes.string,
    }),
  ).isRequired,
  newFloorData: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  submitting: PropTypes.bool.isRequired,
  submitSuccess: PropTypes.bool.isRequired,
  submitError: ErrorPropType,
  updating: PropTypes.shape({ id: PropTypes.string }).isRequired,
  updateSuccess: PropTypes.bool.isRequired,
  updateError: ErrorPropType,
  deleting: PropTypes.shape({ id: PropTypes.string }).isRequired,
  deleteSuccess: PropTypes.bool.isRequired,
  deleteError: ErrorPropType,
  onNewFloorInputChange: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
  onInsert: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default withStyles(styles)(Floors);
