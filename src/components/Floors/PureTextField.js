import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class PureTextField extends PureComponent {
  render() {
    const { className, value, id, dataKey, onChange, disabled = false } = this.props;
    return (
      <input
        className={className}
        value={value}
        onChange={onChange(id, dataKey)}
        disabled={disabled}
      />
    );
  }
}

PureTextField.propTypes = {
  className: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  id: PropTypes.string,
  dataKey: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

export default PureTextField;
