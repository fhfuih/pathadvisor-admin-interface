import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import AutoStatusBar from '../AutoStatusBar/AutoStatusBar';

const styles = {
  floorsContainer: {
    marginTop: '10px',
  },
  divider: {
    margin: '15px 0',
  },
};

function FloorPlansListing({
  classes,
  ids,
  data,
  buildingIds,
  buildingData,
  onMetaUpdate,
  metaUpdating,
  metaUpdateSuccess,
  metaUpdateError,
}) {
  return (
    <>
      {buildingIds.map(buildingId => (
        <div key={buildingId}>
          <Typography variant="h6" color="inherit" noWrap>
            {buildingData[buildingId].name}
          </Typography>
          <Divider />
          <div className={classes.floorsContainer}>
            {ids
              .filter(id => data[id].buildingId === buildingId)
              .sort((a, b) => data[a].rank - data[b].rank)
              .map(floorId => (
                <Button
                  key={floorId}
                  variant="outlined"
                  component={Link}
                  to={`/pano-floor-plans/${floorId}`}
                >
                  {data[floorId].name || floorId}
                </Button>
              ))}
          </div>
        </div>
      ))}
      <Divider className={classes.divider} />
      <Button onClick={onMetaUpdate} variant="contained" color="primary" disabled={metaUpdating}>
        {metaUpdating ? 'Updating meta...' : 'Update meta version'}
      </Button>
      <AutoStatusBar open={metaUpdateSuccess} variant="success" message="Meta updated" />
      <AutoStatusBar open={metaUpdateError} variant="error" message="Failed to update meta" />
    </>
  );
}

FloorPlansListing.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  ids: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  data: PropTypes.objectOf(
    PropTypes.shape({
      name: PropTypes.string,
      buildingId: PropTypes.string.isRequired,
      rank: PropTypes.number.isRequired,
    }),
  ).isRequired,
  buildingIds: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  buildingData: PropTypes.objectOf(
    PropTypes.shape({
      name: PropTypes.string,
    }),
  ).isRequired,
  onMetaUpdate: PropTypes.func.isRequired,
  metaUpdating: PropTypes.bool.isRequired,
  metaUpdateSuccess: PropTypes.bool.isRequired,
  metaUpdateError: PropTypes.bool.isRequired,
};

export default withStyles(styles)(FloorPlansListing);
