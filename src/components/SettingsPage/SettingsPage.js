import React, { Component } from 'react';
import Settings from '../Settings/Settings';
import * as settingsService from '../../services/settings/setting';
import Loading from '../Loading/Loading';
import Error from '../Error/Error';
import BodyWrapper from '../BodyWrapper/BodyWrapper';

class SettingsPage extends Component {
  state = {
    loading: false,
    loadError: null,
    submitting: false,
    submitSuccess: false,
    submitError: null,
    data: {
      levelToScale: [],
      highestLevel: 0,
      lowestLevel: 0,
      minutesPerMeter: 0,
      defaultPosition: {
        level: 0,
        x: 0,
        y: 0,
        floor: '',
      },
      mobileDefaultPosition: {
        level: 0,
        x: 0,
        y: 0,
        floor: '',
      },
    },
  };

  async componentDidMount() {
    this.setState({ loading: true });

    const { error, data } = await settingsService.get();

    this.setState({ loading: false });

    if (error) {
      this.setState({
        loadError: error,
      });
      return;
    }

    const {
      levelToScale,
      highestLevel,
      lowestLevel,
      minutesPerMeter,
      defaultPosition,
      mobileDefaultPosition,
    } = data;

    this.setState({
      data: {
        levelToScale,
        highestLevel,
        lowestLevel,
        minutesPerMeter,
        defaultPosition,
        mobileDefaultPosition,
      },
    });
  }

  onScaleInputChange = level => e => {
    const value = parseFloat(e.target.value);

    this.setState(state => {
      const levelToScale = [...state.data.levelToScale];
      levelToScale[level] = value;
      return {
        data: {
          ...state.data,
          levelToScale,
        },
      };
    });
  };

  onInputChange = key => e => {
    const value = parseFloat(e.target.value);
    this.setState(state => ({
      data: {
        ...state.data,
        [key]: value,
      },
    }));
  };

  onPositionChange = positionName => key => e => {
    const value = key !== 'floor' ? parseFloat(e.target.value) : e.target.value.trim();
    this.setState(state => ({
      data: {
        ...state.data,
        [positionName]: {
          ...state.data[positionName],
          [key]: value,
        },
      },
    }));
  };

  onAddLevel = () => {
    this.setState(state => ({
      data: {
        ...state.data,
        levelToScale: [...state.data.levelToScale, 0],
      },
    }));
  };

  onRemoveLevel = level => () => {
    this.setState(state => {
      const levelToScale = [...state.data.levelToScale];
      levelToScale.splice(level, 1);
      return {
        data: {
          ...state.data,
          levelToScale,
        },
      };
    });
  };

  onSubmit = async () => {
    this.setState({ submitError: null, submitSuccess: false, submitting: true });
    const { error } = await settingsService.insert(this.state.data);
    this.setState({
      submitError: error,
      submitSuccess: !error,
      submitting: false,
    });
  };

  render() {
    const { loading, data, loadError, submitting, submitSuccess, submitError } = this.state;
    switch (true) {
      case loading:
        return <Loading />;
      case Boolean(loadError):
        return <Error message="Error retrieving data" />;
      default:
        return (
          <BodyWrapper title="Application settings">
            <Settings
              data={data}
              onScaleInputChange={this.onScaleInputChange}
              onInputChange={this.onInputChange}
              onPositionChange={this.onPositionChange('defaultPosition')}
              onMobilePositionChange={this.onPositionChange('mobileDefaultPosition')}
              onAddLevel={this.onAddLevel}
              onRemoveLevel={this.onRemoveLevel}
              onSubmit={this.onSubmit}
              submitting={submitting}
              submitSuccess={submitSuccess}
              submitError={submitError}
            />
          </BodyWrapper>
        );
    }
  }
}

export default SettingsPage;
